var dhxComboOriginDest,dhxcomboDuenoCaja,dhxTipoCaja,dhxEstadoCaja,dhxConsignatario,dhxComboDirty,dhxcomboMC;
function EditarES(item,tractor_caja){
    var width = Number(window.innerWidth / 4);
		var ventanaAgregar = dhxWindow.createWindow({
			id:"ventanaAgregar",
			// left:Number(window.innerWidth * 0.1),
			text:"<b>Editar</b>",
			// top:600,
			width:width,
			height:600,
			center:true,
			modal:true
		});
		dhxWindow.window("ventanaAgregar").button("minmax").attachEvent("onClick", function(win, button){
			var elemento = document.getElementsByClassName("dhxwin_active");
			for(var i = 0; i < elemento.length; i++){
				var elemento2 = document.getElementsByClassName("pantalla_completa");
				if(elemento2.length > 0){
					elemento[i].className = "dhxwin_active";
					dhxWindow.window("ventanaAgregar").setPosition(Number(window.innerWidth * 0.1), 0);
				}else{
					dhxWindow.window("ventanaAgregar").setPosition(0, 0);
					elemento[i].className += " pantalla_completa";
				}
			}
			return true;
		});
		var formAdd = ventanaAgregar.attachForm();
        var option = 0;
        if(item.tipoDocumento == 2){
            formAdd.loadStruct("../data/json/agregar_trailer.json");
        }else{
            formAdd.loadStruct("../data/json/agregar_es.json");
            formAdd.hideItem("trailernum");
			formAdd.hideItem("trailerowner");
			formAdd.hideItem("trailertype");
			formAdd.hideItem("traileris");
			formAdd.hideItem("consign");
			formAdd.hideItem("seal");
        }
		formAdd.attachEvent("OnXLE", function(){
			dhxComboClienteDir   = formAdd.getCombo("clientedirecto");
			dhxComboClienteDir.attachEvent("onXLE", function(){
				// your code here
				if(typeof client_name !== "undefined"){					
					option=dhxComboClienteDir.getIndexByValue(client_name);
					console.log(option);
					if(option !== -1){
						dhxComboClienteDir.selectOption(option);
						dhxComboClienteDir.disable();
					}
				}else{
                    if(item.tipoDocumento == 2){
                        option=dhxComboClienteDir.getIndexByValue(item.columna5);
                        console.log(option);
                        if(option !== -1){
                            dhxComboClienteDir.selectOption(option);
                            // dhxComboClienteDir.disable();
                        }
                    }else{
                        option=dhxComboClienteDir.getIndexByValue(item.columna6);
                        console.log(option);
                        if(option !== -1){
                            dhxComboClienteDir.selectOption(option);
                            // dhxComboClienteDir.disable();
                        }
                    }
                }
			});

			formAdd.setItemLabel("title", "<b>Datos:</b>");
            formAdd.setItemValue("patio",patio);
            formAdd.disableItem("patio");
            formAdd.setItemValue("date",moment(item.date_time_ingreso).format("YYYY-MM-DD HH:mm:ss"));
            if(item.tipoDocumento == 2){
                formAdd.setItemValue("trailernum",item.columna1);
                dhxComboOriginDest   = formAdd.getCombo("origin_destiny");
                dhxComboOriginDest.attachEvent("onXLE", function(){
                    option=dhxComboOriginDest.getIndexByValue(item.columna6);
                    console.log(option);
                    if(option !== -1){
                        dhxComboOriginDest.selectOption(option);
                        // dhxComboOriginDest.disable();
                    }
                });
                dhxcomboDuenoCaja    = formAdd.getCombo("trailerowner");
                // console.log(item.columna2)
                dhxcomboDuenoCaja.attachEvent("onXLE", function(){
                    option=dhxcomboDuenoCaja.getIndexByValue(item.columna2);
                    console.log(option);
                    if(option !== -1){
                        dhxcomboDuenoCaja.selectOption(option);
                        // dhxcomboDuenoCaja.disable();
                    }
                });
                dhxTipoCaja          = formAdd.getCombo("trailertype");
                dhxTipoCaja.attachEvent("onXLE", function(){
                    option=dhxTipoCaja.getIndexByValue(item.columna9);
                    console.log(option);
                    if(option !== -1){
                        dhxTipoCaja.selectOption(option);
                        // dhxTipoCaja.disable();
                    }
                });
                dhxEstadoCaja        = formAdd.getCombo("traileris");
                dhxEstadoCaja.attachEvent("onXLE", function(){
                    option=dhxEstadoCaja.getIndexByValue(item.columna8);
                    console.log(option);
                    if(option !== -1){
                        dhxEstadoCaja.selectOption(option);
                        // dhxEstadoCaja.disable();
                    }
                });
                dhxConsignatario     = formAdd.getCombo("consign");
                dhxConsignatario.attachEvent("onXLE", function(){
                    option=dhxConsignatario.getIndexByValue(item.columna4);
                    console.log(option);
                    if(option !== -1){
                        dhxConsignatario.selectOption(option);
                        // dhxConsignatario.disable();
                    }
                });
                formAdd.setItemValue("seal",item.columna7);
                formAdd.setItemValue("notas_datos_generales",item.columna10);
            }else{
                dhxComboDirty      = formAdd.getCombo("clean_dirty");
                dhxComboDirty.attachEvent("onXLE", function(){
                    option=dhxComboDirty.getIndexByValue(item.columna4);
                    console.log(option);
                    if(option !== -1){
                        dhxComboDirty.selectOption(option);
                        // dhxComboDirty.disable();
                    }
                });
                dhxcomboMC   = formAdd.getCombo("motor_carrier");
                dhxcomboMC.attachEvent("onXLE", function(){
                    option=dhxcomboMC.getIndexByValue(item.columna1);
                    console.log(option);
                    if(option !== -1){
                        dhxcomboMC.selectOption(option);
                        // dhxcomboMC.disable();
                    }
                });
                formAdd.setItemValue("tractor_num",item.columna2);
                formAdd.setItemValue("drivername",item.columna3);
                formAdd.setItemValue("origin_destiny",item.columna6);
                formAdd.setItemValue("notas_datos_generales",item.columna10);
            }
			formAdd.disableItem("date");
		
		});
		formAdd.attachEvent("onChange", function (name, value, state){
			if(name == "tractor_whit_trailer"){
				if(value == "1"){//yes trailer
					formAdd.showItem("trailernum");
					formAdd.showItem("trailerowner");
					formAdd.showItem("trailertype");
					formAdd.showItem("traileris");
					formAdd.showItem("consign");
					formAdd.showItem("seal");
				}else if(value == "0"){//not trailer
					formAdd.hideItem("trailernum");
					formAdd.hideItem("trailerowner");
					formAdd.hideItem("trailertype");
					formAdd.hideItem("traileris");
					formAdd.hideItem("consign");
					formAdd.hideItem("seal");
				}
			}
		});
		formAdd.attachEvent("onButtonClick", function(name){
			if (name=="enviar"){
				if(formAdd.validate()){
					var values = formAdd.getFormData(true);		
					delete values.itemid;	
                    values.instruccion = "edit_es";
                    values.tipoDocumento = item.tipoDocumento;
                    values.quien_captura = usuario;
                    values.itemid = item.itemid;
                    
					$.post(node_chapel_web + "entradas_salidas",values, function(data, textStatus, xhr) {
						if(textStatus == "success"){
							if(data !== "ERROR"){
                                
								Refresh(tractor_caja);
								ventanaAgregar.close();
							}else{
                                dhtmlx.alert({
                                    title:"ERROR",
                                    type:"alert-error",
                                    text:"Ocurrio un error al editar E/S"
                                });	
                                // Refresh(tractor_caja);
                                ventanaAgregar.close();
							}
						}
					});						
				}
			}
		});
		formAdd.enableLiveValidation(true);		

}