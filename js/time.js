function convertir_fecha(timestamp1) {
    var d = new Date((timestamp1*1000));
    var day,month,year,h,m,s;
    // var months=[""];

    day = d.getDate();
    month= d.getMonth()+1;
    year= d.getFullYear();

    h = d.getHours();
    m = d.getMinutes();
    s = d.getSeconds();

    if(day<10){day = "0"+day;}
    if(month<10) {month = "0"+month;}

    if (h<10) {h = "0"+h;}
    if (m<10) {m = "0"+m;}
    if (s<10) {s = "0"+s;}
    return day+"/"+month+"/"+year+" "+h+":"+m+":"+s;
}

function convertir_fecha2(timestamp1) {
    var d = new Date((timestamp1*1000));
    var day,month,year,h,m,s;
    // var months=[""];

    day = d.getDate();
    month= d.getMonth()+1;
    year= d.getFullYear();

    h = d.getHours();
    m = d.getMinutes();
    s = d.getSeconds();

    if(day<10){day = "0"+day;}
    if(month<10) {month = "0"+month;}

    if (h<10) {h = "0"+h;}
    if (m<10) {m = "0"+m;}
    if (s<10) {s = "0"+s;}
    return day+"/"+month+"/"+year+"<br>"+h+":"+m+":"+s;
}

function get_time_difference(earlierDate, laterDate) {
    var nTotalDiff = laterDate.getTime() - earlierDate.getTime();
    var oDiff = new Object();

    oDiff.days = Math.floor(nTotalDiff / 1000 / 60 / 60 / 24);
    nTotalDiff -= oDiff.days * 1000 * 60 * 60 * 24;

    oDiff.hours = Math.floor(nTotalDiff / 1000 / 60 / 60);
    nTotalDiff -= oDiff.hours * 1000 * 60 * 60;

    oDiff.minutes = Math.floor(nTotalDiff / 1000 / 60);
    nTotalDiff -= oDiff.minutes * 1000 * 60;

    oDiff.seconds = Math.floor(nTotalDiff / 1000);

    return oDiff;
}


function secondsToHms(d) {
    d = Number(d);

    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    return ((h > 0 ? h + ":" + (m < 10 ? "0" : "") : "") + m + ":" + (s < 10 ? "0" : "") + s);
}