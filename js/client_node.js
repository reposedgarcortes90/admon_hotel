function RowId(){
    rowNum = rowNum+1;
    return rowNum;
}

function CargarSocket() {
    // socket = io.connect('http://10.1.1.117:443', { 'forceNew': true });
    // socket = io.connect('http://dimeca.dvtrace.com:443', { 'forceNew': true });
    socket = io.connect('http://dimecapruebas.dvtrace.com:443', { 'forceNew': true });
    var usname = makeid();

    socket.on('connect',function(data){
        //console.log(data);
        //socket.emit('conectado', usname );
        // console.log("Conectado");
        socket.emit('conectado',user_username,zona_user);
    });

    socket.on('get_viaje', function(json) {
      if (dataViewViaje!==undefined && dataStoreViaje!==undefined){
        var existe = false;

        for (var i = 0; i < dataViewViaje.dataCount(); i++) {
            var idDataViaje=dataViewViaje.idByIndex(i);
            var viaje=dataViewViaje.item(idDataViaje);
            if (viaje.idoperacion==json.idoperacion){
                existe = true;
                break;
            }
        }

        if (existe==false) {
            dataViewViaje.add(json);
            var id="";
            for (var i = 0; i < dataViewViaje.dataCount(); i++) {
                var idDataViaje=dataViewViaje.idByIndex(i);
                var viaje=dataViewViaje.item(idDataViaje);
                if (viaje.idoperacion==json.idoperacion){
                    dataViewViaje.moveTop(idDataViaje);
                    id=idDataViaje;
                }
            }
            for (var j = 0; j < dataStoreViajeAsignado.dataCount(); j++) {
                var idDataViajeAs=dataStoreViajeAsignado.idByIndex(j);
                var viajeAs=dataStoreViajeAsignado.item(idDataViajeAs);
                if (viajeAs.idoperacion==json.idoperacion){
                    dataStoreViajeAsignado.remove(idDataViajeAs);
                }
            }
            filtrar();
        } 

      }
    });

    socket.on('quita_viaje_asignado',function(idViaje){
        for (var j = 0; j < dataStoreViajeAsignado.dataCount(); j++) {
            var idDataViajeAs=dataStoreViajeAsignado.idByIndex(j);
            var viajeAs=dataStoreViajeAsignado.item(idDataViajeAs);
            if (viajeAs.idoperacion==idViaje){
                dataStoreViajeAsignado.remove(idDataViajeAs);
            }
        }
        filtrar();
    });


    socket.on('baja_viaje',function(idviaje) {
        for (var j = 0; j < dataViewViaje.dataCount(); j++) {
            var idDataViaje=dataViewViaje.idByIndex(j);
            var viaje=dataViewViaje.item(idDataViaje);
            if (viaje.idoperacion==idviaje){
                dataViewViaje.remove(idDataViaje);
            }
        }
        filtrar();
    });

    socket.on('quitar_viaje',function(data) {
        ViajeAsignado(data.id_viaje,data.id_operador,data.number,data.numero_activo);
        filtrar();
    });

    socket.on('update_count_viajes',function(data) {
        var tiempo=(new Date().getTime())/1000;
        tiempo=convertir_fecha(tiempo);

        for (var j = 0; j < dataStoreOperador.dataCount(); j++) {
            var idDataOperador=dataStoreOperador.idByIndex(j);
            var operador=dataStoreOperador.item(idDataOperador);
            if (operador.idoperador == data.idoperador) {
                operador.viajes=data.number;
                dataStoreOperador.update(operador);
                if (operador.viajes<1){
                    dhtmlx.message({
                        type:"css_message",
                        text: "<a href='#' style='color:#000' onclick='SeleccionaOperador("+operador.id+")'> El operador "+operador.nombre_nick+" está listo para realizar más viajes "+tiempo+".</a>",
                        expire: -1
                    });
                }
                break;
            } 
        }
        filtrar();
    });

    socket.on('operador_login',function(data) {
            for (var j = 0; j < dataStoreOperador.dataCount(); j++) {
                var idDataOperador=dataStoreOperador.idByIndex(j);
                var operador=dataStoreOperador.item(idDataOperador);
                if (operador.idoperador == data.idoperador) {
                    operador.online=200;
                    operador.idunidad=data.idunidad;
                    operador.numero_activo=data.numero_activo;
                    dataStoreOperador.update(operador);
                    break;
                } 
            }
            filtrar();
    });

    socket.on('refresh_data_as',function() {
        dataStoreViajeAsignado.clearAll();
        dataStoreViajeAsignado.load("../../node/get_viajes_despacho_asignados?zona="+zona_user,function(){
            console.log("Actualizando aqui esto");
        });
    });

    socket.on('refresh_data_all',function() {
        RefreshData();
    });

    socket.on('operador_logout',function(data) {
            for (var j = 0; j < dataStoreOperador.dataCount(); j++) {
                var idDataOperador=dataStoreOperador.idByIndex(j);
                var operador=dataStoreOperador.item(idDataOperador);
                if (operador.idoperador == data) {
                    operador.online=400;
                    operador.idunidad=0;
                    operador.numero_activo="";
                    dataStoreOperador.update(operador);
                    break;
                } 
            }
            filtrar();
    });
            
    socket.on('updated_viaje',function(json) {
        for (var i = 0; i < dataStoreViaje.dataCount(); i++) {
            var idDataViaje=dataStoreViaje.idByIndex(i);
            var viaje=dataStoreViaje.item(dataStoreViaje.idByIndex(i));
            if (viaje.idoperacion==json.idoperacion){
                viaje.contenedor=json.contenedor;
                viaje.fecha_recoleccion=json.fecha_recoleccion;
                viaje.idpatio=json.idpatio;
                viaje.material=json.material;
                viaje.patio=json.patio;
                dataStoreViaje.update(idDataViaje, viaje);
            }
        }
        filtrar();
    });

    socket.on('updated_viaje_asignado',function(json) {
        for (var i = 0; i < dataStoreViajeAsignado.dataCount(); i++) {
            var idDataViaje=dataStoreViajeAsignado.idByIndex(i);
            var viaje=dataStoreViajeAsignado.item(idDataViaje);
            if (viaje.idoperacion==json.idoperacion){
                viaje=json;
                viaje.id=idDataViaje;
                dataStoreViajeAsignado.update(idDataViaje, viaje);
            }
        }
        filtrar();
    });

    socket.on('ubicacion',function(json) {
        //id_operador, lat, lon, vel
        CrearMoverMarker(json.id_operador,json.lat,json.lon,json.vel,json.fecha,json.hora,json.num_viaje);
        filtrar();
    });    

    socket.on('log_despacho',function(cadena) {
        var arr = [];
        for (elem in cadena) {
           arr.push(cadena[elem]);
        }
        if (gridLog!=null){
            gridLog.addRow(RowId(),arr,0);
        }
        filtrar();
    });

    socket.on('messages', function(msg) {
        //console.info(msg);
        //console.log(msg);
        //AQUI COLAR LA ACCION A REALIZARSE!!
        $("#messages").prepend(msg + "</br>");
        // alert("colgar");
    });
}
function addMessage(us, txt) {  
    var message = {username: us, unidades: txt};
    socket.emit('new-message', us, txt);
    return false;
}
function SeleccionaOperador(id) {
    dataViewOperador.select(id);
}
function ViajeAsignado(id_viaje,id_operador,number,numero_activo){
    var viajeAsignar;
    for (var i = 0; i < dataViewViaje.dataCount(); i++) {
        var idDataViaje=dataViewViaje.idByIndex(i);
        var viaje=dataViewViaje.item(idDataViaje);
        if (viaje.idoperacion==id_viaje){
            viajeAsignar=viaje;
            viajeAsignar.numero_activo=numero_activo;
            viajeAsignar.idoperador=id_operador;
            dataViewViaje.remove(idDataViaje);
        }
    }
    for (var j = 0; j < dataViewOperador.dataCount(); j++) {
        var idDataOperador=dataViewOperador.idByIndex(j);
        var operador=dataViewOperador.item(idDataOperador);
        if (operador.idoperador==id_operador){
            operador.viajes=number;
            if (viajeAsignar) {
                viajeAsignar.nombre_nick=operador.nombre_nick;
                dataStoreOperador.update(operador);
            } 
            
            
        }
    }
    if (viajeAsignar) {
        dataStoreViajeAsignado.add(viajeAsignar);
        // console.log(viajeAsignar);
    }
}

function makeid()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 5; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}