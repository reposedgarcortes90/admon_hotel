<?php
$service = $_GET["service"];
$mob = $_GET["mob"];

$variable = "";
foreach($_GET as $key => $value)
{
    $variable = $variable . $key . "=" . urlencode ($value) . "&";
}

$data = array();
foreach($_POST as $key => $value)
{
    $data[$key] = $value;  
 }
 $url = 'http://127.0.0.1/node_hotel/'. $service;
// $url = 'http://intercambios.dvtrace.com/node_intercambios/'. $service;
// $url = 'http://dimeca01.dvtrace.com:8081/'. $service;

$options = array(
    'http' => array(
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($data),
    ),
);
$context  = stream_context_create($options);
$result = file_get_contents($url, false, $context);
if ($result !="ERROR" && $result != "DATOSINVALIDOS") {
    ini_set("session.cookie_lifetime","10");
	ini_set("session.gc_maxlifetime","10");
    session_start();
    $_SESSION['data']=$result;
    if ($mob == "1") {
        // header("location: ../../files/captura_viajes_mob.php");
        $res = json_decode($result);
        if(empty($res)){
            header("location: ../../login.php");
        }else{
            header("location: ../../files/main_mob2.php");
        }
        
    } else {
        $res = json_decode($result);
        if(empty($res)){
            header("location: ../../login.php");
        }else{
            header("location: ../../files/main.php");
        }
    }
}
else{
    // if ($mob=="1") {
    //     header("location: ../../login_mob.php");    
    // } else {
        header("location: ../../login.php?error=".$result);
}
?>