<?php
require_once ('mob_dect/Mobile_Detect.php');
$detect = new Mobile_Detect();
if (!$detect->isMobile()){
// Detecta si es un móvil
  header("location:login.php");
} 
?>



<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>HOTEL | Login</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900'>
<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Montserrat:400,700'>
<link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
<!-- <link rel="shortcut icon" href="./img/chapelicono.ico" /> -->
<link rel="stylesheet" href="css/fontawesome.min.css">
<link rel="shortcut icon" href="img/hotel.png" />
<link rel="stylesheet" href="css/style.css">
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/md5.min.js"></script>
<link rel="stylesheet" href="codebase/dhtmlx.css">
<script src="codebase/dhtmlx.js"></script>
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<style>
.form{
  border: 1px solid #c3c3c3;
  /* display: flex;
  flex-wrap: wrap; */
  align-content: center;
}
</style>
  
</head>

<body onload="establecermedida()">
  


<div class="form">
  <div class="thumbnail">
    <img style="width:100%;border-radius:30%;box-shadow: 0 15px 10px #777;
  transform: rotate(-3deg);" src="img/logo.png"/>
  </div>
  <br>
  <form class="login-form" id="formulario"  action="./data/php/handlerSesion.php?service=login_web&mob=1" method="POST">
    <div class="form-group row">
      <label for="username" class="col-2 col-form-label" style="text-align:left;color:#016836;padding:15px;text-transform:uppercase;font-weight:bolder">Usuario:</label>
      <div class="col-10">
        <input type="text" placeholder="Usuario" name="nombreusuario" id="nombreusuario" required=true>
      </div>
    </div>
    <div class="form-group row">
      <label for="clave" class="col-2 col-form-label" style="text-align:left;color:#016836;padding:15px;text-transform:uppercase;font-weight:bolder">Clave:</label>
      <div class="col-10">
        <input type="password" placeholder="Clave" name="clave" id="clave" required=true/>
      </div>
    </div>    
    <!-- <input id="send" style="display:none;"  type="submit" name="enviar" value='Iniciar Sesión' /> -->
    <div style="width:calc(100% - 30px)" class="boton" id="enviar" onclick="hazlo()"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;<b>Iniciar Sesión</b></div>
  </form>
  <!-- <p class="message"><span>Powered <i class="fa fa-cog"></i> by <a href="http://www.dvtrace.com">DVTrace</a></span></p>
  <a href="http://intercambios.dvtrace.com/node_intercambios/mostrarArchivo?tipo=APK&archivo=intercambiosgeneralizados.apk">Descarga la app</a> -->
</div>

  <!-- <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> -->

    <!-- <script src="./js/index.js"></script> -->
<script>
function hazlo(){
  if(document.forms.formulario.checkValidity()){

    document.getElementById('clave').value = md5(document.getElementById('clave').value);
    document.getElementById('formulario').submit();
    document.getElementById("nombreusuario").value = "";
    document.getElementById("clave").value = "";
  }else{
    dhtmlx.alert({
        title:"Alerta",
        type:"alert-warning",
        text:"Debes ingresar las credenciales"
    });
  }
  // setInterval(() => {}, 5000); 
}
function establecermedida(){
  let medida = (screen.availHeight - $(".form")[0].offsetHeight)/2;
  document.body.style = "padding-top:"+medida+"px";
}
var input = document.getElementById("clave");
var input2 = document.getElementById("nombreusuario");

// Execute a function when the user releases a key on the keyboard
input.addEventListener("keyup", function(event) {
  // Number 13 is the "Enter" key on the keyboard
  if (event.keyCode === 13) {
    // Cancel the default action, if needed
    event.preventDefault();
    // Trigger the button element with a click
    if(input2.value != "") {document.getElementById("enviar").click();}
    else{
      dhtmlx.alert({
          title:"Alerta",
          type:"alert-warning",
          text:"Debes ingresar el nombre de usuario"
      });
    }
  }
});

// Execute a function when the user releases a key on the keyboard
input2.addEventListener("keyup", function(event) {
  // Number 13 is the "Enter" key on the keyboard
  if (event.keyCode === 13) {
    // Cancel the default action, if needed
    event.preventDefault();
    // Trigger the button element with a click
    if(input.value != "") {document.getElementById("enviar").click();}
    else{
      dhtmlx.alert({
          title:"Alerta",
          type:"alert-warning",
          text:"Debes ingresar la clave"
      });
    }
  }
});
</script>
</body>
</html>
