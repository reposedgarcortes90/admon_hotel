<?php 
require_once 'validarsesion.php'; 
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE; Chrome=1" />
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">    
        <meta charset="utf-8" />
        <title>Inicio</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <script src="../codebase/dhtmlx.js" type="text/javascript" charset="utf-8"></script>
        <link rel="stylesheet" href="../codebase/dhtmlx.css" type="text/css">
        <link rel="stylesheet" href="../fonts/css/font-awesome.css">
        <link rel="stylesheet" href="../codebase/fonts/font_roboto/roboto.css">
        <script src="../js/rutas_node.js"></script>
        <script>
            function doOnLoad(){
                window.location.href=""
            }
        </script>
    </head>

<body onload="doOnLoad();">
</body>
</html>
