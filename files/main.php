<?php 
require_once 'auth.php'; 
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE; Chrome=1" />
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">    
        <meta charset="utf-8" />
        <title>ADMINISTRACIÓN DE HOTEL</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <script src="../codebase/dhtmlx.js" type="text/javascript" charset="utf-8"></script>
        <link rel="stylesheet" href="../codebase/dhtmlx.css" type="text/css">
        <link rel="stylesheet" href="../fonts/css/font-awesome.css">
        <link rel="stylesheet" href="../icomoon/hotel/style.css">
        <link rel="stylesheet" href="../codebase/fonts/font_roboto/roboto.css">
        <script src="../js/rutas_node.js"></script>
        <script src="../js/jquery.min.js"></script>
        <!-- <link rel="shortcut icon" href="https://cdn4.iconfinder.com/data/icons/user-interface-33/80/Home-512.png" /> -->
            

        <style>

        html, body {
            width: 100%;
            height: 100%;
            margin: 0px;
            padding: 0px;
            overflow: hidden;
            background-color: #fcfff5;
            font-family:'Roboto'
        }

        a.chapel_app:link {color:black;text-decoration:none;}
		a.chapel_app:visited {color:black;text-decoration:none;}
		a.chapel_app:active {color:black;text-decoration:none;}
        a.chapel_app:hover {color:black;text-decoration:none;}
        .dhxtreeview_item_text{
            height:auto;
            word-break: break-all;
        }
        .dhx_toolbar_material.dhxtoolbar_icons_18 div.dhx_toolbar_btn div.dhxtoolbar_text,.dhx_toolbar_material.dhxtoolbar_icons_18 div.dhx_toolbar_btn, .dhx_toolbar_material.dhxtoolbar_icons_18 div.dhx_toolbar_arw, .dhx_toolbar_material.dhxtoolbar_icons_18 div.dhx_toolbar_text{
            height:100%;
            line-height:100%;

        }
        .dhx_toolbar_material div.dhx_toolbar_arw div.arwimg{
            height:100%;
            font-size:1em
        }
        div.dhxtoolbar_text{
            width: auto;
            max-width: 600px;
            line-height:260%!important
        }
        div.dhxtreeview_item_label{
            left:60px!important
        }
        i.icon-tipohabitaciones-4,i.icon-habitaciones{
            font-size: 1.5em;
            font-weight: 400;
        }
        i.icon-empleados{
            font-size: .9rem!important;
            font-weight: 100!important;
        }
        .dhx_toolbar_poly_material.dhxtoolbar_icons_18.dhx_toolbar_shadow{
            overflow-y: auto!important;
            top: 48px!important;
            visibility: visible;
            z-index: 105!important;
            width: 140!important;
            height: auto!important;
            left: calc(100% - 153.1px)!important;
        }
</style>
    <script>
        var parent=0;
        var myTreeView;
        var differentuser = false;
        var firstlink = "about:blank";
        // var TextToolbarValue = "XYZ";
        function doOnLoad(){
            CargarLayout();
            if(!localStorage.getItem("estados_municipios")){
                $.get("../../node_hotel/estados_municipios?instruccion=getall",function(data){
                    console.log(data);
                    localStorage.setItem("estados_municipios",JSON.stringify(data));
                });
            }
        };

    function CargarLayout() {
        if(typeof itemid != "undefined") localStorage.setItem('itemidhotelsm',itemid);
        if(typeof nombreusuario != "undefined") localStorage.setItem('nombreusuariohotelsm',nombreusuario);
        var link = document.querySelector("link[rel*='icon']") || document.createElement('link');
        // link.type = 'image/x-icon';
        link.rel = 'shortcut icon';
        link.href = '../img/logo.png';
        document.getElementsByTagName('head')[0].appendChild(link);
        myLayout = new dhtmlXLayoutObject({
              parent: document.body,
              pattern: "2U",
        });

        myLayout.cells("a").setText('<i class="fa fa-bars" aria-hidden="true"></i>&nbsp;Menú');
        myLayout.cells("a").setWidth(230);
        myLayout.cells("b").hideHeader();

        var myToolbar=myLayout.attachToolbar();
        myToolbar.setIconSize(12);
        myToolbar.setIconset("awesome");        
        myToolbar.loadStruct("../data/json/maintoolbar.json","json");
        myToolbar.attachEvent("onClick",function(id) {
            if (id=="user_logout"){
                cerrarSesion();
            }
        });
        var user = "Detalles de Conexión.....";

        myLayout.cells("a").attachHTMLString('<div id="treeviewObj" style="width:100%;height:100%;">'+'</div>');
        // if(lectura == 1){
        //     myTreeView = new dhtmlXTreeView({
        //         parent:"treeviewObj",
        //         checkboxes: true,
        //         iconset: "font_awesome",
        //         json: "../data/json/mainmenulectura.json",
        //     });
        // }else{

            myTreeView = new dhtmlXTreeView({
                parent:"treeviewObj",
                checkboxes: true,
                iconset: "font_awesome",
                json: "../data/json/mainmenu.json",
            });
        // }


        myTreeView.attachEvent("onXLE", function(){
            myLayout.cells('a').collapse();
            myLayout.cells("b").attachURL("./dashboard.php");

            // if (localStorage.getItem("user_id") && localStorage.getItem("user_id").length > 0 ) {
            //     if (localStorage.getItem("user_id") == user_id) {
            //         localStorage.setItem("user_id", user_id);
            //         differentuser = false;
            //     } else {
            //         var my_user_id = user_id;
            //         localStorage.setItem("user_id", user_id);
            //         differentuser = true;                    
            //     }
            // } else {
            //         var my_user_id = user_id;
            //         localStorage.setItem("user_id", user_id);
            //         differentuser = true;                   
            // }

/*             var firsturl = "";
            var mispermisos = permisosmenu.split(",");
            var valores = myTreeView.getAllUnchecked();
            for (var i = 0; i < valores.length; i++) {
                for (var j = 0; j < mispermisos.length; j++) {
                     if (valores[i] == mispermisos[j]) {
                         myTreeView.checkItem(mispermisos[j]);

                         if (differentuser && (firsturl == "" || firsturl == null)) {
                            firsturl = myTreeView.getUserData(mispermisos[j], "url")
                         } 
                     }
                    
                }
            } */

            /* var activos =  myTreeView.getAllChecked(); */

            // if (activos.length> 0) {
            //     firstlink = myTreeView.getUserData(activos[0], "url")
            // } 

/*             if (differentuser) {
                firstlink = firsturl;
                localStorage.setItem("url","");
            } 

            for (var i = 0; i < activos.length; i++) {
                var pid = myTreeView.getParentId(activos[i]);
                if (pid) {
                    myTreeView.checkItem(pid);
                } 
                
            } */

/*             var activos =  myTreeView.getAllChecked();

            var valores = myTreeView.getAllUnchecked();
            var conhijos = new Array();
            for (var i = 0; i < valores.length; i++) {
                var hijos = myTreeView.getSubItems(valores[i]);

                if (hijos.length > 0) {
                    conhijos.push(valores[i]);
                } else {
                    myTreeView.deleteItem(valores[i]);
                }
            }

            for (var i = 0; i < conhijos.length; i++) {
                myTreeView.deleteItem(conhijos[i]);
            }

            myTreeView.enableCheckboxes(false); */



            //************
/*             if (localStorage.getItem("url") && localStorage.getItem("url").length > 0 ) {
                myLayout.cells("b").detachObject();
                myLayout.cells("b").attachURL('./'+localStorage.getItem("url"));
            } else { */
                // myLayout.cells("b").attachURL('./'+'dashboard1.php');  //----------------> Aqui llamar a la URL principal de cada cliente
/*                 myLayout.cells("b").detachObject();
                myLayout.cells("b").attachURL('./'+ firstlink);  //----------------> Aqui llamar a la URL principal de cada cliente
                localStorage.setItem("url",firstlink);
            }

            if (localStorage.getItem("apanelstatus") && localStorage.getItem("apanelstatus").length > 0 ) {
                if (localStorage.getItem("apanelstatus")=="1") 
                    {myLayout.cells('a').expand()} 
                else 
                    {myLayout.cells('a').collapse()}
            }  */
            //************

        });     


/*
            if (localStorage.getItem("url") && localStorage.getItem("url").length > 0 ) {
                myLayout.cells("b").attachURL('./'+localStorage.getItem("url"));
            } else {
                // myLayout.cells("b").attachURL('./'+'dashboard1.php');  //----------------> Aqui llamar a la URL principal de cada cliente
                myLayout.cells("b").attachURL('./'+ firstlink);  //----------------> Aqui llamar a la URL principal de cada cliente
            }*/

/*             if (localStorage.getItem("apanelstatus") && localStorage.getItem("apanelstatus").length > 0 ) {
                if (localStorage.getItem("apanelstatus")=="1") 
                    {myLayout.cells('a').expand()} 
                else 
                    {myLayout.cells('a').collapse()}
            }  */


            myTreeView.attachEvent("onSelect", function(id, mode){

                if (mode) {
                    if (parent==0 || parent==undefined) {parent=myTreeView.getParentId(id)} else {}
                    if (myTreeView.getParentId(id)) {
                        if (myTreeView.getParentId(id) != parent) {
                        myTreeView.closeItem(parent);    
                        parent =  myTreeView.getParentId(id);
                        } else {}
                    } 
                    
                    var url = myTreeView.getUserData(id, "url");
                    if (url !=null) {
                        myLayout.cells('a').collapse();
                        myLayout.cells("b").progressOn();
                    } 
                    if (url) {
                    myLayout.cells("b").attachURL("./"+url);
                    localStorage.setItem("url", url);
                    } else {}

                } else {}
            });            

            myLayout.attachEvent("onContentLoaded", function(id){
                myLayout.cells("b").progressOff();
            });

            myLayout.attachEvent("onCollapse", function(id){
                localStorage.setItem("apanelstatus", 0);
            });

            myLayout.attachEvent("onExpand", function(id){
                localStorage.setItem("apanelstatus", 1);
            });            


        }
        function cerrarSesion(){
            $.get("../../node_hotel/logout?itemid="+localStorage.itemidhotelsm+"&usuario="+localStorage.nombreusuariohotelsm,function(data){
                console.log(data);
                localStorage.removeItem("itemidhotelsm");
                localStorage.removeItem("nombreusuariohotelsm");
                window.location="../logout.php";
            });
        }

    </script>

    </head>

<body onload="doOnLoad();">
    </body>
</html>
