<?php 
require_once 'auth.php'; 
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Usuarios</title>
	<script type="text/javascript" src="../js/moment.js"></script>
	<script type="text/javascript" src="../js/md5.min.js"></script>

	<link rel="stylesheet" href="../codebase/fonts/font_roboto/roboto.css">
	<link rel="stylesheet" href="../codebase/dhtmlx.css">
	<link rel="stylesheet" href="../fonts/css/font-awesome.css">
	<script type="text/javascript" src="../codebase/dhtmlx.js"></script>
	<script type="text/javascript" src="../js/jquery.min.js"></script>
	<script type="text/javascript" src="../js/rutas_node.js"></script>
	<link rel="stylesheet" href="../css/others_styles.css">
	<style>
		html, body {
    width: 100%;
    height: 100%;
    margin: 0;
    padding: 0;
	font-family:'Roboto'
}
.pantalla_completa{
	z-index: 1005;
    margin-left: 0!important;
    margin-right: 0!important;
	width: 100%!important;
    height: 100%!important
}
.dhxwin_active{
    z-index: 1005;
    /* margin-left: 10%; */
    /* margin-right: 10%; */
	width: 80%;
    height: 100%;
}
.dhxwin_inactive{
    /* margin-left: 10%; */
    /* margin-right: 10%; */
	width: 80%;
	max-width:100%;
    /* height: 100%; */
}
.dhxwin_brd{
    left:2px!important;
    top:48px!important;
	width: calc(100% - 6px)!important;
	height:100px;
}
.dhx_cell_wins{
	width: calc(100% - 6px)!important;
}
.dhx_cell_cont_wins{
    left: 0px;
    top: 0px;
    overflow: auto;
	width: 100%!important;
	
}

.dhxform_base{
    width: 99%;
}
.dhxform_label{
    width: 40%!important;
}
.dhxform_control{
    width: 50%!important;
}
.dhxform_textarea{
    width: 100%!important;
}
.dhxcombo_material{
    width: 100%!important;
}
.dhxcombo_input{
    width: 100%!important;
}
.dhxform_btn{
    left:40%;
    width: 20%!important;
    text-align: center;
}
.dhxform_obj_material fieldset.dhxform_fs{
	width:calc(100% - 10px)!important
}
/* enabled, not checked */
.dhxform_obj_material div.dhxform_img.btn2state_0 {
	background-image: url("../button2state/material/toggle_off.png");
	width: 42px;
	height: 24px;
}
/* enabled, checked */
.dhxform_obj_material div.dhxform_img.btn2state_1 {
	background-image: url("../button2state/material/toggle_on.png");
	width: 42px;
	height: 24px;
}
/* disabled, not checked */
.dhxform_obj_material div.disabled div.dhxform_img.btn2state_0 {
	background-image: url("../button2state/material/toggle_off_dis.png");
	width: 42px;
	height: 24px;
}
/* disabled, checked */
.dhxform_obj_material div.disabled div.dhxform_img.btn2state_1 {
	background-image: url("../button2state/material/toggle_on_dis.png");
	width: 42px;
	height: 24px;
}

/* common */
/* fix label align a bit */
.dhxform_obj_material div.dhxform_item_label_right div.dhxform_label div.dhxform_label_nav_link {
	padding-top: 2px;
}
.dhxform_obj_material .dhxform_select{
	width:100%!important;
}
textarea[name="correos"]{
	width:200%!important;
}
#blah{
    width:150%;
    height:300px
}
.dhxwins_vp_material div.dhxwin_hdr div.dhxwin_text{

    padding-left: 19px;
    padding-right: 92px;
    height: 100%;
    vertical-align: middle;
    font-size: 2em;
    padding-top: 5px;
}
	</style>
</head>
<body onload="Cargar()"></body>

<script type="text/javascript">
	var layout, grid, datastore;
	var toolbar,ruta_get_usuarios,dhxWindow;
	ruta_get_usuarios = "../../node_hotel/usuarios_hotel?instruccion=leer";
	LoadMenu();
	function Cargar()
	{
		dhxWindow = new dhtmlXWindows("material");
		datastore= new dhtmlXDataStore({
		    url:ruta_get_usuarios,
		    datatype:"json"
		});
		layout = new dhtmlXLayoutObject({
			parent: document.body,
			pattern: "1C",
			offsets: {
				top: 0,
				right: 0,
				bottom: 0,
				left: 0
			},
		});
		layout.cells("a").hideHeader();
		// });
		// layout.cells("a").setText('<i class="fa fa-users" aria-hidden="true"></i>&nbsp;Usuarios');

		toolbar = layout.cells("a").attachToolbar();
		toolbar.addButton("agregar_user", 1, "<i class='fa fa-user-plus' style='color:#0f9038' aria-hidden='true'></i>", null, null);
		// toolbar.addButton("agregar_client", 2, "<i class='fa fa-plus-circle' style='color:#0f9038' aria-hidden='true'></i> <b>Cliente</b>", null, null);
		toolbar.addButton("editar", 3, "<i class='fa fa-pencil' style='color:#2475ff' aria-hidden='true'></i>", null, null);
		toolbar.addButton("eliminar", 4, "<i class='fa fa-trash' style='color:#3869ad' aria-hidden='true'></i>", null, null);
		// toolbar.addButton("exportar_pdf", 5, "<i class='fa fa-file-pdf-o' style='color:#ff0000' aria-hidden='true'></i>", null, null);
		toolbar.addButton("exportar_excel", 6, "<i class='fa fa-file-excel-o' style='color:#0f9038' aria-hidden='true'></i>", null, null);
	    toolbar.addButton("refrescar", 7, "<i class='fa fa-refresh' style='color:#0000ff;font-weight:bolder' aria-hidden='true'></i>", null, null);

	    toolbar.attachEvent('onClick',function(id){
	    	if (id=="refrescar"){
	    		Refresh();
	    	}
			if(id == "agregar_user"){
				AgregarUsuario();
			}
			if(id == "agregar_client"){
				AgregarCliente();
			}
			if(id == "editar"){
				selectedId = grid.getSelectedRowId();
				if(selectedId !== null){
					EditarUsuario(selectedId);
				}else{
					dhtmlx.alert({
						title:"Alerta",
						type:"alert-warning",
						text:"Debes seleccionar un usuario previamente"
					});
				}
			}
			if(id == "eliminar"){
				selectedId = grid.getSelectedRowId();
				if(selectedId !== null){
					EliminarUsuario(selectedId);
				}else{
					dhtmlx.alert({
						title:"Alerta",
						type:"alert-warning",
						text:"Debes seleccionar un usuario previamente"
					});
				}
			}
			if(id == "exportar_pdf"){
				grid.toPDF('../codebase/grid-pdf-php/generate.php','color',true,true);
			}
			if(id == "exportar_excel"){
				grid.toExcel('../codebase/grid-excel-php/generate.php');	
			}
	    });
		grid=layout.cells("a").attachGrid();
		var header = [
			"Usuario",
			"itemid",
			"Fecha de ingreso",
			"Creador",
			"Último usuario que editó"
		];
		var styles = [
			"text-align:left;vertical-align:middle",
			"text-align:left;vertical-align:middle",
			"text-align:center;vertical-align:middle",
			"text-align:center;vertical-align:middle",
			"text-align:center;vertical-align:middle"
		];
		var widths = [
			"*",
			"*",
			"*",
			"*",
			"*"
		];
		var columnsids = [
			"nombreusuario",
			"itemid",
			"fechacrea",
			"usuariocrea",
			"usuarioedita"
		];
		var aligns = [
			"left",
			"left",
			"center",
			"center",
			"center"
		];
		var sorts = [
			"str",
			"str",
			"str",
			"str",
			"str"
		];
		var coltypes = [
			"ro",
			"ro",
			"ro",
			"ro",
			"ro"
		];
		grid.setHeader(header.join(","),null,styles);
		grid.setInitWidths(widths.join(","));
		grid.setColumnIds(columnsids.join(","));
		grid.setColAlign(aligns.join(","));
		grid.setColSorting(sorts.join(","));
		grid.setColTypes(coltypes.join(","));
		grid.setEditable(true);
		grid.setColumnHidden(1,true);
		// grid.setColumnHidden(6,true);
		grid.setImagePath("../codebase/imgs/");
		grid.enableContextMenu(menu);
		grid.enableMultiline(true);
		grid.enableKeyboardSupport(true);
		grid.enableBlockSelection(true);
		grid.attachEvent("onKeyPress",onKeyPressed); 
		grid.init();
		grid.sync(datastore);
		grid.attachEvent("onEditCell", onEditCell);
	}
	function LoadMenu()
	{
		menu=new dhtmlXMenuObject();
		menu.renderAsContextMenu();
		menu.attachEvent("onClick", onButtonClick);
		menu.loadStruct("../data/xml/Registro_ContextMenuReportes.xml");
	}
	function onButtonClick(menuitemId, type) {
		if (menuitemId == "excel") {
			grid.toExcel('../codebase/grid-excel-php/generate.php','color', true);
		}

		if (menuitemId == "pdf") {
			grid.toPDF('../codebase/grid-pdf-php/generate.php', 'color', true);
		}
		if (menuitemId == "refresh") {
			Refresh();
		}
	}
	function Refresh(){
		datastore.clearAll();
		// grid.clearAll();
		datastore.load(ruta_get_usuarios,function(){
			grid.clearAll();
			grid.sync(datastore);
		});
	}
	function onKeyPressed(code,ctrl,shift){
		if(code==67&&ctrl){
			if (!grid._selectionArea){
				return dhtmlx.alert(
					{
						title:"Alerta",
						type:"alert",
						text:"You must select a block from the grid previously"
					}
				);
			} 
			grid.setCSVDelimiter("\t");
			grid.copyBlockToClipboard();
		}
		return true;
	}
	function onEditCell(stage,rId,cInd,nValue,oValue){
		//stage Estado del Editor
		//rId Id del Row
		//cInd Indice de la celda
		//nValue Valor nuevo solo si el stage es 2
		//oValue Valor viejo solo si el stage es 2	
		item = datastore.item(rId);
		if(stage == 1){//El editor esta abierto
			return true;
		}
		else if(stage == 2){//El editor esta cerrado
			if(cInd == 0){//username
				item.username = nValue;
				EditarCellUsuario(item);
			}	
			if(cInd == 4){//correo
				item.correo = nValue;
				EditarCellUsuario(item);
			}	
			if(cInd == 6){//admin
				item.admin = nValue;
				EditarCellUsuario(item);
			}
			// if(cInd == 7){//captura
			// 	item.captura = nValue;
			// 	EditarCellUsuario(item);
			// }
			if(cInd == 7){//patio
				item.patio = nValue;
				EditarCellUsuario(item);
			}
			return true;
		} 				
	}
	function AgregarUsuario(){
        var width = document.body.clientWidth - 200;
        var height = document.body.clientHeight;
			
		
		var ventanaAgregar = dhxWindow.createWindow({
			id:"ventanaAgregar",
			// left:Number(window.innerWidth * 0.1),
			text:"<i class='fa fa-user-plus' style='color:white;heigth:100%;vertical-align:middle' aria-hidden='true'></i>",
			// top:600,
			width:width,
			height:height,
			center:true,
			modal:true
		});
		dhxWindow.window("ventanaAgregar").setPosition(100, 0);
		dhxWindow.window("ventanaAgregar").button("minmax").attachEvent("onClick", function(win, button){
			console.log(win, button);
			var elemento = document.getElementsByClassName("dhxwin_active");
			for(var i = 0; i < elemento.length; i++){
				var elemento2 = document.getElementsByClassName("pantalla_completa");
				if(elemento2.length > 0){
					elemento[i].className = "dhxwin_active";
					dhxWindow.window("ventanaAgregar").setPosition(100, 0);
				}else{
					dhxWindow.window("ventanaAgregar").setPosition(0, 0);
					elemento[i].className += " pantalla_completa";
				}
			}
			return true;
		});
		document.body.onresize = ()=>{
            var width = document.body.clientWidth - 200;
            var height = document.body.clientHeight;
			try{
				dhxWindow.window("ventanaAgregar").setDimension(width, height);
			}catch(error){
			}
        };
		var formAdd = ventanaAgregar.attachForm();
		formAdd.loadStruct("../data/json/agregar_usuario.json");
		formAdd.attachEvent("OnXLE", function(){
		});
		formAdd.attachEvent("onButtonClick", function(name){
			if (name=="enviar"){
				if(formAdd.validate()){
					var values = formAdd.getFormData(true);
					var cpassword = values.cclave;
					delete values.cclave;
					if(cpassword.toUpperCase() == values.clave.toUpperCase()){			
						values.usuariocrea = nombreusuario;//nombreusuario
						values.eliminado = 0;
						values.clave = md5(values.clave);
						delete values.itemid;
						console.log(values,"values");
						$.post("../../node_hotel/usuarios_hotel?instruccion=agregar",values, function(data, textStatus, xhr) {
							if(textStatus == "success"){
								if(typeof data !== "string"){
									Refresh();
									ventanaAgregar.close();
								}else{
									dhtmlx.alert({
										title:"ERROR",
										type:"alert-error",
										text:data
									});	
								}
							}
						});
					}else{
						dhtmlx.alert({
							title:"ALERTA",
							type:"alert-warning",
							text:"Las claves deben coincidir"
						});
					}
						
				}
			}
		});
		formAdd.enableLiveValidation(true);	
	}
	function AgregarCliente(){
		var width = Number(window.innerWidth / 4);
		var ventanaAgregar = dhxWindow.createWindow({
			id:"ventanaAgregar",
			// left:Number(window.innerWidth * 0.1),
			text:"Agregar Cliente",
			// top:600,
			width:width,
			height:600,
			center:true,
			modal:true
		});
		dhxWindow.window("ventanaAgregar").button("minmax").attachEvent("onClick", function(win, button){
			console.log(win, button);
			var elemento = document.getElementsByClassName("dhxwin_active");
			for(var i = 0; i < elemento.length; i++){
				var elemento2 = document.getElementsByClassName("pantalla_completa");
				if(elemento2.length > 0){
					elemento[i].className = "dhxwin_active";
					dhxWindow.window("ventanaAgregar").setPosition(Number(window.innerWidth * 0.1), 0);
				}else{
					dhxWindow.window("ventanaAgregar").setPosition(0, 0);
					elemento[i].className += " pantalla_completa";
				}
			}
			return true;
		});
		var formAdd = ventanaAgregar.attachForm();
		formAdd.loadStruct("../data/json/agregar_client_access.json");
		formAdd.attachEvent("OnXLE", function(){
		});
		formAdd.attachEvent("onButtonClick", function(name){
			if (name=="enviar"){
				if(formAdd.validate()){
					var values = formAdd.getFormData(true);
					var cpassword = values.cpassword;
					delete values.cpassword;
					if(cpassword.toUpperCase() == values.password.toUpperCase()){			
						var repetido = false;
						$.get(ruta_get_usuarios, function(datos){	
							datos.forEach(dato => {
								if(dato.username.toUpperCase() === values.username.toUpperCase()){
									repetido = true;
								}
							});
							if(repetido){
								dhtmlx.alert({
									title:"ERROR",
									type:"alert-error",
									text:"The client "+ values.username +" is already registered please enter another"
								});
								Refresh();
							}else{
								values.instruccion = "add";
								values.client_id   = client_id;
								var permisos = {};
								permisos.capturar_app = values.capturar_app;
								permisos.ingresar_web = values.ingresar_web;
								permisos.meter_sacar_inv = values.meter_sacar_inv;
								permisos.ver_detalles_solo_cliente = values.ver_detalles_solo_cliente;
								values.permisos = JSON.stringify(permisos);
								delete values.capturar_app;
								delete values.ingresar_web;
								delete values.meter_sacar_inv;
								delete values.ver_detalles_solo_cliente;
								values.eliminado=0;
								$.post(node_chapel_web + "usuarios",values, function(data, textStatus, xhr) {
									if(textStatus == "success"){
										if(data !== "ERROR"){
											Refresh();
											ventanaAgregar.close();
										}else{
											dhtmlx.alert({
												title:"ERROR",
												type:"alert-error",
												text:"An error occurred when adding a client"
											});	
											Refresh();
										}
									}
								});
							}
						});
					}else{
						dhtmlx.alert({
							title:"ERROR",
							type:"alert-warning",
							text:"The passwords do not match"
						});	
					}
				}
			}
		});
		formAdd.enableLiveValidation(true);	
	}
	function EditarUsuario(selectedId){
		item = datastore.item(selectedId);
/* 		delete item.fecha_alta;
		delete item.last_login;
		var permisos = JSON.parse(item.permisos);
		var user = "";
		if(item.idCliente !== null){
			user = "Client";
		}else{
			user = "User";
		} */
        var width = document.body.clientWidth - 200;
        var height = document.body.clientHeight;
		
		
		var ventanaEditar = dhxWindow.createWindow({
			id:"ventanaEditar",
			// left:Number(window.innerWidth * 0.1),
			text:"<i class='fa fa-pencil' style='color:white;heigth:100%;vertical-align:middle' aria-hidden='true'></i><i class='fa fa-user' aria-hidden='true'></i>",
			// top:600,
			width:width,
			height:height,
			center:true,
			modal:true
		});
		document.body.onresize = ()=>{
            var width = document.body.clientWidth - 200;
            var height = document.body.clientHeight;
			try{
				dhxWindow.window("ventanaEditar").setDimension(width, height);
			}catch(error){
			}
        };
		dhxWindow.window("ventanaEditar").setPosition(100, 0);
		dhxWindow.window("ventanaEditar").button("minmax").attachEvent("onClick", function(win, button){
			console.log(win, button);
			var elemento = document.getElementsByClassName("dhxwin_active");
			for(var i = 0; i < elemento.length; i++){
				var elemento2 = document.getElementsByClassName("pantalla_completa");
				if(elemento2.length > 0){
					elemento[i].className = "dhxwin_active";
					dhxWindow.window("ventanaEditar").setPosition(100, 0);
				}else{
					dhxWindow.window("ventanaEditar").setPosition(0, 0);
					elemento[i].className += " pantalla_completa";
				}
			}
			return true;
		});
		var formEdit = ventanaEditar.attachForm();
		formEdit.loadStruct("../data/json/agregar_usuario.json");
		formEdit.attachEvent("OnXLE", function(){
			formEdit.setItemValue("itemid", item.itemid);
			formEdit.setReadonly("nombreusuario",true);
			formEdit.setItemLabel("titulo", "<b>Datos de usuario</b>");
			formEdit.setItemValue("nombreusuario", item.nombreusuario);
			var dhxCombito2 = formEdit.getCombo("empleado");
            dhxCombito2.attachEvent("onXLE", function(){					
                var option=dhxCombito2.getIndexByValue(item.empleado);
                if(option !== -1){
                    dhxCombito2.selectOption(option);
                }
			});
			// formEdit.setItemValue("correo", item.correo);
			// formEdit.setItemValue("capturar_app", permisos.capturar_app);
			// formEdit.setItemValue("ingresar_web", permisos.ingresar_web);
			// formEdit.setItemValue("meter_sacar_inv", permisos.meter_sacar_inv);
			// formEdit.setItemValue("ver_detalles_solo_cliente", permisos.ver_detalles_solo_cliente);
			// formEdit.setItemValue("activo_app", item.activo_app);
			// formEdit.setItemValue("captura", item.captura);
			// formEdit.setItemValue("patio", item.patio);
			// formEdit.removeItem("cpassword");
		});
		formEdit.attachEvent("onButtonClick", function(name){
			if (name=="enviar"){
				if(formEdit.validate()){
					var values = formEdit.getFormData(true);
					if(values.clave.length > 0){
						var cpassword = values.cclave;
						delete values.cclave;

						if(cpassword.toUpperCase() == values.clave.toUpperCase()){
							values.usuarioedita = nombreusuario;//nombreusuario
							values.eliminado = 0;
							values.clave = md5(values.clave);
							// delete values.itemid;
							console.log(values,"values");
							$.post("../../node_hotel/usuarios_hotel?instruccion=actualizar",values, function(data, textStatus, xhr) {
								if(textStatus == "success"){
									if(data !== "ERROR"){
										Refresh();
										ventanaEditar.close();
									}else{
										dhtmlx.alert({
											title:"ERROR",
											type:"alert-error",
											text:"Ocurrio un error al agregar usuario"
										});	
									}
								}
							});
						}else{
							dhtmlx.alert({
								title:"ALERTA",
								type:"alert-warning",
								text:"Las claves deben coincidir"
							});
						}
					}else{
						delete values.cclave;
						delete values.clave;
						values.usuarioedita = nombreusuario;//nombreusuario
						values.eliminado = 0;
						// values.clave = md5(values.clave);
						// delete values.itemid;
						console.log(values,"values");
						$.post("../../node_hotel/usuarios_hotel?instruccion=actualizar",values, function(data, textStatus, xhr) {
							if(textStatus == "success"){
								if(data !== "ERROR"){
									Refresh();
									ventanaEditar.close();
								}else{
									dhtmlx.alert({
										title:"ERROR",
										type:"alert-error",
										text:"Ocurrio un error al agregar usuario"
									});	
								}
							}
						});
					}
					
				}
			}
		});
		formEdit.enableLiveValidation(true);	
	}
	function EditarCellUsuario(item){
		delete item.id;
		delete item.password;
		delete item.fecha_alta;
		delete item.last_login;
		var permisos = JSON.parse(item.permisos);
		permisos.capturar_app = item.capturar_app;
		permisos.ingresar_web = item.ingresar_web;
		permisos.meter_sacar_inv = item.meter_sacar_inv;
		permisos.ver_detalles_solo_cliente = item.ver_detalles_solo_cliente;
		item.permisos = JSON.stringify(permisos);
		delete item.capturar_app;
		delete item.ingresar_web;
		delete item.meter_sacar_inv;
		delete item.ver_detalles_solo_cliente;
		item.instruccion = "update";
		$.post(node_chapel_web + "usuarios",item, function(data, textStatus, xhr) {
			if(textStatus == "success"){
				if(data !== "ERROR"){
					Refresh();
				}else{
					dhtmlx.alert({
						title:"ERROR",
						type:"alert-error",
						text:"An error occurred while editing a user"
					});	
					Refresh();
				}
			}
		});
	}
	function EliminarUsuario(selectedId){
		item = datastore.item(selectedId);
		// var values = {};
		// values.itemid      = item.itemid;
		item.eliminado   = 1;
		item.usuarioelimina = nombreusuario;
		delete item.id;
		// values.instruccion = "eliminar";
		dhtmlx.confirm({
			title: "Cerrar",
			type:"confirm-warning",
			text: "Quieres eliminar el elemento seleccionado?",
			ok: "SI",
			cancel: "NO",
			callback: function(result){
				if(result){
					$.post("../../node_hotel/usuarios_hotel?instruccion=eliminar",item, function(data, textStatus, xhr) {
						if(textStatus == "success"){
							if(data !== "ERROR"){
								Refresh();
							}else{
								dhtmlx.alert({
									title:"ERROR",
									type:"alert-error",
									text:"Ocurrio un error al eliminar usuario"
								});	
							}
						}
					});
				}
			}
		});	
	}
</script>
</html>