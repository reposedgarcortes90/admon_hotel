<?php 
require_once 'auth.php'; 
if($_GET){
	if(isset($_GET["idCliente"]) && !empty($_GET["idCliente"])){
		echo "<script>";
		echo "var idCliente = '".$_GET["idCliente"] ."';";
		if(isset($_GET["client_name"]) && !empty($_GET["client_name"])){
			echo "var client_name = '".$_GET["client_name"] ."';";
		}
		echo "</script>".PHP_EOL;
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Tipos de Habitaciones</title>
	<link rel="stylesheet" href="../codebase/dhtmlx.css">
	<script type="text/javascript" src="../js/moment.js"></script>
	<script src="../codebase/dhtmlx.js"></script>
    <link href="../css/fontawesome.css" rel="stylesheet">
    <link href="../css/brands.css" rel="stylesheet">
    <link href="../css/solid.css" rel="stylesheet">
    <!-- <script type="text/javascript" src="../js/jquery.min.js"></script> -->
    <script type="text/javascript" src="../js/jquery2.1.1.min.js"></script>
    <script type="text/javascript" src="../js/rutas_node.js"></script>
    <!-- <script src="../js/functions.js"></script> -->
	<link rel="stylesheet" href="../css/others_styles.css">
	<style>
html, body {
    width: 100%;
    height: 100%;
    margin: 0;
    padding: 0;
}
.pantalla_completa{
	z-index: 1005;
    margin-left: 0!important;
    margin-right: 0!important;
	width: 100%!important;
    height: 100%!important
}
.dhxwin_active{
    z-index: 1005;
    /* margin-left: 10%; */
    /* margin-right: 10%; */
	width: 80%;
    /* height: 100%; */
}
.dhxwin_inactive{
    /* margin-left: 10%;
    margin-right: 10%; */
	width: 80%;
	max-width:100%;
    /* height: 100%; */
}
.dhxwin_brd{
    left:2px!important;
    top:48px!important;
	width:calc(100% - 6px)!important;
	height:100px;
}
.dhx_cell_wins{
	width: calc(100% - 6px)!important;
}
.dhx_cell_cont_wins{
    left: 0px;
    top: 0px;
    overflow: auto;
	width: 100%!important;
	
}

.dhxform_base{
    width: 100%;
}
.dhxform_label{
    width: 40%!important;
}
.dhxform_control{
    width: 50%!important;
}
.dhxform_textarea{
    width: 100%!important;
}
.dhxcombo_material{
    width: 100%!important;
}
.dhxcombo_input{
    width: 100%!important;
}
.dhxform_btn{
    left:40%;
    width: 20%!important;
    text-align: center;
}
.dhxform_obj_material fieldset.dhxform_fs{
	width:calc(100% - 10px)!important
}
/* enabled, not checked */
.dhxform_obj_material div.dhxform_img.btn2state_0 {
	background-image: url("../button2state/material/toggle_off.png");
	width: 42px;
	height: 24px;
}
/* enabled, checked */
.dhxform_obj_material div.dhxform_img.btn2state_1 {
	background-image: url("../button2state/material/toggle_on.png");
	width: 42px;
	height: 24px;
}
/* disabled, not checked */
.dhxform_obj_material div.disabled div.dhxform_img.btn2state_0 {
	background-image: url("../button2state/material/toggle_off_dis.png");
	width: 42px;
	height: 24px;
}
/* disabled, checked */
.dhxform_obj_material div.disabled div.dhxform_img.btn2state_1 {
	background-image: url("../button2state/material/toggle_on_dis.png");
	width: 42px;
	height: 24px;
}

/* common */
/* fix label align a bit */
.dhxform_obj_material div.dhxform_item_label_right div.dhxform_label div.dhxform_label_nav_link {
	padding-top: 2px;
}
.dhxform_obj_material .dhxform_select{
	width:100%!important;
}
textarea[name="correos"]{
	width:200%!important;
}
.dhxtoolbar_input{
    top:7px;
}
.dhx_toolbar_material.dhxtoolbar_icons_18 div.dhx_toolbar_btn, .dhx_toolbar_material.dhxtoolbar_icons_18 div.dhx_toolbar_arw, .dhx_toolbar_material.dhxtoolbar_icons_18 div.dhx_toolbar_text{
    line-height:12px
}
div.gridbox .filter input, div.gridbox .filter select{
	width:calc(100% - 8px)!important;
}
#blah{
    width:100%;
    /* height:300px */
}
.hdr{
    margin-right: 0!important;
	padding-right: 0!important;
}
input[type="number"] {
  -webkit-appearance: textfield;
  -moz-appearance: textfield;
  appearance: textfield;
}

input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
  -webkit-appearance: none;
}

.number-input {
  border: 2px solid #3B83BD;
  display: inline-flex;
}

.number-input,
.number-input * {
  box-sizing: border-box;
}

.number-input button {
  outline:none;
  -webkit-appearance: none;
  background-color: #3B83BD;
  border: none;
  align-items: center;
  justify-content: center;
  width: 2rem;
  /* height: 3rem; */
  cursor: pointer;
  margin: 0;
  position: relative;
}


.number-input input[type=number] {
  /* font-family: 'Roboto'; */
  max-width: 3rem;
  /* padding: 5rem; */
  border: solid #ddd;
  border-width: 0 2px;
  /* font-size: 1rem; */
  /* height: 3rem; */
  font-weight: bold;
  text-align: center;
}

	</style>
</head>
<body onload="Cargar()"></body>

<script type="text/javascript">
	var layout,menu,empresas,nombre_catalogo, grid,json_form, datastore,item,toolbar,url_get_catalogo_elemento,dhxWindow,selectedId,desde,hasta,elementoseleccionado;
	// nombre_catalogo = catalogo;
	// json_form = "agregar_catalogo.json";
	// titulo_elemento = titulo;
	var formAdd;
    var fechaPrimerDia = moment(moment().subtract(2, 'days').format("YYYY-MM-DD")).format('YYYY-MM-DD');
	var fechaUltimoDia = moment().format("YYYY-MM-DD");
	url_get_catalogo_elemento = "../../node_hotel/tiposhabitaciones_hotel?instruccion=leer";

	LoadMenu();
	function Cargar()
	{
            // var neweact = [];
            // empresasactuales.forEach(element => {
            //     neweact[element.itemid] = element.nombre;
            // });
            // empresas = neweact;
            dhxWindow = new dhtmlXWindows("material");
            datastore= new dhtmlXDataStore({
                url:url_get_catalogo_elemento,
                datatype:"json"
            });
            layout = new dhtmlXLayoutObject({
                parent: document.body,
                pattern: "1C",
				offsets: {
					top: 0,
					right: 0,
					bottom: 0,
					left: 0
				},
            });
			layout.cells("a").hideHeader();
    
            toolbar = layout.cells("a").attachToolbar();
            // if(lectura == 0){
                // toolbar.addButton("agregar", 1, "<i class='fa fa-plus-circle' style='color:#0f9038' aria-hidden='true'></i>", null, null);
                // toolbar.addButton("editar", 2, "<i class='fa fa-pencil-alt' style='color:#2475ff' aria-hidden='true'></i>", null, null);
                // toolbar.addButton("eliminar", 3, "<i class='fa fa-trash-alt' style='color:#3869ad' aria-hidden='true'></i>", null, null);
            // }
            // toolbar.addButton("exportar_pdf", 4, "<i class='fa fa-file-pdf' style='color:#ff0000' aria-hidden='true'></i>", null, null);
            toolbar.addButton("exportar_excel", 5, "<i class='fa fa-file-excel' style='color:#0f9038' aria-hidden='true'></i>", null, null);
            toolbar.addButton("refrescar", 7, "<i class='fa fa-sync-alt' style='color:#0000ff;font-weight:bolder' aria-hidden='true'></i>", null, null);
            // toolbar.addSeparator("sep1",8);
            // toolbar.addInput("desde", 9, fechaPrimerDia, 80);		
            // toolbar.addInput("hasta", 10,fechaUltimoDia, 80);
            // toolbar.addButton("filtrar", 11, "<i class='fa fa-filter' style='color:#0000ff;font-weight:bolder' aria-hidden='true'></i>", null, null);
            // var desdeInputt = toolbar.getInput("desde");
            // desde = new dhtmlXCalendarObject([desdeInputt]);
            // desde.setDate(fechaPrimerDia);
            // desde.hideTime();
            // desde.showToday();
            // var hastaInputt = toolbar.getInput("hasta");
            // hasta = new dhtmlXCalendarObject([hastaInputt]);
            // hasta.setDate(fechaUltimoDia);
            // hasta.hideTime();
            // hasta.showToday();
            toolbar.attachEvent('onClick',function(id){
                if (id=="refrescar"){
                    Refresh();
                }
                if (id=="filtrar"){
                    let fecha_desde=desde.getDate(true);
                    let fecha_hasta=hasta.getDate(true);
                    Filtrar(fecha_desde,fecha_hasta);
                }
                if(id == "agregar"){
                    AgregarElemento();
                }
                if(id == "editar"){
                    selectedId = grid.getSelectedRowId();
                    if(selectedId !== null){
                        EditarElemento(selectedId);
                    }else{
                        dhtmlx.alert({
                            title:"Alerta",
                            type:"alert-warning",
                            text:"Debes seleccionar un elemento de la cuadricula primero"
                        });
                    }
                }
                if(id == "eliminar"){
                    selectedId = grid.getSelectedRowId();
                    if(selectedId !== null){
                        EliminarElemento(selectedId);
                    }else{
                        dhtmlx.alert({
                            title:"Alerta",
                            type:"alert-warning",
                            text:"Debes seleccionar un elemento de la cuadricula primero"
                        });
                    }
                }
                if(id == "exportar_pdf"){
                    grid.toPDF('../codebase/grid-pdf-php/generate.php');
                }
                if(id == "exportar_excel"){
                    grid.toExcel('../codebase/grid-excel-php/generate.php');	
                }
            });
    
            grid=layout.cells("a").attachGrid();
			grid.setHeader("Nombre,itemid,Tarifa",null,
			["text-align:center","text-align:center","text-align:center"]);
            grid.attachHeader("#text_filter,,");
            grid.setInitWidths("*,*");
            grid.setColAlign("center,center,center");
            grid.setColumnIds("nombre,itemid,tarifa");
            grid.setColSorting("str,str,int");
            grid.setColTypes("ro,ro,tarifa");
            grid.setColumnHidden(1, true);
            grid.enableContextMenu(menu);
            grid.setImagePath("../codebase/imgs/");
            grid.setEditable(false);
            grid.enableMultiline(true);
            grid.enableKeyboardSupport(true);
            grid.enableBlockSelection(true);
            grid.attachEvent("onKeyPress",onKeyPressed); 
            grid.init();
            grid.sync(datastore);
            grid.attachEvent("onBeforeSelect", onBeforeSelect);
            // grid.attachEvent("onEditCell", onEditCell);
	}
	function LoadMenu()
	{
		menu=new dhtmlXMenuObject();
		menu.renderAsContextMenu();
		menu.attachEvent("onClick", onButtonClick);
		menu.loadStruct("../data/xml/Registro_ContextMenuReportes.xml");
	}
	function onButtonClick(menuitemId, type) {
		if (menuitemId == "excel") {
			grid.toExcel('../codebase/grid-excel-php/generate.php');
		}

		if (menuitemId == "pdf") {
			grid.toPDF('../codebase/grid-pdf-php/generate.php');
		}
		if (menuitemId == "refresh") {
			Refresh();
		}
    }
    
    function onBeforeSelect(new_row,old_row,new_col_index){
        console.log(new_row,old_row,new_col_index)
        elementoseleccionado = datastore.item(new_row);
        console.log(elementoseleccionado,"elementoseleccionado")
        return true;
    }
	function onEditCell(stage,rId,cInd,nValue,oValue){
        console.log(stage,rId,cInd,nValue,oValue)
		//stage Estado del Editor
		//rId Id del Row
		//cInd Indice de la celda
		//nValue Valor nuevo solo si el stage es 2
		//oValue Valor viejo solo si el stage es 2	
        item = datastore.item(rId);
        console.log(item);
        if(stage == 0){//El editor esta abierto
            // EditarCellElemento(rId);
			if(cInd == 4) return true;
		}
        if(stage == 1){//El editor esta abierto
            if(cInd == 4){
                EditarCellElementoActivo(rId);
                return true;
            }
		}
        else if(stage == 2){//El editor esta cerrado
            console.log("entro")
			if(cInd != 3){
				EditarCellElemento(rId);
                return true;	
			}
		} 				
	}
	function Refresh(){
        // url_get_catalogo_elemento = node_chapel_web + "entradas_salidas?instruccion=get_registros&cliente="+client_id+"&fechadesde="+fechaPrimerDia+"&fechahasta="+fechaUltimoDia+"&tipoDocumento=4";
		datastore.clearAll();
			grid.clearAll();
			datastore.load(url_get_catalogo_elemento,function(){
			// grid.clearAll();
			grid.sync(datastore);
		});
    }
    function Filtrar(fecha_desde,fecha_hasta){
        console.log(fecha_desde,fecha_hasta);
        url_get_catalogo_elemento = node_chapel_web + "entradas_salidas?instruccion=get_registros&cliente="+client_id+"&fechadesde="+fecha_desde+"&fechahasta="+fecha_hasta+"&tipoDocumento=9";
		datastore.clearAll();
			grid.clearAll();
			datastore.load(url_get_catalogo_elemento,function(){
			grid.clearAll();
			grid.sync(datastore);
		});
	}
	function onKeyPressed(code,ctrl,shift){
		if(code==67&&ctrl){
			if (!grid._selectionArea){
				return dhtmlx.alert(
					{
						title:"Alerta",
						type:"alert",
						text:"Debes seleccionar un bloque de la cuadricula previamente"
					}
				);
			} 
			grid.setCSVDelimiter("\t");
			grid.copyBlockToClipboard();
		}
		return true;
	}
	function AgregarElemento(){
		// $.get(node_chapel_web + "get_uuid", function(uuid){
			var width = document.body.clientWidth - 200;
        	var height = document.body.clientHeight;
			var ventanaAgregar = dhxWindow.createWindow({
				id:"ventanaAgregar",
				// left:Number(window.innerWidth * 0.1),
				text:"<b>Agregar Tipo de Habitación</b>",
				// top:600,
				width:width,
				height:height,
				center:true,
				modal:true
			});
			dhxWindow.window("ventanaAgregar").button("minmax").attachEvent("onClick", function(win, button){
				var elemento = document.getElementsByClassName("dhxwin_active");
				for(var i = 0; i < elemento.length; i++){
					var elemento2 = document.getElementsByClassName("pantalla_completa");
					if(elemento2.length > 0){
						elemento[i].className = "dhxwin_active";
						dhxWindow.window("ventanaAgregar").setPosition(Number(window.innerWidth * 0.1), 0);
					}else{
						dhxWindow.window("ventanaAgregar").setPosition(0, 0);
						elemento[i].className += " pantalla_completa";
					}
				}
				return true;
			});
			document.body.onresize = ()=>{
				var width = document.body.clientWidth - 200;
				var height = document.body.clientHeight;
				try{
					dhxWindow.window("ventanaAgregar").setDimension(width, height);
				}catch(error){
				}
			};
			formAdd = ventanaAgregar.attachForm();
			formAdd.loadStruct("../data/json/agregar_tiposhabitaciones_hotel.json");
			formAdd.attachEvent("OnXLE", function(){
                formAdd.removeItem("itemid");
				formAdd.setItemValue("usuariocrea", nombreusuario);
				var input1 = formAdd.getContainer("input1");
                $("#"+input1.id).html(`
                <div class="number-input">
                    <button onclick="disminuir(this,'numeropersonas')" >
                        <i class="fa fa-minus" aria-hidden="true"></i>
                    </button>
                    <input class="quantity" min="0" name="q1"  value="0" id="q1"  type="number">
                    <button onclick="aumentar(this,'numeropersonas')" class="plus">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </button>
                </div>
                `);
                document.getElementById("q1").onkeydown=(a)=>{
                    console.log(a)
                    var numeros = "0123456789";
                    if(formAdd.isReadonly("input1")) return false;
                    if(a.key == "ArrowUp") return true; 
                    if(a.key == "ArrowDown") return true; 
                    if(numeros.includes(a.key)){
                        return true;
                    }else{
                        return false;
                    }
                };
                document.getElementById("q1").onkeyup=(a)=>{
                    if(!formAdd.isReadonly("input1")) formAdd.setItemValue("numeropersonas",Number(document.getElementById("q1").value));
                    return true;
                };
				document.getElementById("q1").min = 0;
				var input2 = formAdd.getContainer("input2");
                $("#"+input2.id).html(`
                <div class="number-input">
                    <button onclick="disminuir(this,'numerocamasind')" >
                        <i class="fa fa-minus" aria-hidden="true"></i>
                    </button>
                    <input class="quantity" min="0" name="q2"  value="0" id="q2"  type="number">
                    <button onclick="aumentar(this,'numerocamasind')" class="plus">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </button>
                </div>
                `);
                document.getElementById("q2").onkeydown=(a)=>{
                    console.log(a)
                    var numeros = "0123456789";
                    if(formAdd.isReadonly("input1")) return false;
                    if(a.key == "ArrowUp") return true; 
                    if(a.key == "ArrowDown") return true; 
                    if(numeros.includes(a.key)){
                        return true;
                    }else{
                        return false;
                    }
                };
                document.getElementById("q2").onkeyup=(a)=>{
                    if(!formAdd.isReadonly("input1")) formAdd.setItemValue("numerocamasind",Number(document.getElementById("q2").value));
                    return true;
                };
				document.getElementById("q2").min = 0;
				var input3 = formAdd.getContainer("input3");
                $("#"+input3.id).html(`
                <div class="number-input">
                    <button onclick="disminuir(this,'numerocamasqueen')" >
                        <i class="fa fa-minus" aria-hidden="true"></i>
                    </button>
                    <input class="quantity" min="0" name="q3"  value="0" id="q3"  type="number">
                    <button onclick="aumentar(this,'numerocamasqueen')" class="plus">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </button>
                </div>
                `);
                document.getElementById("q3").onkeydown=(a)=>{
                    console.log(a)
                    var numeros = "0123456789";
                    if(formAdd.isReadonly("input1")) return false;
                    if(a.key == "ArrowUp") return true; 
                    if(a.key == "ArrowDown") return true; 
                    if(numeros.includes(a.key)){
                        return true;
                    }else{
                        return false;
                    }
                };
                document.getElementById("q3").onkeyup=(a)=>{
                    if(!formAdd.isReadonly("input1")) formAdd.setItemValue("numerocamasqueen",Number(document.getElementById("q3").value));
                    return true;
                };
				document.getElementById("q3").min = 0;
				// var input4 = formAdd.getContainer("input4");
                // $("#"+input4.id).html(`
                // <div class="number-input">
                //     <button onclick="disminuir(this,'numerocamasking')" >
                //         <i class="fa fa-minus" aria-hidden="true"></i>
                //     </button>
                //     <input class="quantity" min="0" name="q4"  value="1" id="q4"  type="number">
                //     <button onclick="aumentar(this,'numerocamasking')" class="plus">
                //         <i class="fa fa-plus" aria-hidden="true"></i>
                //     </button>
                // </div>
                // `);
                // document.getElementById("q4").onkeydown=(a)=>{
                //     console.log(a)
                //     var numeros = "0123456789";
                //     if(formAdd.isReadonly("input1")) return false;
                //     if(a.key == "ArrowUp") return true; 
                //     if(a.key == "ArrowDown") return true; 
                //     if(numeros.includes(a.key)){
                //         return true;
                //     }else{
                //         return false;
                //     }
                // };
                // document.getElementById("q4").onkeyup=(a)=>{
                //     if(!formAdd.isReadonly("input1")) formAdd.setItemValue("numerocamasking",Number(document.getElementById("q4").value));
                //     return true;
                // };
                // document.getElementById("q4").min = 0;
				$("#"+formAdd.getContainer("uploading").id).html(`<form id="perfilform" enctype="multipart/form-data" method="post" action="/" runat="server">
					<input type='file' accept="image/*" name ="foto_perfil" id="imgInp"  style="display:none;vidibility:hidden;"/>
					<input type="text" name="nombre" id="nombre" value="foto_tipohabitacion" style="display:none;vidibility:hidden;"/>
					<input type="text" name="usuario" id="usuario" value="`+nombreusuario+`" style="display:none;vidibility:hidden;"/>
					<img id="blah"  src="../../node_hotel/mostrarArchivo?tipo=tipoHabitacion&archivo=foto_tipohabitacion.jpg" alt="your image" />
					<input type="submit" value="Submit" id="btnSubmit" style="display:none;vidibility:hidden;"/>
				</form>
				`);
				$("#blah").click(function() {
					$("#imgInp").click();
				});
				$("#imgInp").change(function() {
					readURL(this);
				});
				$("#btnSubmit").click(function (event) {

					//stop submit the form, we will post it manually.
					event.preventDefault();

					// Get form
					var form = $('#perfilform')[0];

					// Create an FormData object
					var data = new FormData(form);

					var archivo = $("#imgInp").val();
					// console.log(archivo);

					if( archivo === "" ) return console.log("Sin foto");
					// If you want to add an extra field for the FormData
					data.append("CustomField", "This is some extra data, testing");

					// disabled the submit button
					$("#btnSubmit").prop("disabled", true);

					$.ajax({
						type: "POST",
						enctype: 'multipart/form-data',
						url: "../../node_hotel/upload",
						data: data,
						processData: false,
						contentType: false,
						cache: false,
						timeout: 600000,
						success: function (data) {

							$("#result").text(data);
							console.log("SUCCESS : ", data);
							$("#btnSubmit").prop("disabled", false);

						},
						error: function (e) {

							$("#result").text(e.responseText);
							console.log("ERROR : ", e);
							$("#btnSubmit").prop("disabled", false);

						}
					});

				});
			});
			formAdd.attachEvent("onButtonClick", function(name){
				if (name=="enviar"){
					if(formAdd.validate()){
						var values = formAdd.getFormData(true);
						var val = {};
						val.usuariocrea = nombreusuario;
						val.nombre = values.nombre;
						val.tarifa = values.tarifa;
						console.log(values)
						$.post("../../node_hotel/tiposhabitaciones_hotel?instruccion=agregar",val, function(data, textStatus, xhr) {
                            console.log(data);
							if(textStatus == "success"){
								if(data !== "ERROR"){
									Refresh();
									$("#nombre").val("foto_tipoHabitacion"+data.insertId);
                                	$("#btnSubmit").click();
									ventanaAgregar.close();
								}else{
                                    Refresh();
                                    dhtmlx.alert({
                                        title:"ERROR",
                                        type:"alert-warning",
                                        text:"Ocurrio un error al agregar Tipo de Habitación"
                                    });	
								}
							}
						});
					}
				}
			});
			formAdd.enableLiveValidation(true);			
		// });
	}
	function EditarElemento(selectedId){
		item = datastore.item(selectedId);
        delete item.id;
        console.log(item);
        var width = document.body.clientWidth - 200;
        var height = document.body.clientHeight;
		var ventanaEditar = dhxWindow.createWindow({
			id:"ventanaEditar",
			// left:Number(window.innerWidth * 0.1),
			text:"<b>Editar Tipo de Habitación</b>",
			// top:600,
			width:width,
			height:height,
			center:true,
			modal:true
        });
        
        dhxWindow.window("ventanaEditar").setPosition(100, 0);
		dhxWindow.window("ventanaEditar").button("minmax").attachEvent("onClick", function(win, button){
			console.log(win, button);
			var elemento = document.getElementsByClassName("dhxwin_active");
			for(var i = 0; i < elemento.length; i++){
				var elemento2 = document.getElementsByClassName("pantalla_completa");
				if(elemento2.length > 0){
					elemento[i].className = "dhxwin_active";
					dhxWindow.window("ventanaEditar").setPosition(100, 0);
				}else{
					dhxWindow.window("ventanaEditar").setPosition(0, 0);
					elemento[i].className += " pantalla_completa";
				}
			}
			return true;
		});
		document.body.onresize = ()=>{
            var width = document.body.clientWidth - 200;
            var height = document.body.clientHeight;
			try{
				dhxWindow.window("ventanaEditar").setDimension(width, height);
			}catch(error){
			}
        };
		formAdd = ventanaEditar.attachForm();
		formAdd.loadStruct("../data/json/agregar_tiposhabitaciones_hotel.json");
		formAdd.attachEvent("OnXLE", function(){
            formAdd.setItemValue("itemid", item.itemid);
            // formAdd.setItemValue("activo", item.activo);
			formAdd.setItemValue("nombre", item.nombre);
			formAdd.setItemValue("tarifa", item.tarifa);
			var input1 = formAdd.getContainer("input1");
			$("#"+input1.id).html(`
			<div class="number-input">
				<button onclick="disminuir(this,'numeropersonas')" >
					<i class="fa fa-minus" aria-hidden="true"></i>
				</button>
				<input class="quantity" min="0" name="q1"  value="`+item.numeropersonas+`" id="q1"  type="number">
				<button onclick="aumentar(this,'numeropersonas')" class="plus">
					<i class="fa fa-plus" aria-hidden="true"></i>
				</button>
			</div>
			`);
			document.getElementById("q1").onkeydown=(a)=>{
				console.log(a)
				var numeros = "0123456789";
				if(formAdd.isReadonly("input1")) return false;
				if(a.key == "ArrowUp") return true; 
				if(a.key == "ArrowDown") return true; 
				if(numeros.includes(a.key)){
					return true;
				}else{
					return false;
				}
			};
			document.getElementById("q1").onkeyup=(a)=>{
				if(!formAdd.isReadonly("input1")) formAdd.setItemValue("numeropersonas",Number(document.getElementById("q1").value));
				return true;
			};
			document.getElementById("q1").min = 0;
			var input2 = formAdd.getContainer("input2");
			$("#"+input2.id).html(`
			<div class="number-input">
				<button onclick="disminuir(this,'numerocamasind')" >
					<i class="fa fa-minus" aria-hidden="true"></i>
				</button>
				<input class="quantity" min="0" name="q2"  value="`+item.numerocamasind+`" id="q2"  type="number">
				<button onclick="aumentar(this,'numerocamasind')" class="plus">
					<i class="fa fa-plus" aria-hidden="true"></i>
				</button>
			</div>
			`);
			document.getElementById("q2").onkeydown=(a)=>{
				console.log(a)
				var numeros = "0123456789";
				if(formAdd.isReadonly("input1")) return false;
				if(a.key == "ArrowUp") return true; 
				if(a.key == "ArrowDown") return true; 
				if(numeros.includes(a.key)){
					return true;
				}else{
					return false;
				}
			};
			document.getElementById("q2").onkeyup=(a)=>{
				if(!formAdd.isReadonly("input1")) formAdd.setItemValue("numerocamasind",Number(document.getElementById("q2").value));
				return true;
			};
			document.getElementById("q2").min = 0;
			var input3 = formAdd.getContainer("input3");
			$("#"+input3.id).html(`
			<div class="number-input">
				<button onclick="disminuir(this,'numerocamasqueen')" >
					<i class="fa fa-minus" aria-hidden="true"></i>
				</button>
				<input class="quantity" min="0" name="q3"  value="`+item.numerocamasqueen+`" id="q3"  type="number">
				<button onclick="aumentar(this,'numerocamasqueen')" class="plus">
					<i class="fa fa-plus" aria-hidden="true"></i>
				</button>
			</div>
			`);
			document.getElementById("q3").onkeydown=(a)=>{
				console.log(a)
				var numeros = "0123456789";
				if(formAdd.isReadonly("input1")) return false;
				if(a.key == "ArrowUp") return true; 
				if(a.key == "ArrowDown") return true; 
				if(numeros.includes(a.key)){
					return true;
				}else{
					return false;
				}
			};
			document.getElementById("q3").onkeyup=(a)=>{
				if(!formAdd.isReadonly("input1")) formAdd.setItemValue("numerocamasqueen",Number(document.getElementById("q3").value));
				return true;
			};
			document.getElementById("q3").min = 0;
			$("#"+formAdd.getContainer("uploading").id).html(`<form id="perfilform" enctype="multipart/form-data" method="post" action="/" runat="server">
                <input type='file' accept="image/*" name ="foto_perfil" id="imgInp"  style="display:none;vidibility:hidden;"/>
                <input type="text" name="nombre" value="foto_tipoHabitacion`+item.itemid+`" style="display:none;vidibility:hidden;"/>
                <img id="blah"  src="../../node_hotel/mostrarArchivo?tipo=Perfil&archivo=foto_tipoHabitacion`+item.itemid+`.jpg" alt="your image" />
                <input type="submit" value="Submit" id="btnSubmit" style="display:none;vidibility:hidden;"/>
            </form>
            `);
            $("#blah").click(function() {
                $("#imgInp").click();
            });
            $("#imgInp").change(function() {
                readURL(this);
            });
            $("#btnSubmit").click(function (event) {

                //stop submit the form, we will post it manually.
                event.preventDefault();

                // Get form
                var form = $('#perfilform')[0];

                // Create an FormData object
                var data = new FormData(form);
                var archivo = $("#imgInp").val();
                // console.log(archivo);

                if( archivo === "" ) return console.log("Sin foto");
                // If you want to add an extra field for the FormData
                data.append("CustomField", "This is some extra data, testing");

                // disabled the submit button
                $("#btnSubmit").prop("disabled", true);

                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: "../../node_hotel/upload",
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function (data) {

                        $("#result").text(data);
                        console.log("SUCCESS : ", data);
                        $("#btnSubmit").prop("disabled", false);

                    },
                    error: function (e) {

                        $("#result").text(e.responseText);
                        console.log("ERROR : ", e);
                        $("#btnSubmit").prop("disabled", false);

                    }
                });

            });
		});
		formAdd.attachEvent("onButtonClick", function(name){
			if (name=="enviar"){
				if(formAdd.validate()){
                    var values = formAdd.getFormData(true);
					var val = {};
					val.usuarioedita = nombreusuario;
					val.itemid = values.itemid;
					val.nombre = values.nombre;
					val.tarifa = values.tarifa;
					val.numerocamasind = values.numerocamasind;
					val.numeropersonas = values.numeropersonas;
					val.numerocamasqueen = values.numerocamasqueen;
                    $.post("../../node_hotel/tiposhabitaciones_hotel?instruccion=actualizar",val, function(data, textStatus, xhr) {
                        if(data !== "ERROR"){
							Refresh();
							$("#btnSubmit").click();
							ventanaEditar.close();
                        }else{
                            dhtmlx.alert({
                                title:"ERROR",
                                type:"alert-error",
                                text:"Ocurrio un error al editar"
                            });	
                        }
                    });					
				}
			}
		});
		formAdd.enableLiveValidation(true);	
	}
	function EliminarElemento(selectedId){
		item = datastore.item(selectedId);
		delete item.id;
		dhtmlx.confirm({
			title: "Confirmación",
			type:"confirm-warning",
			text: "Desea eliminar el registro seleccionado?",
			ok: "Si",
			cancel: "No",
			callback: function(result){
				if(result){		
                    item.usuarioelimina = nombreusuario;			
					$.post("../../node_hotel/tiposhabitaciones_hotel?instruccion=eliminar",item, function(data, textStatus, xhr) {
						if(textStatus == "success"){
							if(data !== "ERROR"){
								Refresh();
							}else{
								dhtmlx.alert({
									title:"ERROR",
									type:"alert-error",
									text:"Ocurrio un error al eliminar registro"
								});	
								Refresh();
							}
						}
					});
				}
			}
		});	
	}
	function EditarCellElemento(selectedId){
		item = datastore.item(selectedId);
		delete item.id;
        // item.instruccion = "ud";
        console.log(item);
        var empresa = grid.cells(selectedId,3).getValue();
        item.empresa = empresa;
		$.post("../../node_hotel/clientes_hotel?instruccion=actualizar",item, function(data, textStatus, xhr) {
			if(textStatus == "success"){
				if(data !== "ERROR"){
					Refresh();
				}else{
                    dhtmlx.alert({
                        title:"Warning",
                        type:"alert-warning",
                        text:"Ha ocurrido un error al editar el elemento"
                    });	
                    Refresh();	
				}
			}
		});	
    }
    function EditarCellElementoActivo(selectedId){
		item = datastore.item(selectedId);
		delete item.id;
        item.instruccion = "ud";
        if(item.activo == 1) item.activo = 0;
        else item.activo = 1;
		$.post("../../node_hotel/clientes_hotel?instruccion=actualizar",item, function(data, textStatus, xhr) {
			if(textStatus == "success"){
				if(data !== "ERROR"){
					Refresh();
				}else{
                    dhtmlx.alert({
                        title:"Warning",
                        type:"alert-warning",
                        text:"Ha ocurrido un error al editar el elemento"
                    });	
                    Refresh();	
				}
			}
		});	
    }

    function eXcell_es(cell){ //the eXcell name is defined here
        if (cell){                // the default pattern, just copy it
            this.cell = cell;
            this.grid = this.cell.parentNode.grid;
        }
        this.edit = function(){}  //read-only cell doesn't have edit method
        // the cell is read-only, so it's always in the disabled state
        this.isDisabled = function(){ return true; }
        this.setValue=function(val){
            let icono,row_id;
            row_id = this.cell.parentNode.idd;
            item = datastore.item(row_id);
			if(item.entrada_salida == 0){
                this.cell.style.color = "green";
                icono = `<i style="font-weigth:bolder;font-size:1.5em;" class="fa fa-arrow-circle-left" aria-hidden="true"></i> <span style="color:black;font-weight:bolder;font-size:2em;">|</span>`;
			}else{
                this.cell.style.color = "red";
                icono = `<span style="color:black;font-weight:bolder;font-size:2em;">|</span> <i style="font-weigth:bolder;font-size:1.5em;" class="fa fa-arrow-circle-right" aria-hidden="true"></i>`;
			}
			this.setCValue(icono,val);
        }
    }
    eXcell_es.prototype = new eXcell;// nests all other methods from the base class
    function eXcell_empresa(cell){ //the eXcell name is defined here
        if (cell){                // the default pattern, just copy it
            this.cell = cell;
            this.grid = this.cell.parentNode.grid;
            // eXcell_ed.call(this);
        }
        /* this.edit = function(){
            this.val = this.getValue(); // save current value
            // alert(this.val)
            var valor = this.val;
            var html = "<select class='select1' style='width:150px;'>";
            html += "<option value=''>ND</option>";
            empresas.forEach((element,i) => {
                if(this.val == element) html += "<option value='"+i+"' selected>" + element + "</option>";
                else html += "<option value='"+i+"'>" + element + "</option>";
            });
            this.cell.innerHTML = html + "</select>"; // editor's html
            // this.cell.firstChild.value=parseInt(val); // set the first part of data
            // if (val.indexOf("PM")!=-1) this.cell.childNodes[1].value="PM";
            // blocks onclick event
            this.cell.childNodes[0].onclick=function(e){ (e||event).cancelBubble=true;} 
            console.log(this.cell.childNodes[0],"this.cell.childNodes")
            // $('.select1').click();
            // this.cell.childNodes[0].onclick=function(e){ 
            //     alert("clicko")
            //     (e||event).cancelBubble=true;
            // } 
            // blocks onclick event
            // this.cell.childNodes[1].onclick=function(e){ (e||event).cancelBubble=true;}
        }  //read-only cell doesn't have edit method */
        // the cell is read-only, so it's always in the disabled state
        this.isDisabled = function(){ return true; }
        /* this.detach=function(){
            console.log("detach",this)
            console.log(this.cell.childNodes);
            this.setValue(this.cell.childNodes[0].value);
            return this.val!=this.getValue();
            // sets the new value
            // this.setValue(this.cell.childNodes[0].value+" "+this.cell.childNodes[1].value); 
            // return this.val!=this.getValue(); // compares the new and the old values
        } */
        this.getValue=function(){
            return this.cell.innerHTML; // get value
        }
        this.setValue=function(val){
            if(typeof empresas[val] != "undefined") this.setCValue(empresas[val],val);
            else this.setCValue("",val);
        }
    }
	eXcell_empresa.prototype = new eXcell;// nests all other methods from the base class
	function mostrarImagen(archivo,tipo){
		console.log("se abrirá la imagen",archivo,tipo);
		var width = Number(window.innerWidth / 2);
		var ventanaIntercambio = dhxWindow.createWindow({
			id:"abrirImage",
			// left:Number(window.innerWidth * 0.1),
			text:"Imagen",
			// top:0,
			width:width,
			height:600,
			center:true,
			modal:true
		});
		dhxWindow.window("abrirImage").button("minmax").attachEvent("onClick", function(win, button){

			var elemento = document.getElementsByClassName("dhxwin_active");
			for(var i = 0; i < elemento.length; i++){
				var elemento2 = document.getElementsByClassName("pantalla_completa");
				if(elemento2.length > 0){
					elemento[i].className = "dhxwin_active";
					dhxWindow.window("abrirImage").setPosition(Number(window.innerWidth * 0.1), 0);
				}else{
					dhxWindow.window("abrirImage").setPosition(0, 0);
					elemento[i].className += " pantalla_completa";
				}
			}
			return true;
		});
		dhxWindow.window("abrirImage").attachHTMLString("<div style='width:calc(100%)!important;height:100%!important'><iframe allowfullscreen src='"+node_chapel_web +"mostrarArchivo?tipo="+tipo+"&archivo="+archivo+"' width='100%' height='100%'"+"></iframe></div>");
		ventanaIntercambio.button("close").attachEvent("onClick", function(){
			// Refresh();
			return true; // deny default action
		});
	}
	function eXcell_fotito(cell){ //the eXcell name is defined here
        if (cell){                // the default pattern, just copy it
            this.cell = cell;
            this.grid = this.cell.parentNode.grid;
        }
        this.edit = function(){}  //read-only cell doesn't have edit method
        // the cell is read-only, so it's always in the disabled state
        this.isDisabled = function(){ return true; }
        this.setValue=function(val){
            let icono,row_id;
            row_id = this.cell.parentNode.idd;
			item = datastore.item(row_id);
			console.log(item.fotos);
			if(item.fotos && item.fotos != ""){
				let fotos = JSON.parse(item.fotos);
				let todaslasfotos = "";
				for (let key in fotos) {
					console.log(key);
					if (fotos.hasOwnProperty(key)) {
						let element = fotos[key];	
						console.log(element);
						for (const llave in element) {
							if (element.hasOwnProperty(llave)) {
								let element2 = element[llave];								
								console.log(element2,llave);					
								let foto = `<i style="font-weigth:bolder;font-size:1.5em;display:inline-block;padding-right:1px" onclick="mostrarImagen('`+element2+`','Imagen')" class="fa fa-picture-o" aria-hidden="true"></i>`;
								todaslasfotos += foto;
							}
						}
					}
				}
				//tipo=Imagen,archivo=foto
				this.cell.style.color = "green";
				this.setCValue("<div style='width:100%;'>" + todaslasfotos + "</div>",fotos.length);
			}
        }
    }
	eXcell_fotito.prototype = new eXcell;// nests all other methods from the base class
	function eXcell_tarifa(cell){ //the eXcell name is defined here
        if (cell){                // the default pattern, just copy it
            this.cell = cell;
            this.grid = this.cell.parentNode.grid;
        }
        this.edit = function(){}  //read-only cell doesn't have edit method
        // the cell is read-only, so it's always in the disabled state
        this.isDisabled = function(){ return true; }
        this.setValue=function(val){
            
				this.cell.style.color = "green";
				this.setCValue("<div style='width:100%;'><b style='color:black'>$</b> " + val + "</div>",val);
			
        }
    }
    eXcell_tarifa.prototype = new eXcell;// nests all other methods from the base class
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
            $('#blah').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
	}
	function disminuir(esto,elemento){
        if(!esto.parentNode.querySelector('input[type=number]').readOnly){
            esto.parentNode.querySelector('input[type=number]').stepDown();
            formAdd.setItemValue(elemento,Number(esto.parentNode.querySelector('input[type=number]').value));
        }
    }
    function aumentar(esto,elemento){
        if(!esto.parentNode.querySelector('input[type=number]').readOnly){
            esto.parentNode.querySelector('input[type=number]').stepUp();
            formAdd.setItemValue(elemento,Number(esto.parentNode.querySelector('input[type=number]').value));
        }
    }
</script>
</html>