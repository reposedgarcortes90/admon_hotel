<?php 
require_once 'auth.php'; 
if($_GET){
	if(isset($_GET["idCliente"]) && !empty($_GET["idCliente"])){
		echo "<script>";
		echo "var idCliente = '".$_GET["idCliente"] ."';";
		if(isset($_GET["client_name"]) && !empty($_GET["client_name"])){
			echo "var client_name = '".$_GET["client_name"] ."';";
		}
		echo "</script>".PHP_EOL;
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Hospedajes</title>
	<link rel="stylesheet" href="../codebase/dhtmlx.css">
	<script type="text/javascript" src="../js/moment.js"></script>
	<script src="../codebase/dhtmlx.js"></script>
    <link href="../css/fontawesome.css" rel="stylesheet">
    <link href="../css/brands.css" rel="stylesheet">
    <link href="../css/solid.css" rel="stylesheet">
	<script src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="../js/rutas_node.js"></script>
    <!-- <script src="../js/functions.js"></script> -->
    <link rel="stylesheet" href="../css/others_styles.css">
    <link rel="stylesheet" href="../codebase/fonts/font_roboto/roboto.css">
	<style>
html, body {
    font-family:'Roboto';
    width: 100%;
    height: 100%;
    margin: 0;
    padding: 0;
}
.pantalla_completa{
	z-index: 1005;
    margin-left: 0!important;
    margin-right: 0!important;
	width: 100%!important;
    height: 100%!important
}
.dhxwin_active{
    z-index: 1005;
    /* margin-left: 10%; */
    /* margin-right: 10%; */
	width: 80%;
    /* height: 100%; */
}
.dhxwin_inactive{
    /* margin-left: 10%;
    margin-right: 10%; */
	width: 80%;
	max-width:100%;
    /* height: 100%; */
}
.dhxwin_brd{
    left:2px!important;
    top:48px!important;
	width: calc(100% - 6px)!important;
	height:100px;
}
.dhx_cell_wins{
	width: calc(100% - 6px)!important;
}
.dhx_cell_cont_wins{
    left: 0px;
    top: 0px;
    overflow: auto;
	width: 100%!important;
	
}

.dhxform_base{
    width: 99%;
}
.dhxform_label{
    width: 40%!important;
}
.dhxform_control{
    width: 50%!important;
}
.dhxform_textarea{
    width: 100%!important;
}
.dhxcombo_material{
    width: 100%!important;
}
.dhxcombo_input{
    width: 100%!important;
}
.dhxform_btn{
    left:40%;
    width: 20%!important;
    text-align: center;
}
.dhxform_obj_material fieldset.dhxform_fs{
	width:calc(100% - 10px)!important
}
/* enabled, not checked */
.dhxform_obj_material div.dhxform_img.btn2state_0 {
	background-image: url("../button2state/material/toggle_off.png");
	width: 42px;
	height: 24px;
}
/* enabled, checked */
.dhxform_obj_material div.dhxform_img.btn2state_1 {
	background-image: url("../button2state/material/toggle_on.png");
	width: 42px;
	height: 24px;
}
/* disabled, not checked */
.dhxform_obj_material div.disabled div.dhxform_img.btn2state_0 {
	background-image: url("../button2state/material/toggle_off_dis.png");
	width: 42px;
	height: 24px;
}
/* disabled, checked */
.dhxform_obj_material div.disabled div.dhxform_img.btn2state_1 {
	background-image: url("../button2state/material/toggle_on_dis.png");
	width: 42px;
	height: 24px;
}

/* common */
/* fix label align a bit */
.dhxform_obj_material div.dhxform_item_label_right div.dhxform_label div.dhxform_label_nav_link {
	padding-top: 2px;
}
.dhxform_obj_material .dhxform_select{
	width:100%!important;
}
textarea[name="correos"]{
	width:200%!important;
}
.dhxtoolbar_input{
    top:7px;
}
.dhx_toolbar_material.dhxtoolbar_icons_18 div.dhx_toolbar_btn, .dhx_toolbar_material.dhxtoolbar_icons_18 div.dhx_toolbar_arw, .dhx_toolbar_material.dhxtoolbar_icons_18 div.dhx_toolbar_text{
    line-height:12px
}
div.gridbox .filter input, div.gridbox .filter select, div.combo{
	width:calc(100% - 8px)!important;
}
input[type="number"] {
  -webkit-appearance: textfield;
  -moz-appearance: textfield;
  appearance: textfield;
}

input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
  -webkit-appearance: none;
}

.number-input {
  border: 2px solid #3B83BD;
  display: inline-flex;
}

.number-input,
.number-input * {
  box-sizing: border-box;
}

.number-input button {
  outline:none;
  -webkit-appearance: none;
  background-color: #3B83BD;
  border: none;
  align-items: center;
  justify-content: center;
  width: 2rem;
  /* height: 3rem; */
  cursor: pointer;
  margin: 0;
  position: relative;
}


.number-input input[type=number] {
  /* font-family: 'Roboto'; */
  max-width: 3rem;
  /* padding: 5rem; */
  border: solid #ddd;
  border-width: 0 2px;
  /* font-size: 1rem; */
  /* height: 3rem; */
  font-weight: bold;
  text-align: center;
}

	</style>
</head>
<body onload="Cargar()"></body>

<script type="text/javascript">
	var layout,menu,empresas,nombre_catalogo, grid,json_form, datastore,item,toolbar,url_get_catalogo_elemento,dhxWindow,selectedId,desde,hasta,elementoseleccionado;
	// nombre_catalogo = catalogo;
	// json_form = "agregar_catalogo.json";
    // titulo_elemento = titulo;
    var formAdd;
    var fechaPrimerDia = moment(moment().subtract(2, 'days').format("YYYY-MM-DD")).format('YYYY-MM-DD');
	var fechaUltimoDia = moment().format("YYYY-MM-DD");
	url_get_catalogo_elemento = "../../node_hotel/hospedajes_hotel?instruccion=leer";
    var habitaciones,clientes;
	LoadMenu();
	function Cargar()
	{
        $.get("../../node_hotel/habitaciones_hotel?instruccion=leer",function(hab){
            $.get("../../node_hotel/clientes_hotel?instruccion=leer",function(cli){
                clientes = cli;
                habitaciones = hab;
                dhxWindow = new dhtmlXWindows("material");
                datastore= new dhtmlXDataStore({
                    url:url_get_catalogo_elemento,
                    datatype:"json"
                });
                layout = new dhtmlXLayoutObject({
                    parent: document.body,
                    pattern: "1C",
                    offsets: {
                        top: 0,
                        right: 0,
                        bottom: 0,
                        left: 0
                    },
                });
                layout.cells("a").hideHeader();
        
                toolbar = layout.cells("a").attachToolbar();
                // if(lectura == 0){
                    toolbar.addButton("agregar", 1, "<i class='fa fa-plus-circle' style='color:#0f9038' aria-hidden='true'></i>", null, null);
                    toolbar.addButton("editar", 2, "<i class='fa fa-pencil-alt' style='color:#2475ff' aria-hidden='true'></i>", null, null);
                    toolbar.addButton("eliminar", 3, "<i class='fa fa-trash-alt' style='color:#3869ad' aria-hidden='true'></i>", null, null);
                // }
                // toolbar.addButton("exportar_pdf", 4, "<i class='fa fa-file-pdf' style='color:#ff0000' aria-hidden='true'></i>", null, null);
                toolbar.addButton("exportar_excel", 5, "<i class='fa fa-file-excel' style='color:#0f9038' aria-hidden='true'></i>", null, null);
                toolbar.addButton("refrescar", 7, "<i class='fa fa-sync-alt' style='color:#0000ff;font-weight:bolder' aria-hidden='true'></i>", null, null);
                toolbar.attachEvent('onClick',function(id){
                    if (id=="refrescar"){
                        Refresh();
                    }
                    if (id=="filtrar"){
                        let fecha_desde=desde.getDate(true);
                        let fecha_hasta=hasta.getDate(true);
                        Filtrar(fecha_desde,fecha_hasta);
                    }
                    if(id == "agregar"){
                        AgregarElemento();
                    }
                    if(id == "editar"){
                        selectedId = grid.getSelectedRowId();
                        if(selectedId !== null){
                            EditarElemento(selectedId);
                        }else{
                            dhtmlx.alert({
                                title:"Alerta",
                                type:"alert-warning",
                                text:"Debes seleccionar un elemento de la cuadricula primero"
                            });
                        }
                    }
                    if(id == "eliminar"){
                        selectedId = grid.getSelectedRowId();
                        if(selectedId !== null){
                            EliminarElemento(selectedId);
                        }else{
                            dhtmlx.alert({
                                title:"Alerta",
                                type:"alert-warning",
                                text:"Debes seleccionar un elemento de la cuadricula primero"
                            });
                        }
                    }
                    if(id == "exportar_pdf"){
                        grid.toPDF('../codebase/grid-pdf-php/generate.php');
                    }
                    if(id == "exportar_excel"){
                        grid.toExcel('../codebase/grid-excel-php/generate.php');	
                    }
                });
        
                grid=layout.cells("a").attachGrid();
    
                var header = [
                    "Habitación",//habitacion
                    "itemid",//itemid
                    "Cliente",//cliente
                    "RFC",//rfc
                    "F. Hospedaje",//fechahospedaje
                    "Personas",//personas
                    // "Cantidad de personas",//cantidadespersonas
                    "Dias",//diashospedaje
                    "Costo",//costo
                    "IVA",//iva
                    "IMPUESTO SOBRE HOSPEDAJE",//impsobrehosp
                    "TOTAL",//grantotal
                    "Comentarios",//comentarios
                    "Usuario",//usuariocrea
                    "F. Creación"//fechacrea
                ];
                var styles = [
                    "text-align:left",//habitacion
                    "text-align:center",//itemid
                    "text-align:left",//cliente
                    "text-align:center",//rfc
                    "text-align:center",//fechahospedaje
                    "text-align:center",//personas
                    // "text-align:center",//cantidadespersonas
                    "text-align:center",//diashospedaje
                    "text-align:center",//costo
                    "text-align:center",//iva
                    "text-align:center",//impsobrehosp
                    "text-align:center",//grantotal
                    "text-align:left",//comentarios
                    "text-align:center",//usuariocrea
                    "text-align:center"//fechacrea
                ];
                var filtros = [
                    "#text_filter",//habitacion
                    "#text_filter",//itemid
                    "#combo_filter",//cliente
                    "#text_filter",//rfc
                    "#text_filter",//fechahospedaje
                    "#text_filter",//personas
                    // "#text_filter",//cantidadespersonas
                    "#text_filter",//diashospedaje
                    "#text_filter",//costo
                    "#text_filter",//iva
                    "#text_filter",//impsobrehosp
                    "#text_filter",//grantotal
                    "#text_filter",//comentarios
                    "#combo_filter",//usuariocrea
                    "#text_filter"//fechacrea
                ];
                var widths = [
                    "*",//habitacion
                    "*",//itemid
                    "*",//cliente
                    "*",//rfc
                    "*",//fechahospedaje
                    "200",//personas
                    // "*",//cantidadespersonas
                    "*",//diashospedaje
                    "*",//costo
                    "*",//iva
                    "*",//impsobrehosp
                    "*",//grantotal
                    "*",//comentarios
                    "*",//usuariocrea
                    "*"//fechacrea
                ];
                var colalign = [
                    "center",//habitacion
                    "left",//itemid
                    "center",//cliente
                    "left",//rfc
                    "center",//fechahospedaje
                    "center",//personas
                    // "center",//cantidadespersonas
                    "center",//diashospedaje
                    "center",//costo
                    "center",//iva
                    "center",//impsobrehosp
                    "center",//grantotal
                    "left",//comentarios
                    "center",//usuariocrea
                    "center"//fechacrea
                ];
                var columns = [
                    "habitacion",
                    "itemid",
                    "cliente",
                    "rfc",
                    "fechahospedaje",
                    "personas",
                    // "cantidadespersonas",
                    "diashospedaje",
                    "costo",
                    "iva",
                    "impsobrehosp",
                    "grantotal",
                    "comentarios",
                    "usuariocrea",
                    "fechacrea"
                ];
                var sorting = [
                    "str",//habitacion
                    "str",//itemid
                    "str",//cliente
                    "str",//rfc
                    "str",//fechahospedaje
                    "str",//personas
                    // "str",//cantidadespersonas
                    "str",//diashospedaje
                    "str",//costo
                    "str",//iva
                    "str",//impsobrehosp
                    "str",//grantotal
                    "str",//comentarios
                    "str",//usuariocrea
                    "str"//fechacrea
                ];
                var ctypes = [
                    "habitacion",//habitacion
                    "ro",//itemid
                    "cliente",//cliente
                    "ro",//rfc
                    "ro",//fechahospedaje
                    "personas",//personas
                    // "cantidadespersonas",//cantidadespersonas
                    "ro",//diashospedaje
                    "monto",//costo
                    "monto",//iva
                    "monto",//impsobrehosp
                    "monto",//grantotal
                    "ro",//comentarios
                    "ro",//usuariocrea
                    "fecha"//fechacrea
                ];
                grid.setHeader(header.join(","),null,
                styles);
                grid.attachHeader(filtros.join(","));
                grid.setInitWidths(widths.join(","));
                grid.setColAlign(colalign.join(","));
                grid.setColumnIds(columns.join(","));
                grid.setColSorting(sorting.join(","));
                grid.setColTypes(ctypes.join(","));
                grid.setColumnHidden(1, true);
                grid.enableContextMenu(menu);
                grid.setImagePath("../codebase/imgs/");
                grid.setEditable(false);
                grid.enableMultiline(true);
                grid.enableKeyboardSupport(true);
                grid.enableBlockSelection(true);
                grid.attachEvent("onKeyPress",onKeyPressed); 
                grid.init();
                grid.sync(datastore);
                grid.attachEvent("onBeforeSelect", onBeforeSelect);
                // grid.attachEvent("onEditCell", onEditCell);
            });
        });
	}
	function LoadMenu()
	{
		menu=new dhtmlXMenuObject();
		menu.renderAsContextMenu();
		menu.attachEvent("onClick", onButtonClick);
		menu.loadStruct("../data/xml/Registro_ContextMenuReportes.xml");
	}
	function onButtonClick(menuitemId, type) {
		if (menuitemId == "excel") {
			grid.toExcel('../codebase/grid-excel-php/generate.php');
		}

		if (menuitemId == "pdf") {
			grid.toPDF('../codebase/grid-pdf-php/generate.php');
		}
		if (menuitemId == "refresh") {
			Refresh();
		}
    }
    
    function onBeforeSelect(new_row,old_row,new_col_index){
        console.log(new_row,old_row,new_col_index)
        elementoseleccionado = datastore.item(new_row);
        console.log(elementoseleccionado,"elementoseleccionado")
        return true;
    }
	function onEditCell(stage,rId,cInd,nValue,oValue){
        console.log(stage,rId,cInd,nValue,oValue)
		//stage Estado del Editor
		//rId Id del Row
		//cInd Indice de la celda
		//nValue Valor nuevo solo si el stage es 2
		//oValue Valor viejo solo si el stage es 2	
        item = datastore.item(rId);
        console.log(item);
        if(stage == 0){//El editor esta abierto
            // EditarCellElemento(rId);
			if(cInd == 4) return true;
		}
        if(stage == 1){//El editor esta abierto
            if(cInd == 4){
                EditarCellElementoActivo(rId);
                return true;
            }
		}
        else if(stage == 2){//El editor esta cerrado
            console.log("entro")
			if(cInd != 3){
				EditarCellElemento(rId);
                return true;	
			}
		} 				
	}
	function Refresh(){
        // url_get_catalogo_elemento = node_chapel_web + "entradas_salidas?instruccion=get_registros&cliente="+client_id+"&fechadesde="+fechaPrimerDia+"&fechahasta="+fechaUltimoDia+"&tipoDocumento=4";
		datastore.clearAll();
			grid.clearAll();
			datastore.load(url_get_catalogo_elemento,function(){
			// grid.clearAll();
			grid.sync(datastore);
		});
    }
    function Filtrar(fecha_desde,fecha_hasta){
        console.log(fecha_desde,fecha_hasta);
        url_get_catalogo_elemento = node_chapel_web + "entradas_salidas?instruccion=get_registros&cliente="+client_id+"&fechadesde="+fecha_desde+"&fechahasta="+fecha_hasta+"&tipoDocumento=9";
		datastore.clearAll();
			grid.clearAll();
			datastore.load(url_get_catalogo_elemento,function(){
			grid.clearAll();
			grid.sync(datastore);
		});
	}
	function onKeyPressed(code,ctrl,shift){
		if(code==67&&ctrl){
			if (!grid._selectionArea){
				return dhtmlx.alert(
					{
						title:"Alerta",
						type:"alert",
						text:"Debes seleccionar un bloque de la cuadricula previamente"
					}
				);
			} 
			grid.setCSVDelimiter("\t");
			grid.copyBlockToClipboard();
		}
		return true;
	}
	function AgregarElemento(){
		// $.get(node_chapel_web + "get_uuid", function(uuid){
			var width = document.body.clientWidth - 200;
        	var height = document.body.clientHeight;
			var ventanaAgregar = dhxWindow.createWindow({
				id:"ventanaAgregar",
				// left:Number(window.innerWidth * 0.1),
				text:"<b>Agregar Hospedaje</b>",
				// top:600,
				width:width,
				height:height,
				center:true,
				modal:true
			});
			dhxWindow.window("ventanaAgregar").button("minmax").attachEvent("onClick", function(win, button){
				var elemento = document.getElementsByClassName("dhxwin_active");
				for(var i = 0; i < elemento.length; i++){
					var elemento2 = document.getElementsByClassName("pantalla_completa");
					if(elemento2.length > 0){
						elemento[i].className = "dhxwin_active";
						dhxWindow.window("ventanaAgregar").setPosition(Number(window.innerWidth * 0.1), 0);
					}else{
						dhxWindow.window("ventanaAgregar").setPosition(0, 0);
						elemento[i].className += " pantalla_completa";
					}
				}
				return true;
			});
			document.body.onresize = ()=>{
				var width = document.body.clientWidth - 200;
				var height = document.body.clientHeight;
                try{

                    dhxWindow.window("ventanaAgregar").setDimension(width, height);
                }catch(error){
                    console.log(error);
                }
			};
			formAdd = ventanaAgregar.attachForm();
			formAdd.loadStruct("../data/json/agregar_hospedaje_hotel.json");
			formAdd.attachEvent("OnXLE", function(){
                // formAdd.hideItem("activo");
                // formAdd.setItemValue("fecha", moment().format("YYYY-MM-DD HH:mm:ss"));
                
                formAdd.setItemValue("diashospedaje", 0);
                formAdd.setItemValue("costo", Number(0).toFixed(2));
                formAdd.setItemValue("iva", Number(0).toFixed(2));
                formAdd.setItemValue("impsobrehosp", Number(0).toFixed(2));
                formAdd.setItemValue("grantotal", Number(0).toFixed(2));
                formAdd.setItemValue("fechahospedaje", moment().format("YYYY-MM-DD HH:mm"));
                formAdd.removeItem("fechasalida");
                var input1 = formAdd.getContainer("input1");
                $("#"+input1.id).html(`
                <div class="number-input">
                    <button onclick="disminuir(this,'adultosmayorespersonascantidad')" >
                        <i class="fa fa-minus" aria-hidden="true"></i>
                    </button>
                    <input class="quantity" min="0" name="q1"  value="1" id="q1"  type="number">
                    <button onclick="aumentar(this,'adultosmayorespersonascantidad')" class="plus">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </button>
                </div>
                `);
                document.getElementById("q1").onkeydown=(a)=>{
                    console.log(a)
                    var numeros = "0123456789";
                    if(formAdd.isReadonly("input1")) return false;
                    if(a.key == "ArrowUp") return true; 
                    if(a.key == "ArrowDown") return true; 
                    if(numeros.includes(a.key)){
                        return true;
                    }else{
                        return false;
                    }
                };
                document.getElementById("q1").onkeyup=(a)=>{
                    if(!formAdd.isReadonly("input1")) formAdd.setItemValue("adultosmayorespersonascantidad",Number(document.getElementById("q1").value));
                    return true;
                };
                document.getElementById("q1").min = 0;
                var input2 = formAdd.getContainer("input2");
                $("#"+input2.id).html(`
                <div class="number-input">
                    <button onclick="disminuir(this,'adultospersonascantidad')" >
                        <i class="fa fa-minus" aria-hidden="true"></i>
                    </button>
                    <input class="quantity" min="0" name="q2"  value="1" id="q2"  type="number">
                    <button onclick="aumentar(this,'adultospersonascantidad')" class="plus">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </button>
                </div>
                `);
                document.getElementById("q2").onkeydown=(a)=>{
                    console.log(a)
                    var numeros = "0123456789";
                    if(a.key == "ArrowUp") return true; 
                    if(a.key == "ArrowDown") return true;
                    if(numeros.includes(a.key)){
                        return true;
                    }else{
                        return false;
                    }
                }; 
                document.getElementById("q2").onkeyup=(a)=>{
                    formAdd.setItemValue("adultospersonascantidad",Number(document.getElementById("q2").value));
                    return true;
                };
                var input3 = formAdd.getContainer("input3");
                $("#"+input3.id).html(`
                <div class="number-input">
                    <button onclick="disminuir(this,'ninospersonascantidad')" >
                        <i class="fa fa-minus" aria-hidden="true"></i>
                    </button>
                    <input class="quantity" min="0" name="q3"  value="1" id="q3"  type="number">
                    <button onclick="aumentar(this,'ninospersonascantidad')" class="plus">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </button>
                </div>
                `);
                document.getElementById("q3").onkeydown=(a)=>{
                    console.log(a)
                    var numeros = "0123456789";
                    if(a.key == "ArrowUp") return true; 
                    if(a.key == "ArrowDown") return true;
                    if(numeros.includes(a.key)){
                        return true;
                    }else{
                        // document.getElementById("q3").value = formAdd.getItemValue("ninospersonascantidad");
                        return false;
                    }
                };
                document.getElementById("q3").onkeyup=(a)=>{
                    formAdd.setItemValue("ninospersonascantidad",Number(document.getElementById("q3").value));
                    return true;
                }; 
                var input4 = formAdd.getContainer("input4");
                $("#"+input4.id).html(`
                <div class="number-input">
                    <button onclick="disminuir(this,'bebespersonascantidad')" >
                        <i class="fa fa-minus" aria-hidden="true"></i>
                    </button>
                    <input class="quantity" min="0" name="q4"  value="1" id="q4"  type="number">
                    <button onclick="aumentar(this,'bebespersonascantidad')" class="plus">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </button>
                </div>
                `);
                document.getElementById("q4").onkeydown=(a)=>{
                    console.log(a)
                    var numeros = "0123456789";
                    if(a.key == "ArrowUp") return true; 
                    if(a.key == "ArrowDown") return true;
                    if(numeros.includes(a.key)){
                        return true;
                    }else{
                        return false;
                    }
                }; 
                document.getElementById("q4").onkeyup=(a)=>{
                    formAdd.setItemValue("bebespersonascantidad",Number(document.getElementById("q4").value));
                    return true;
                }; 
                var input5 = formAdd.getContainer("input5");
                $("#"+input5.id).html(`
                <div class="number-input">
                    <button onclick="disminuir(this,'diashospedaje')" >
                        <i class="fa fa-minus" aria-hidden="true"></i>
                    </button>
                    <input class="quantity" min="0" name="q5"  value="1" id="q5"  type="number">
                    <button onclick="aumentar(this,'diashospedaje')" class="plus">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </button>
                </div>
                `);
                document.getElementById("q5").onkeydown=(a)=>{
                    console.log(a)
                    var numeros = "0123456789";
                    if(a.key == "ArrowUp") return true; 
                    if(a.key == "ArrowDown") return true;
                    if(numeros.includes(a.key)){
                        return true;
                    }else{
                        return false;
                    }
                }; 
                document.getElementById("q5").onkeyup=(a)=>{
                    formAdd.setItemValue("diashospedaje",Number(document.getElementById("q5").value));
                    calcularTotal();
                    return true;
                }; 
                $("input[type=number]").focus(function(){	   
                    this.select();
                });
                $("input[type=number]").click(function(){	   
                    this.select();
                });
                $.get("../../node_hotel/clientes_hotel?instruccion=leer",function(hab){

                    cliente = formAdd.getCombo("cliente");
                    cliente.attachEvent("onChange", function(index,value){
                        if(value !== "ND"){
                            let filtro = hab.filter((a)=>{ return a.itemid == index; })[0];
                            formAdd.setItemValue("rfc",filtro.rfc);
                        }else{
                            formAdd.setItemValue("rfc","");
                        }		
                    });
                });
                tipohabitacion = formAdd.getCombo("tipohabitacion");
                habitacion = formAdd.getCombo("habitacion");	
                tipohabitacion.attachEvent("onChange", function(index,value){
                    habitacion.clearAll();
                    habitacion.unSelectOption();
                    // if(value !== "ND"){
                    //     let filtro = hab.filter((a)=>{ return a.itemid == index; })[0];
                    //     formAdd.setItemValue("rfc",filtro.rfc);
                    // }else{
                    //     formAdd.setItemValue("rfc","");
                    // }	
                    habitacion.load("../../node_hotel/datos_cat?instruccion=combo_habitaciones&tipoHabitacion="+tipohabitacion.getActualValue(), function(){
                        // data loaded and rendered
                        // your code here
                        // alert("cargo")
                        var i = habitacion.getOptionsCount();
                        
                    });
                });
                // "connector":"../../../node_hotel/datos_cat?instruccion=combo_habitaciones"
                // $.get("../../node_hotel/habitaciones_hotel?instruccion=leer",function(hab){

                    // habitacion = formAdd.getCombo("habitacion");
                    habitacion.attachEvent("onChange", function(index,value){
                        if(value !== "ND"){
                            let filtro = habitaciones.filter((a)=>{ return a.itemid == index; })[0];
                            console.log(index,value,filtro);
                            // if(filtro.length > 0){
                                var tarifa = filtro.tarifa;
                                var iva = Number(tarifa * .16).toFixed(2);
                                var impsobrehosp = Number(tarifa * .03).toFixed(2);
                                var costo = Number(tarifa - iva - impsobrehosp).toFixed(2);
                                var diashospedaje = 1;
                                formAdd.setItemValue("diashospedaje",diashospedaje);
                                formAdd.setItemValue("costo",costo);
                                formAdd.setItemValue("iva",iva);
                                formAdd.setItemValue("impsobrehosp",impsobrehosp);
                                formAdd.setItemValue("grantotal",Number(diashospedaje * Number(filtro.tarifa)).toFixed(2));
                            // }
                        }else{
                            formAdd.setItemValue("grantotal",Number(0).toFixed(2));
                            formAdd.setItemValue("costo",Number(0).toFixed(2));
                            formAdd.setItemValue("iva",Number(0).toFixed(2));
                            formAdd.setItemValue("impsobrehosp",Number(0).toFixed(2));
                            formAdd.setItemValue("diashospedaje",0);
                        }		
                    });
                // });
            });
            // formAdd.attachEvent("onBeforeChange", function (name, old_value, new_value){
            //     //your code here
            //     if(name == "diashospedaje") { if(isNaN(formAdd.getItemValue(name))) {return false;}}
            //     return true;
            // });
            formAdd.attachEvent("onBeforeValidate", function (id){
                // your code here
                console.log(id)
                return true;
            });
            formAdd.attachEvent("onKeyUp",function(inp, ev, name, value){
                console.log(inp, ev, name, value,formAdd.getItemValue(name));
            });
            formAdd.attachEvent("onChange", function (name, value, state){
                console.log(name, value, state)
                var personas = ["adultosmayorespersonas","adultospersonas","ninospersonas","bebespersonas"];
                if(personas.includes(name) && !state){

                    if(name=="adultosmayorespersonas"){
                        var pos = 1;
                        formAdd.setRequired("input"+pos,false);
                        formAdd.setReadonly("input"+pos,true);
                        document.getElementById("q"+pos).value = 0;
                        document.getElementById("q"+pos).readOnly =true;
                    }
                    if(name=="adultospersonas"){
                        var pos = 2;
                        formAdd.setRequired("input"+pos,false);
                        formAdd.setReadonly("input"+pos,true);
                        document.getElementById("q"+pos).value = 0;
                        document.getElementById("q"+pos).readOnly =true;
                    }
                    if(name=="ninospersonas"){
                        var pos = 3;
                        formAdd.setRequired("input"+pos,false);
                        formAdd.setReadonly("input"+pos,true);
                        document.getElementById("q"+pos).value = 0;
                        document.getElementById("q"+pos).readOnly =true;
                    }
                    if(name=="bebespersonas"){
                        var pos = 4;
                        formAdd.setRequired("input"+pos,false);
                        formAdd.setReadonly("input"+pos,true);
                        document.getElementById("q"+pos).value = 0;
                        document.getElementById("q"+pos).readOnly =true;
                    }
                    formAdd.setItemValue(name+"cantidad",0);//myForm.setReadonly("myInput", true);
                }else{
                    if(name=="adultosmayorespersonas"){
                        var pos = 1;
                        formAdd.setRequired("input"+pos,true);
                        formAdd.setReadonly("input"+pos,false);
                        document.getElementById("q"+pos).value = 1;
                        document.getElementById("q"+pos).readOnly =false;
                    }
                    if(name=="adultospersonas"){
                        var pos = 2;
                        formAdd.setRequired("input"+pos,true);
                        formAdd.setReadonly("input"+pos,false);
                        document.getElementById("q"+pos).value = 1;
                        document.getElementById("q"+pos).readOnly =false;
                    }
                    if(name=="ninospersonas"){
                        var pos = 3;
                        formAdd.setRequired("input"+pos,true);
                        formAdd.setReadonly("input"+pos,false);
                        document.getElementById("q"+pos).value = 1;
                        document.getElementById("q"+pos).readOnly =false;
                    }
                    if(name=="bebespersonas"){
                        var pos = 4;
                        formAdd.setRequired("input"+pos,true);
                        formAdd.setReadonly("input"+pos,false);
                        document.getElementById("q"+pos).value = 1;
                        document.getElementById("q"+pos).readOnly =false;
                    }
                    formAdd.setItemValue(name+"cantidad",1);//myForm.setReadonly("myInput", true);
                }
            });
            formAdd.enableLiveValidation(true);
			formAdd.attachEvent("onButtonClick", function(name){
				if (name=="enviar"){
					if(formAdd.validate()){
                        var values = formAdd.getFormData(true);
                        values.usuariocrea = nombreusuario;
                        values.personas = {};
                        values.personas.adultosmayores = values.adultosmayorespersonas;
                        delete values.adultosmayorespersonas;
                        values.personas.adultos = values.adultospersonas;
                        delete values.adultospersonas;
                        values.personas.ninos = values.ninospersonas;
                        delete values.ninospersonas;
                        values.personas.bebes = values.bebespersonas;
                        delete values.bebespersonas;
                        values.cantidadespersonas = {};
                        values.cantidadespersonas.adultosmayores = values.adultosmayorespersonascantidad;
                        delete values.adultosmayorespersonascantidad;
                        values.cantidadespersonas.adultos = values.adultospersonascantidad;
                        delete values.adultospersonascantidad;
                        values.cantidadespersonas.ninos = values.ninospersonascantidad;
                        delete values.ninospersonascantidad;
                        values.cantidadespersonas.bebes = values.bebespersonascantidad;
                        delete values.bebespersonascantidad;
                        values.personas = JSON.stringify(values.personas);
                        values.cantidadespersonas = JSON.stringify(values.cantidadespersonas);
                        delete values.tipohabitacion;
                        console.log(values);
						$.post("../../node_hotel/hospedajes_hotel?instruccion=agregar",values, function(data, textStatus, xhr) {
                            console.log(data);
                           
							if(textStatus == "success"){
								if(typeof data !== "string" ){
									Refresh();
									ventanaAgregar.close();
								}else{
                                    Refresh();
                                    dhtmlx.alert({
                                        title:"ERROR",
                                        type:"alert-warning",
                                        text:data
                                    });	
								}
							}
						});
					}
				}
			});
			formAdd.enableLiveValidation(true);			
		// });
	}
	function EditarElemento(selectedId){
		item = datastore.item(selectedId);
        delete item.id;
        console.log(item);
        var personas = JSON.parse(item.personas);
        var cantidadespersonas = JSON.parse(item.cantidadespersonas);
		var width = document.body.clientWidth - 200;
        var height = document.body.clientHeight;
        var ventanaAgregar = dhxWindow.createWindow({
            id:"ventanaAgregar",
            // left:Number(window.innerWidth * 0.1),
            text:"<b>Editar Hospedaje</b>",
            // top:600,
            width:width,
            height:height,
            center:true,
            modal:true
        });
        dhxWindow.window("ventanaAgregar").button("minmax").attachEvent("onClick", function(win, button){
            var elemento = document.getElementsByClassName("dhxwin_active");
            for(var i = 0; i < elemento.length; i++){
                var elemento2 = document.getElementsByClassName("pantalla_completa");
                if(elemento2.length > 0){
                    elemento[i].className = "dhxwin_active";
                    dhxWindow.window("ventanaAgregar").setPosition(Number(window.innerWidth * 0.1), 0);
                }else{
                    dhxWindow.window("ventanaAgregar").setPosition(0, 0);
                    elemento[i].className += " pantalla_completa";
                }
            }
            return true;
        });
        document.body.onresize = ()=>{
            var width = document.body.clientWidth - 200;
            var height = document.body.clientHeight;
            try{
                dhxWindow.window("ventanaAgregar").setDimension(width, height);
            }catch(error){
            }
        };
        formAdd = ventanaAgregar.attachForm();
        formAdd.loadStruct("../data/json/editar_hospedaje_hotel.json");
        formAdd.attachEvent("OnXLE", function(){
            // formAdd.hideItem("activo");
            // formAdd.setItemValue("fecha", moment().format("YYYY-MM-DD HH:mm:ss"));
            $.get("../../node_hotel/habitaciones_hotel?instruccion=leer",function(hab){
                hab = hab.filter((a)=>{return a.itemid == item.habitacion})[0];
                // alert(item.habitacion);
                formAdd.setItemValue("habitacion", hab.nombre);
                var tipohabitacion = formAdd.getCombo("tipohabitacion");
                var option=tipohabitacion.getIndexByValue(hab.tipohabitacion);
                tipohabitacion.selectOption(option);
            });
            formAdd.disableItem("cliente");
            formAdd.removeItem("activo");
            formAdd.disableItem("tipohabitacion");
            formAdd.disableItem("diashospedaje");
            formAdd.disableItem("fechahospedaje");
            formAdd.disableItem("adultosmayorespersonas");
            formAdd.disableItem("adultospersonas");
            formAdd.disableItem("ninospersonas");
            formAdd.disableItem("bebespersonas");
            formAdd.setItemValue("itemid", item.itemid);
            formAdd.setItemValue("diashospedaje", item.diashospedaje);
            formAdd.setItemValue("costo", Number(item.costo).toFixed(2));
            formAdd.setItemValue("iva", Number(item.iva).toFixed(2));
            formAdd.setItemValue("impsobrehosp", Number(item.impsobrehosp).toFixed(2));
            formAdd.setItemValue("grantotal", Number(item.grantotal).toFixed(2));
            formAdd.setItemValue("fechahospedaje", item.fechahospedaje);
            formAdd.setItemValue("comentarios", item.comentarios);
            formAdd.setItemValue("rfc", item.rfc);
            formAdd.setItemValue("adultosmayorespersonascantidad", cantidadespersonas.adultosmayores);
            formAdd.setItemValue("adultospersonascantidad", cantidadespersonas.adultos);
            formAdd.setItemValue("ninospersonascantidad", cantidadespersonas.ninos);
            formAdd.setItemValue("bebespersonascantidad", cantidadespersonas.bebes);
            formAdd.setItemValue("adultosmayorespersonas", personas.adultosmayores);
            formAdd.setItemValue("adultospersonas", personas.adultos);
            formAdd.setItemValue("ninospersonas", personas.ninos);
            formAdd.setItemValue("bebespersonas", personas.bebes);
            
            formAdd.removeItem("fechasalida");
            var input1 = formAdd.getContainer("input1");
            $("#"+input1.id).html(`
            <div class="number-input">
                <button onclick="disminuir(this,'adultosmayorespersonascantidad')"     disabled>
                    <i class="fa fa-minus" aria-hidden="true"></i>
                </button>
                <input class="quantity" disabled min="0" name="q1"  value="`+cantidadespersonas.adultosmayores+`" id="q1"  type="number">
                <button onclick="aumentar(this,'adultosmayorespersonascantidad')" class="plus"    disabled>
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </button>
            </div>
            `);
            document.getElementById("q1").onkeydown=(a)=>{
                console.log(a)
                var numeros = "0123456789";
                if(formAdd.isReadonly("input1")) return false;
                if(a.key == "ArrowUp") return true; 
                if(a.key == "ArrowDown") return true; 
                if(numeros.includes(a.key)){
                    return true;
                }else{
                    return false;
                }
            };
            document.getElementById("q1").onkeyup=(a)=>{
                if(!formAdd.isReadonly("input1")) formAdd.setItemValue("adultosmayorespersonascantidad",Number(document.getElementById("q1").value));
                return true;
            };
            document.getElementById("q1").min = 0;
            var input2 = formAdd.getContainer("input2");
            $("#"+input2.id).html(`
            <div class="number-input">
                <button onclick="disminuir(this,'adultospersonascantidad')"    disabled>
                    <i class="fa fa-minus" aria-hidden="true"></i>
                </button>
                <input class="quantity" disabled min="0" name="q2"  value="`+cantidadespersonas.adultos+`" id="q2"  type="number">
                <button onclick="aumentar(this,'adultospersonascantidad')" class="plus"   disabled>
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </button>
            </div>
            `);
            document.getElementById("q2").onkeydown=(a)=>{
                console.log(a)
                var numeros = "0123456789";
                if(a.key == "ArrowUp") return true; 
                if(a.key == "ArrowDown") return true;
                if(numeros.includes(a.key)){
                    return true;
                }else{
                    return false;
                }
            }; 
            document.getElementById("q2").onkeyup=(a)=>{
                formAdd.setItemValue("adultospersonascantidad",Number(document.getElementById("q2").value));
                return true;
            };
            var input3 = formAdd.getContainer("input3");
            $("#"+input3.id).html(`
            <div class="number-input">
                <button onclick="disminuir(this,'ninospersonascantidad')"   disabled>
                    <i class="fa fa-minus" aria-hidden="true"></i>
                </button>
                <input class="quantity" disabled min="0" name="q3"  value="`+cantidadespersonas.ninos+`" id="q3"  type="number">
                <button onclick="aumentar(this,'ninospersonascantidad')" class="plus"  disabled>
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </button>
            </div>
            `);
            document.getElementById("q3").onkeydown=(a)=>{
                console.log(a)
                var numeros = "0123456789";
                if(a.key == "ArrowUp") return true; 
                if(a.key == "ArrowDown") return true;
                if(numeros.includes(a.key)){
                    return true;
                }else{
                    // document.getElementById("q3").value = formAdd.getItemValue("ninospersonascantidad");
                    return false;
                }
            };
            document.getElementById("q3").onkeyup=(a)=>{
                formAdd.setItemValue("ninospersonascantidad",Number(document.getElementById("q3").value));
                return true;
            }; 
            var input4 = formAdd.getContainer("input4");
            $("#"+input4.id).html(`
            <div class="number-input">
                <button onclick="disminuir(this,'bebespersonascantidad')"  disabled>
                    <i class="fa fa-minus" aria-hidden="true"></i>
                </button>
                <input class="quantity" disabled min="0" name="q4"  value="`+cantidadespersonas.bebes+`" id="q4"  type="number">
                <button onclick="aumentar(this,'bebespersonascantidad')" class="plus" disabled>
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </button>
            </div>
            `);
            document.getElementById("q4").onkeydown=(a)=>{
                console.log(a)
                var numeros = "0123456789";
                if(a.key == "ArrowUp") return true; 
                if(a.key == "ArrowDown") return true;
                if(numeros.includes(a.key)){
                    return true;
                }else{
                    return false;
                }
            }; 
            document.getElementById("q4").onkeyup=(a)=>{
                formAdd.setItemValue("bebespersonascantidad",Number(document.getElementById("q4").value));
                return true;
            }; 
            var input5 = formAdd.getContainer("input5");
            $("#"+input5.id).html(`
            <div class="number-input">
                <button onclick="disminuir(this,'diashospedaje')" disabled>
                    <i class="fa fa-minus" aria-hidden="true"></i>
                </button>
                <input class="quantity" disabled min="0" name="q5"  value="`+item.diashospedaje+`" id="q5"  type="number">
                <button onclick="aumentar(this,'diashospedaje')" class="plus" disabled>
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </button>
            </div>
            `);
            document.getElementById("q5").onkeydown=(a)=>{
                console.log(a)
                var numeros = "0123456789";
                if(a.key == "ArrowUp") return true; 
                if(a.key == "ArrowDown") return true;
                if(numeros.includes(a.key)){
                    return true;
                }else{
                    return false;
                }
            }; 
            document.getElementById("q5").onkeyup=(a)=>{
                formAdd.setItemValue("diashospedaje",Number(document.getElementById("q5").value));
                calcularTotal();
                return true;
            }; 
            $("input[type=number]").focus(function(){	   
                this.select();
            });
            $("input[type=number]").click(function(){	   
                this.select();
            });
            if(personas.adultosmayores == 0){
                var pos = 1;
                formAdd.setRequired("input"+pos,false);
                formAdd.setReadonly("input"+pos,true);
                document.getElementById("q"+pos).value = 0;
                document.getElementById("q"+pos).readOnly =true;
            }else{
                var pos = 1;
                formAdd.setRequired("input"+pos,true);
                formAdd.setReadonly("input"+pos,false);
                document.getElementById("q"+pos).value = cantidadespersonas.adultosmayores;
                document.getElementById("q"+pos).readOnly =false;
            }
            if(personas.adultos == 0){
                var pos = 2;
                formAdd.setRequired("input"+pos,false);
                formAdd.setReadonly("input"+pos,true);
                document.getElementById("q"+pos).value = 0;
                document.getElementById("q"+pos).readOnly =true;
            }else{
                var pos = 2;
                formAdd.setRequired("input"+pos,true);
                formAdd.setReadonly("input"+pos,false);
                document.getElementById("q"+pos).value = cantidadespersonas.adultos;
                document.getElementById("q"+pos).readOnly =false;
            }
            if(personas.ninos == 0){
                var pos = 3;
                formAdd.setRequired("input"+pos,false);
                formAdd.setReadonly("input"+pos,true);
                document.getElementById("q"+pos).value = 0;
                document.getElementById("q"+pos).readOnly =true;
            }else{
                var pos = 3;
                formAdd.setRequired("input"+pos,true);
                formAdd.setReadonly("input"+pos,false);
                document.getElementById("q"+pos).value = cantidadespersonas.ninos;
                document.getElementById("q"+pos).readOnly =false;
            }
            if(personas.bebes == 0){
                var pos = 4;
                formAdd.setRequired("input"+pos,false);
                formAdd.setReadonly("input"+pos,true);
                document.getElementById("q"+pos).value = 0;
                document.getElementById("q"+pos).readOnly =true;
            }else{
                var pos = 4;
                formAdd.setRequired("input"+pos,true);
                formAdd.setReadonly("input"+pos,false);
                document.getElementById("q"+pos).value = cantidadespersonas.bebes;
                document.getElementById("q"+pos).readOnly =false;
            }
            $.get("../../node_hotel/clientes_hotel?instruccion=leer",function(hab){

                cliente = formAdd.getCombo("cliente");
                cliente.attachEvent("onChange", function(index,value){
                    if(value !== "ND"){
                        let filtro = hab.filter((a)=>{ return a.itemid == index; })[0];
                        formAdd.setItemValue("rfc",filtro.rfc);
                    }else{
                        formAdd.setItemValue("rfc","");
                    }		
                });
            });
            
                var cliente = formAdd.getCombo("cliente");
                cliente.attachEvent("onXLE", function(){	
                    var option=cliente.getIndexByValue(item.cliente);
                    cliente.selectOption(option);
                });
                // var habitacion = formAdd.getCombo("habitacion");
                // habitacion.attachEvent("onXLE", function(){	
                //     var option=habitacion.getIndexByValue(item.habitacion);
                //     habitacion.selectOption(option);
                // });
                // formAdd.removeItem("tipohabitacion");
                // habitacion.attachEvent("onChange", function(index,value){
                //     if(value !== "ND"){
                //         let filtro = habitaciones.filter((a)=>{ return a.itemid == index; })[0];
                //         console.log(index,value,filtro);
                //         var tarifa = filtro.tarifa;
                //         var iva = Number(tarifa * .16).toFixed(2);
                //         var impsobrehosp = Number(tarifa * .03).toFixed(2);
                //         var costo = Number(tarifa - iva - impsobrehosp).toFixed(2);
                //         var diashospedaje = 1;
                //         formAdd.setItemValue("diashospedaje",diashospedaje);
                //         formAdd.setItemValue("costo",costo);
                //         formAdd.setItemValue("iva",iva);
                //         formAdd.setItemValue("impsobrehosp",impsobrehosp);
                //         formAdd.setItemValue("grantotal",Number(diashospedaje * Number(filtro.tarifa)).toFixed(2));
                //     }else{
                //         formAdd.setItemValue("grantotal",Number(0).toFixed(2));
                //         formAdd.setItemValue("costo",Number(0).toFixed(2));
                //         formAdd.setItemValue("iva",Number(0).toFixed(2));
                //         formAdd.setItemValue("impsobrehosp",Number(0).toFixed(2));
                //         formAdd.setItemValue("diashospedaje",0);
                //     }		
                // });
            
        });
        // formAdd.attachEvent("onBeforeChange", function (name, old_value, new_value){
        //     //your code here
        //     if(name == "diashospedaje") { if(isNaN(formAdd.getItemValue(name))) {return false;}}
        //     return true;
        // });
        formAdd.attachEvent("onBeforeValidate", function (id){
            // your code here
            console.log(id)
            return true;
        });
        formAdd.attachEvent("onKeyUp",function(inp, ev, name, value){
            console.log(inp, ev, name, value,formAdd.getItemValue(name));
        });
        formAdd.attachEvent("onChange", function (name, value, state){
            console.log(name, value, state)
            var personas = ["adultosmayorespersonas","adultospersonas","ninospersonas","bebespersonas"];
            if(personas.includes(name) && !state){

                if(name=="adultosmayorespersonas"){
                    var pos = 1;
                    formAdd.setRequired("input"+pos,false);
                    formAdd.setReadonly("input"+pos,true);
                    document.getElementById("q"+pos).value = 0;
                    document.getElementById("q"+pos).readOnly =true;
                }
                if(name=="adultospersonas"){
                    var pos = 2;
                    formAdd.setRequired("input"+pos,false);
                    formAdd.setReadonly("input"+pos,true);
                    document.getElementById("q"+pos).value = 0;
                    document.getElementById("q"+pos).readOnly =true;
                }
                if(name=="ninospersonas"){
                    var pos = 3;
                    formAdd.setRequired("input"+pos,false);
                    formAdd.setReadonly("input"+pos,true);
                    document.getElementById("q"+pos).value = 0;
                    document.getElementById("q"+pos).readOnly =true;
                }
                if(name=="bebespersonas"){
                    var pos = 4;
                    formAdd.setRequired("input"+pos,false);
                    formAdd.setReadonly("input"+pos,true);
                    document.getElementById("q"+pos).value = 0;
                    document.getElementById("q"+pos).readOnly =true;
                }
                formAdd.setItemValue(name+"cantidad",0);//myForm.setReadonly("myInput", true);
            }else{
                if(name=="adultosmayorespersonas"){
                    var pos = 1;
                    formAdd.setRequired("input"+pos,true);
                    formAdd.setReadonly("input"+pos,false);
                    document.getElementById("q"+pos).value = 1;
                    document.getElementById("q"+pos).readOnly =false;
                }
                if(name=="adultospersonas"){
                    var pos = 2;
                    formAdd.setRequired("input"+pos,true);
                    formAdd.setReadonly("input"+pos,false);
                    document.getElementById("q"+pos).value = 1;
                    document.getElementById("q"+pos).readOnly =false;
                }
                if(name=="ninospersonas"){
                    var pos = 3;
                    formAdd.setRequired("input"+pos,true);
                    formAdd.setReadonly("input"+pos,false);
                    document.getElementById("q"+pos).value = 1;
                    document.getElementById("q"+pos).readOnly =false;
                }
                if(name=="bebespersonas"){
                    var pos = 4;
                    formAdd.setRequired("input"+pos,true);
                    formAdd.setReadonly("input"+pos,false);
                    document.getElementById("q"+pos).value = 1;
                    document.getElementById("q"+pos).readOnly =false;
                }
                formAdd.setItemValue(name+"cantidad",1);//myForm.setReadonly("myInput", true);
            }
        });
        formAdd.enableLiveValidation(true);
        // formAdd.lock();
        // formAdd.enableItem("comentarios")
        formAdd.attachEvent("onButtonClick", function(name){
            if (name=="enviar"){
                if(formAdd.validate()){
                    var values = formAdd.getFormData(true);
                    values.usuariocrea = nombreusuario;
                    values.personas = {};
                    values.personas.adultosmayores = values.adultosmayorespersonas;
                    delete values.adultosmayorespersonas;
                    values.personas.adultos = values.adultospersonas;
                    delete values.adultospersonas;
                    values.personas.ninos = values.ninospersonas;
                    delete values.ninospersonas;
                    values.personas.bebes = values.bebespersonas;
                    delete values.bebespersonas;
                    values.cantidadespersonas = {};
                    values.cantidadespersonas.adultosmayores = values.adultosmayorespersonascantidad;
                    delete values.adultosmayorespersonascantidad;
                    values.cantidadespersonas.adultos = values.adultospersonascantidad;
                    delete values.adultospersonascantidad;
                    values.cantidadespersonas.ninos = values.ninospersonascantidad;
                    delete values.ninospersonascantidad;
                    values.cantidadespersonas.bebes = values.bebespersonascantidad;
                    delete values.bebespersonascantidad;
                    values.personas = JSON.stringify(values.personas);
                    values.cantidadespersonas = JSON.stringify(values.cantidadespersonas);
                    values.usuarioedita = nombreusuario;
                    delete values.tipohabitacion;
                    delete values.habitacion;
                    console.log(values);
                    $.post("../../node_hotel/hospedajes_hotel?instruccion=actualizar",values, function(data, textStatus, xhr) {
                        console.log(data);
                        if(textStatus == "success"){
                            if(data !== "ERROR"){
                                Refresh();
                                ventanaAgregar.close();
                            }else{
                                Refresh();
                                dhtmlx.alert({
                                    title:"ERROR",
                                    type:"alert-warning",
                                    text:"Ocurrio un error al editar Hospedaje"
                                });	
                            }
                        }
                    });
                }
            }
        });
        formAdd.enableLiveValidation(true);		
	}
	function EliminarElemento(selectedId){
		item = datastore.item(selectedId);
		delete item.id;
		dhtmlx.confirm({
			title: "Confirmación",
			type:"confirm-warning",
			text: "Desea eliminar el registro seleccionado?",
			ok: "Si",
			cancel: "No",
			callback: function(result){
				if(result){		
                    item.usuarioelimina = nombreusuario;			
					$.post("../../node_hotel/hospedajes_hotel?instruccion=eliminar",item, function(data, textStatus, xhr) {
						if(textStatus == "success"){
							if(data !== "ERROR"){
								Refresh();
							}else{
								dhtmlx.alert({
									title:"ERROR",
									type:"alert-error",
									text:"Ocurrio un error al eliminar registro"
								});	
								Refresh();
							}
						}
					});
				}
			}
		});	
	}
	function EditarCellElemento(selectedId){
		item = datastore.item(selectedId);
		delete item.id;
        // item.instruccion = "ud";
        console.log(item);
        var empresa = grid.cells(selectedId,3).getValue();
        item.empresa = empresa;
		$.post("../../node_hotel/clientes_hotel?instruccion=actualizar",item, function(data, textStatus, xhr) {
			if(textStatus == "success"){
				if(data !== "ERROR"){
					Refresh();
				}else{
                    dhtmlx.alert({
                        title:"Warning",
                        type:"alert-warning",
                        text:"Ha ocurrido un error al editar el elemento"
                    });	
                    Refresh();	
				}
			}
		});	
    }
    function EditarCellElementoActivo(selectedId){
		item = datastore.item(selectedId);
		delete item.id;
        item.instruccion = "ud";
        if(item.activo == 1) item.activo = 0;
        else item.activo = 1;
		$.post("../../node_hotel/clientes_hotel?instruccion=actualizar",item, function(data, textStatus, xhr) {
			if(textStatus == "success"){
				if(data !== "ERROR"){
					Refresh();
				}else{
                    dhtmlx.alert({
                        title:"Warning",
                        type:"alert-warning",
                        text:"Ha ocurrido un error al editar el elemento"
                    });	
                    Refresh();	
				}
			}
		});	
    }

    function eXcell_es(cell){ //the eXcell name is defined here
        if (cell){                // the default pattern, just copy it
            this.cell = cell;
            this.grid = this.cell.parentNode.grid;
        }
        this.edit = function(){}  //read-only cell doesn't have edit method
        // the cell is read-only, so it's always in the disabled state
        this.isDisabled = function(){ return true; }
        this.setValue=function(val){
            let icono,row_id;
            row_id = this.cell.parentNode.idd;
            item = datastore.item(row_id);
			if(item.entrada_salida == 0){
                this.cell.style.color = "green";
                icono = `<i style="font-weigth:bolder;font-size:1.5em;" class="fa fa-arrow-circle-left" aria-hidden="true"></i> <span style="color:black;font-weight:bolder;font-size:2em;">|</span>`;
			}else{
                this.cell.style.color = "red";
                icono = `<span style="color:black;font-weight:bolder;font-size:2em;">|</span> <i style="font-weigth:bolder;font-size:1.5em;" class="fa fa-arrow-circle-right" aria-hidden="true"></i>`;
			}
			this.setCValue(icono,val);
        }
    }
    eXcell_es.prototype = new eXcell;// nests all other methods from the base class
    // 
    function eXcell_habitacion(cell){ //the eXcell name is defined here
        if (cell){                // the default pattern, just copy it
            this.cell = cell;
            this.grid = this.cell.parentNode.grid;
        }
        this.isDisabled = function(){ return true; }
        this.getValue=function(){
            return this.cell.innerText; // get value
        }
        this.setValue=function(val){
            console.log(val)
            let filtro = habitaciones.filter((a)=>{ return a.itemid == val; })[0];
            console.log(filtro,val,habitaciones)
            this.setCValue(filtro.nombre,val);
        }
    }
    eXcell_habitacion.prototype = new eXcell;// nests all other methods from the base class
    function eXcell_cliente(cell){ //the eXcell name is defined here
        if (cell){                // the default pattern, just copy it
            this.cell = cell;
            this.grid = this.cell.parentNode.grid;
        }
        this.isDisabled = function(){ return true; }
        this.getValue=function(){
            return this.cell.innerText; // get value
        }
        this.setValue=function(val){
            let filtro = clientes.filter((a)=>{ return a.itemid == val; })[0];
            this.setCValue(filtro.nombre,val);
        }
    }
    eXcell_cliente.prototype = new eXcell;// nests all other methods from the base class
    function eXcell_monto(cell){ //the eXcell name is defined here
        if (cell){                // the default pattern, just copy it
            this.cell = cell;
            this.grid = this.cell.parentNode.grid;
        }
        this.isDisabled = function(){ return true; }
        this.getValue=function(){
            return this.cell.innerText; // get value
        }
        this.setValue=function(val){
            var custom = `<b style="color:black">$</b>&nbsp;&nbsp;<span style="color:green">`+Number(val).toFixed(2)+`</span>`;
            this.setCValue(custom,val);
        }
    }
    eXcell_monto.prototype = new eXcell;// nests all other methods from the base class
    
    function eXcell_personas(cell){ //the eXcell name is defined here
        if (cell){                // the default pattern, just copy it
            this.cell = cell;
            this.grid = this.cell.parentNode.grid;
        }
        this.isDisabled = function(){ return true; }
        this.getValue=function(){
			// console.log(this.cell);
			return this.cell.innerText; // get button label
		}
        this.setValue=function(val){
            var row_id = this.cell.parentNode.idd;
			item = datastore.item(row_id);
            let personas = JSON.parse(item.personas);
            let cantidadespersonas = JSON.parse(item.cantidadespersonas);
            var icono = "fa fa-users";
            var textoam = `<b>`+cantidadespersonas.adultosmayores+` Adultos Mayores</b>`;
            var textoa = `<b>`+cantidadespersonas.adultos+` Adultos</b>`;
            var texton = `<b>`+cantidadespersonas.ninos+` Niños</b>`;
            var textobb = `<b>`+cantidadespersonas.bebes+` Bebés</b>`;
            var valor = `
            <div style="width:100%;">
                <div style="width:2px;display:inline-block;color:green"><i class="`+icono+`" aria-hidden="true"></i></div><div style="width:calc(100% - 2px);text-align:right;vertical-align:middle;display:inline-block">`+textoam+`</div>
                <div style="width:2px;display:inline-block;color:green"><i class="`+icono+`" aria-hidden="true"></i></div><div style="width:calc(100% - 2px);text-align:right;vertical-align:middle;display:inline-block">`+textoa+`</div>
                <div style="width:2px;display:inline-block;color:green"><i class="`+icono+`" aria-hidden="true"></i></div><div style="width:calc(100% - 2px);text-align:right;vertical-align:middle;display:inline-block">`+texton+`</div>
                <div style="width:2px;display:inline-block;color:green"><i class="`+icono+`" aria-hidden="true"></i></div><div style="width:calc(100% - 2px);text-align:right;vertical-align:middle;display:inline-block">`+textobb+`</div>
            </div>
            `;
            console.log(valor);

            this.setCValue(valor,val);
        }
    }
    eXcell_personas.prototype = new eXcell;// nests all other methods from the base class
    function eXcell_cantidadespersonas(cell){ //the eXcell name is defined here
        if (cell){                // the default pattern, just copy it
            this.cell = cell;
            this.grid = this.cell.parentNode.grid;
        }
        this.isDisabled = function(){ return true; }
        this.getValue=function(){
            return this.cell.innerHTML; // get value
        }
        this.setValue=function(val){
            let valor = JSON.parse(val);
            console.log(valor);
            this.setCValue("vamosprogresando2",val);
        }
    }
    eXcell_cantidadespersonas.prototype = new eXcell;// nests all other methods from the base class
    function eXcell_fecha(cell){ //the eXcell name is defined here
        if (cell){                // the default pattern, just copy it
            this.cell = cell;
            this.grid = this.cell.parentNode.grid;
        }
        this.isDisabled = function(){ return true; }
        this.getValue=function(){
            return this.cell.innerHTML; // get value
        }
        this.setValue=function(val){
            this.setCValue(moment(Number(val) * 1000).format("YYYY-MM-DD HH:mm"),val);
        }
    }
	eXcell_fecha.prototype = new eXcell;// nests all other methods from the base class
    function eXcell_empresa(cell){ //the eXcell name is defined here
        if (cell){                // the default pattern, just copy it
            this.cell = cell;
            this.grid = this.cell.parentNode.grid;
            // eXcell_ed.call(this);
        }
        /* this.edit = function(){
            this.val = this.getValue(); // save current value
            // alert(this.val)
            var valor = this.val;
            var html = "<select class='select1' style='width:150px;'>";
            html += "<option value=''>ND</option>";
            empresas.forEach((element,i) => {
                if(this.val == element) html += "<option value='"+i+"' selected>" + element + "</option>";
                else html += "<option value='"+i+"'>" + element + "</option>";
            });
            this.cell.innerHTML = html + "</select>"; // editor's html
            // this.cell.firstChild.value=parseInt(val); // set the first part of data
            // if (val.indexOf("PM")!=-1) this.cell.childNodes[1].value="PM";
            // blocks onclick event
            this.cell.childNodes[0].onclick=function(e){ (e||event).cancelBubble=true;} 
            console.log(this.cell.childNodes[0],"this.cell.childNodes")
            // $('.select1').click();
            // this.cell.childNodes[0].onclick=function(e){ 
            //     alert("clicko")
            //     (e||event).cancelBubble=true;
            // } 
            // blocks onclick event
            // this.cell.childNodes[1].onclick=function(e){ (e||event).cancelBubble=true;}
        }  //read-only cell doesn't have edit method */
        // the cell is read-only, so it's always in the disabled state
        this.isDisabled = function(){ return true; }
        /* this.detach=function(){
            console.log("detach",this)
            console.log(this.cell.childNodes);
            this.setValue(this.cell.childNodes[0].value);
            return this.val!=this.getValue();
            // sets the new value
            // this.setValue(this.cell.childNodes[0].value+" "+this.cell.childNodes[1].value); 
            // return this.val!=this.getValue(); // compares the new and the old values
        } */
        this.getValue=function(){
            return this.cell.innerHTML; // get value
        }
        this.setValue=function(val){
            if(typeof empresas[val] != "undefined") this.setCValue(empresas[val],val);
            else this.setCValue("",val);
        }
    }
	eXcell_empresa.prototype = new eXcell;// nests all other methods from the base class
	function mostrarImagen(archivo,tipo){
		console.log("se abrirá la imagen",archivo,tipo);
		var width = Number(window.innerWidth / 2);
		var ventanaIntercambio = dhxWindow.createWindow({
			id:"abrirImage",
			// left:Number(window.innerWidth * 0.1),
			text:"Imagen",
			// top:0,
			width:width,
			height:600,
			center:true,
			modal:true
		});
		dhxWindow.window("abrirImage").button("minmax").attachEvent("onClick", function(win, button){

			var elemento = document.getElementsByClassName("dhxwin_active");
			for(var i = 0; i < elemento.length; i++){
				var elemento2 = document.getElementsByClassName("pantalla_completa");
				if(elemento2.length > 0){
					elemento[i].className = "dhxwin_active";
					dhxWindow.window("abrirImage").setPosition(Number(window.innerWidth * 0.1), 0);
				}else{
					dhxWindow.window("abrirImage").setPosition(0, 0);
					elemento[i].className += " pantalla_completa";
				}
			}
			return true;
		});
		dhxWindow.window("abrirImage").attachHTMLString("<div style='width:calc(100%)!important;height:100%!important'><iframe allowfullscreen src='"+node_chapel_web +"mostrarArchivo?tipo="+tipo+"&archivo="+archivo+"' width='100%' height='100%'"+"></iframe></div>");
		ventanaIntercambio.button("close").attachEvent("onClick", function(){
			// Refresh();
			return true; // deny default action
		});
	}
	function eXcell_fotito(cell){ //the eXcell name is defined here
        if (cell){                // the default pattern, just copy it
            this.cell = cell;
            this.grid = this.cell.parentNode.grid;
        }
        this.edit = function(){}  //read-only cell doesn't have edit method
        // the cell is read-only, so it's always in the disabled state
        this.isDisabled = function(){ return true; }
        this.setValue=function(val){
            let icono,row_id;
            row_id = this.cell.parentNode.idd;
			item = datastore.item(row_id);
			console.log(item.fotos);
			if(item.fotos && item.fotos != ""){
				let fotos = JSON.parse(item.fotos);
				let todaslasfotos = "";
				for (let key in fotos) {
					console.log(key);
					if (fotos.hasOwnProperty(key)) {
						let element = fotos[key];	
						console.log(element);
						for (const llave in element) {
							if (element.hasOwnProperty(llave)) {
								let element2 = element[llave];								
								console.log(element2,llave);					
								let foto = `<i style="font-weigth:bolder;font-size:1.5em;display:inline-block;padding-right:1px" onclick="mostrarImagen('`+element2+`','Imagen')" class="fa fa-picture-o" aria-hidden="true"></i>`;
								todaslasfotos += foto;
							}
						}
					}
				}
				//tipo=Imagen,archivo=foto
				this.cell.style.color = "green";
				this.setCValue("<div style='width:100%;'>" + todaslasfotos + "</div>",fotos.length);
			}
        }
    }
	eXcell_fotito.prototype = new eXcell;// nests all other methods from the base class
	function rfcValido(rfc, aceptarGenerico = true) {
        return true;
		const re       = /^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$/;
		var   validado = rfc.match(re);

		if (!validado)  //Coincide con el formato general del regex?
			return false;

		//Separar el dígito verificador del resto del RFC
		const digitoVerificador = validado.pop(),
			rfcSinDigito      = validado.slice(1).join(''),
			len               = rfcSinDigito.length,

		//Obtener el digito esperado
			diccionario       = "0123456789ABCDEFGHIJKLMN&OPQRSTUVWXYZ Ñ",
			indice            = len + 1;
		var   suma,
			digitoEsperado;

		if (len == 12) suma = 0
		else suma = 481; //Ajuste para persona moral

		for(var i=0; i<len; i++)
			suma += diccionario.indexOf(rfcSinDigito.charAt(i)) * (indice - i);
		digitoEsperado = 11 - suma % 11;
		if (digitoEsperado == 11) digitoEsperado = 0;
		else if (digitoEsperado == 10) digitoEsperado = "A";

		//El dígito verificador coincide con el esperado?
		// o es un RFC Genérico (ventas a público general)?
		if ((digitoVerificador != digitoEsperado)
		&& (!aceptarGenerico || rfcSinDigito + digitoVerificador != "XAXX010101000"))
			return false;
		else if (!aceptarGenerico && rfcSinDigito + digitoVerificador == "XEXX010101000")
			return false;
		return rfcSinDigito + digitoVerificador;
    }
    function esnumero(data){ 	
        return !isNaN(data);
		// if(!isNaN(data)){
		// 	return data >= 0;
		// }else{
		// 	return false;
		// }
    }
    function esdiavalido(data){ 	
        // return !isNaN(data);
		if(!isNaN(data)){
			return data >= 0;
		}else{
			return false;
		}
    }
    
   
    function disminuir(esto,elemento){
        if(!esto.parentNode.querySelector('input[type=number]').readOnly){
            esto.parentNode.querySelector('input[type=number]').stepDown();
             formAdd.setItemValue(elemento,Number(esto.parentNode.querySelector('input[type=number]').value));
            if(esto.parentNode.querySelector('input[type=number]').id == "q5") calcularTotal();
        }
    }
    function aumentar(esto,elemento){
        if(!esto.parentNode.querySelector('input[type=number]').readOnly){
            esto.parentNode.querySelector('input[type=number]').stepUp();
            formAdd.setItemValue(elemento,Number(esto.parentNode.querySelector('input[type=number]').value));
            if(esto.parentNode.querySelector('input[type=number]').id == "q5") calcularTotal();
        }
    }
    function calcularTotal(){
        var diashospedaje = Number(document.getElementById("q5").value);
        if(diashospedaje == 0) formAdd.setItemValue("diashospedaje",1);
        console.log(diashospedaje,"que paso papu");
        var costo = Number(formAdd.getItemValue("costo")).toFixed(2);
        if(formAdd.getItemValue("costo") != ""){
            var gt = Number(formAdd.getItemValue("costo")) + Number(formAdd.getItemValue("iva")) + Number(formAdd.getItemValue("impsobrehosp"));
            var cant = Number(diashospedaje);
            if(diashospedaje == "") cant = 1; 
            formAdd.setItemValue("grantotal",Number(gt * cant).toFixed(2));
        }else{
            var cant = Number(diashospedaje);
            if(diashospedaje == "") cant = 1; 
            formAdd.setItemValue("grantotal", Number(0).toFixed(2));
        }
    }
</script>
</html>