<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link rel="stylesheet" href="../fonts/css/font-awesome.css">
    <link rel="stylesheet" href="../icomoon/demo-files/demo.css">
    <link rel="stylesheet" href="../icomoon/style.css"></head>
    <title>Loading....</title>
    <style>
    i.loading{
        font-size: 3em;
    }
    html{
        width:100%;
        height:100%;
        overflow-x:hidden;overflow-y:hidden
    }
    </style>
</head>
<body style="display: flex;justify-content: center;align-items: center;width:100%;height:100%;background-color:#016830;overflow-x:hidden;overflow-y:hidden">
    <div style="display: flex;justify-content: center;align-items: center;height:100%!important;over-flow-y:hidden!important;background-color:#016830">
        <i id="loading" style="color:white;font-size:10em;" class='fa fa-spinner fa-spin' aria-hidden='true'></i><span class='sr-only'>Refreshing...</span>
    </div>
</body>
</html>