<?php require_once 'auth.php'; 
if($_GET){
	if(isset($_GET["idCliente"]) && !empty($_GET["idCliente"])){
		echo "<script>";
		echo "var idCliente = '".$_GET["idCliente"] ."';";
		if(isset($_GET["client_name"]) && !empty($_GET["client_name"])){
			echo "var client_name = '".$_GET["client_name"] ."';";
		}
		echo "</script>".PHP_EOL;
    }
    if(isset($_GET["archivo"]) && !empty($_GET["archivo"])){
		echo "<script>";
		echo "var archivo = '".$_GET["archivo"] ."';";
		if(isset($_GET["nombreLayout"]) && !empty($_GET["nombreLayout"])){
			echo "var nombreLayout = '".$_GET["nombreLayout"] ."';";
		}
		echo "</script>".PHP_EOL;
	}
	if(isset($_GET["registro"]) && !empty($_GET["registro"])){
		echo "<script>";
		echo "var registro = '".$_GET["registro"] ."';";
		echo "</script>".PHP_EOL;
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Yard Inventory</title>
	<script type="text/javascript" src="../js/moment.js"></script>
	<link rel="stylesheet" href="../codebase/fonts/font_roboto/roboto.css">
	<link rel="stylesheet" href="../codebase/dhtmlx.css">
	<!-- <link rel="stylesheet" href="../fonts/css/font-awesome.css"> -->
	<script type="text/javascript" src="../codebase/dhtmlx.js"></script>
	<script type="text/javascript" src="../js/jquery.min.js"></script>
	<script type="text/javascript" src="../js/rutas_node.js"></script>
	<link rel="stylesheet" href="../css/others_styles.css">
	<link rel="stylesheet" href="../icomoon/demo-files/demo.css">
    <link rel="stylesheet" href="../icomoon/style.css">
	<link rel="stylesheet" href="../fonts/css/font-awesome.css">
	</head>
	<style>
	html, body {
    width: 100%;
    height: 100%;
    margin: 0;
	padding: 0;
	font-family:'Roboto'
}
.dhx_toolbar_material div.dhx_toolbar_btn input.dhxtoolbar_input {
	margin-top:6px;
}
.pantalla_completa{
	z-index: 1005;
    margin-left: 0!important;
    margin-right: 0!important;
	width: 100%!important;
    height: 100%!important
}
.dhxwin_active{
    z-index: 1005;
    /* margin-left: 10%; */
    /* margin-right: 10%; */
	width: 80%;
    height: 100%;
}
.dhxwin_inactive{
    /* margin-left: 10%; */
    /* margin-right: 10%; */
	width: 80%;
	max-width:100%;
    height: 100%;
}
.dhxwin_brd{
    left:2px!important;
    top:48px!important;
	width: 98.7%!important;
	height:100px;
}
.dhx_cell_wins{
	width: 98.7%!important;
}
.dhx_cell_cont_wins{
    left: 0px;
    top: 0px;
    overflow: auto;
	width: 100%!important;
	
}

.dhxform_base{
    width: 99%;
}
.dhxform_label{
    width: 40%!important;
}
.dhxform_control{
    width: 50%!important;
}
.dhxform_textarea{
    width: 100%!important;
}
.dhxcombo_material{
    width: 100%!important;
}
.dhxcombo_input{
    width: 100%!important;
}
.dhxform_btn{
    left:40%;
    width: 20%!important;
    text-align: center;
}
.dhxform_obj_material fieldset.dhxform_fs{
	width:calc(100% - 10px)!important
}
/* enabled, not checked */
.dhxform_obj_material div.dhxform_img.btn2state_0 {
	background-image: url("../button2state/material/toggle_off.png");
	width: 42px;
	height: 24px;
}
/* enabled, checked */
.dhxform_obj_material div.dhxform_img.btn2state_1 {
	background-image: url("../button2state/material/toggle_on.png");
	width: 42px;
	height: 24px;
}
/* disabled, not checked */
.dhxform_obj_material div.disabled div.dhxform_img.btn2state_0 {
	background-image: url("../button2state/material/toggle_off_dis.png");
	width: 42px;
	height: 24px;
}
/* disabled, checked */
.dhxform_obj_material div.disabled div.dhxform_img.btn2state_1 {
	background-image: url("../button2state/material/toggle_on_dis.png");
	width: 42px;
	height: 24px;
}

/* common */
/* fix label align a bit */
.dhxform_obj_material div.dhxform_item_label_right div.dhxform_label div.dhxform_label_nav_link {
	padding-top: 2px;
}
.dhxform_obj_material .dhxform_select{
	width:100%!important;
}
textarea[name="correos"]{
	width:200%!important;
}
	</style>
</head>
<body onload="Cargar()"></body>
<script type="text/javascript">
	var layout;
	function Cargar()
	{
        dhxWindow = new dhtmlXWindows("material");	
        layout = new dhtmlXLayoutObject({
			parent: document.body,
			pattern: "1C",
			offsets: {
				top: 0,
				right: 0,
				bottom: 0,
				left: 0
			},
        });
        var datos = "";
        if(typeof idCliente !== "undefined"){
            if(datos.length == 0){
                datos += "?idCliente="+idCliente;
            }else{
                datos += "&idCliente="+idCliente;
            }
        }
        if(typeof client_name !== "undefined"){
            if(datos.length == 0){
                datos += "?client_name="+client_name;
            }else{
                datos += "&client_name="+client_name;
            }
		}
		if(typeof registro !== "undefined"){
			if(datos.length == 0){
                datos += "?registro="+registro;
            }else{
                datos += "&registro="+registro;
            }
		}
		console.log(nombreLayout,archivo+datos);
        layout.cells("a").setText("<b>"+nombreLayout+"</b>"+"<span id='fecha' style='float:right'>"+moment().format("YYYY-MM-DD HH:mm:ss")+"</span>");
		layout.cells("a").attachURL(archivo+datos);
		setInterval(() => {
			$("#fecha").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		}, 1000);
	}
</script>
</html>