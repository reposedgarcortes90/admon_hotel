<?php 
/* require_once 'auth.php'; */ 
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>barChart Horizontal</title>
	<link rel="stylesheet" href="../codebase/dhtmlx.css">
	<script type="text/javascript" src="../js/moment.js"></script>
	<script src="../codebase/dhtmlx.js"></script>
    <link href="../css/fontawesome.css" rel="stylesheet">
    <link href="../css/brands.css" rel="stylesheet">
    <link href="../css/solid.css" rel="stylesheet">
	<script src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="../js/rutas_node.js"></script>
    <!-- <script src="../js/functions.js"></script> -->
	<link rel="stylesheet" href="../css/others_styles.css">
	<style>
html, body {
    width: 100%;
    height: 100%;
    margin: 0;
    padding: 0;
}
.pantalla_completa{
	z-index: 1005;
    margin-left: 0!important;
    margin-right: 0!important;
	width: 100%!important;
    height: 100%!important
}
.dhxwin_active{
    z-index: 1005;
    /* margin-left: 10%; */
    /* margin-right: 10%; */
	width: 80%;
    /* height: 100%; */
}
.dhxwin_inactive{
    /* margin-left: 10%;
    margin-right: 10%; */
	width: 80%;
	max-width:100%;
    /* height: 100%; */
}
.dhxwin_brd{
    left:2px!important;
    top:48px!important;
	width: 98.7%!important;
	height:100px;
}
.dhx_cell_wins{
	width: 98.7%!important;
}
.dhx_cell_cont_wins{
    left: 0px;
    top: 0px;
    overflow: auto;
	width: 100%!important;
	
}

.dhxform_base{
    width: 99%;
}
.dhxform_label{
    width: 40%!important;
}
.dhxform_control{
    width: 50%!important;
}
.dhxform_textarea{
    width: 100%!important;
}
.dhxcombo_material{
    width: 100%!important;
}
.dhxcombo_input{
    width: 100%!important;
}
.dhxform_btn{
    left:40%;
    width: 20%!important;
    text-align: center;
}
.dhxform_obj_material fieldset.dhxform_fs{
	width:calc(100% - 10px)!important
}
/* enabled, not checked */
.dhxform_obj_material div.dhxform_img.btn2state_0 {
	background-image: url("../button2state/material/toggle_off.png");
	width: 42px;
	height: 24px;
}
/* enabled, checked */
.dhxform_obj_material div.dhxform_img.btn2state_1 {
	background-image: url("../button2state/material/toggle_on.png");
	width: 42px;
	height: 24px;
}
/* disabled, not checked */
.dhxform_obj_material div.disabled div.dhxform_img.btn2state_0 {
	background-image: url("../button2state/material/toggle_off_dis.png");
	width: 42px;
	height: 24px;
}
/* disabled, checked */
.dhxform_obj_material div.disabled div.dhxform_img.btn2state_1 {
	background-image: url("../button2state/material/toggle_on_dis.png");
	width: 42px;
	height: 24px;
}

/* common */
/* fix label align a bit */
.dhxform_obj_material div.dhxform_item_label_right div.dhxform_label div.dhxform_label_nav_link {
	padding-top: 2px;
}
.dhxform_obj_material .dhxform_select{
	width:100%!important;
}
textarea[name="correos"]{
	width:200%!important;
}
.dhxtoolbar_input{
    top:7px;
}
.dhx_toolbar_material.dhxtoolbar_icons_18 div.dhx_toolbar_btn, .dhx_toolbar_material.dhxtoolbar_icons_18 div.dhx_toolbar_arw, .dhx_toolbar_material.dhxtoolbar_icons_18 div.dhx_toolbar_text{
    line-height:12px
}
	</style>
</head>
<body onload="Cargar()"></body>

<script type="text/javascript">
	var layout,grafica;

	function Cargar()
	{
        var data = [
            { id:1, sales:20, year:"02", color: "#ee4339"},
            { id:2, sales:55, year:"03", color: "#ee9336"},
            { id:3, sales:40, year:"04", color: "#eed236"},
            { id:4, sales:78, year:"05", color: "#d3ee36"},
            { id:5, sales:61, year:"06", color: "#a7ee70"},
            { id:6, sales:35, year:"07", color: "#58dccd"},
            { id:7, sales:80, year:"08", color: "#36abee"},
            { id:8, sales:50, year:"09", color: "#476cee"},
            { id:9, sales:65, year:"10", color: "#a244ea"},
            { id:10, sales:59, year:"11", color: "#e33fc7"}
        ];
        layout = new dhtmlXLayoutObject({
            parent: document.body,
            pattern: "1C",
            offsets: {
                top: 0,
                right: 0,
                bottom: 0,
                left: 0
            },
        });
        layout.cells("a").hideHeader();
        grafica = layout.cells("a").attachChart(
            {
				view:"barH",
				container:"chartDiv",
				value:"#sales#",
				label:"#sales#",
				color:"#color#",
				barWidth:30,
				radius:2,
				tooltip:{
					template:"#sales#"
				},
				yAxis:{
					template:"'#year#"
				},
				xAxis:{
					start:0,
					end:100,
					step:10,
					template:function(obj){
						return (obj%20?"":obj)
					}
				},
				padding:{
					left: 30
				}
			}
        );
        grafica.parse(data,"json");
            

	}
	
</script>
</html>