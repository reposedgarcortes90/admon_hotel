<?php
$cache_expire = session_cache_expire();

/* start the session */
$lifetime=600;
session_set_cookie_params($lifetime);
session_start();
echo "The cached session pages expire after $cache_expire minutes";
?>