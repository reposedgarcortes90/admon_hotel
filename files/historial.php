<?php 
require_once 'auth.php'; 
if($_GET){
	if(isset($_GET["idCliente"]) && !empty($_GET["idCliente"])){
		echo "<script>";
		echo "var idCliente = '".$_GET["idCliente"] ."';";
		if(isset($_GET["client_name"]) && !empty($_GET["client_name"])){
			echo "var client_name = '".$_GET["client_name"] ."';";
		}
		echo "</script>".PHP_EOL;
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Historial</title>
	<link rel="stylesheet" href="../codebase/dhtmlx.css">
	<script type="text/javascript" src="../js/moment.js"></script>
	<script src="../codebase/dhtmlx.js"></script>
    <link href="../css/fontawesome.css" rel="stylesheet">
    <link href="../css/brands.css" rel="stylesheet">
    <link href="../css/solid.css" rel="stylesheet">
	<script src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="../js/rutas_node.js"></script>
    <!-- <script src="../js/functions.js"></script> -->
	<link rel="stylesheet" href="../css/others_styles.css">
	<style>
html, body {
    width: 100%;
    height: 100%;
    margin: 0;
    padding: 0;
}
.pantalla_completa{
	z-index: 1005;
    margin-left: 0!important;
    margin-right: 0!important;
	width: 100%!important;
    height: 100%!important
}
.dhxwin_active{
    z-index: 1005;
    /* margin-left: 10%; */
    /* margin-right: 10%; */
	width: 80%;
    /* height: 100%; */
}
.dhxwin_inactive{
    /* margin-left: 10%;
    margin-right: 10%; */
	width: 80%;
	max-width:100%;
    /* height: 100%; */
}
.dhxwin_brd{
    left:2px!important;
    top:48px!important;
	width: calc(100% - 6px)!important;
	height:100px;
}
.dhx_cell_wins{
	width: calc(100% - 6px)!important;
}
.dhx_cell_cont_wins{
    left: 0px;
    top: 0px;
    overflow: auto;
	width: 100%!important;
	
}

.dhxform_base{
    width: 99%;
}
.dhxform_label{
    width: 40%!important;
}
.dhxform_control{
    width: 50%!important;
}
.dhxform_textarea{
    width: 100%!important;
}
.dhxcombo_material{
    width: 100%!important;
}
.dhxcombo_input{
    width: 100%!important;
}
.dhxform_btn{
    left:40%;
    width: 20%!important;
    text-align: center;
}
.dhxform_obj_material fieldset.dhxform_fs{
	width:calc(100% - 10px)!important
}
/* enabled, not checked */
.dhxform_obj_material div.dhxform_img.btn2state_0 {
	background-image: url("../button2state/material/toggle_off.png");
	width: 42px;
	height: 24px;
}
/* enabled, checked */
.dhxform_obj_material div.dhxform_img.btn2state_1 {
	background-image: url("../button2state/material/toggle_on.png");
	width: 42px;
	height: 24px;
}
/* disabled, not checked */
.dhxform_obj_material div.disabled div.dhxform_img.btn2state_0 {
	background-image: url("../button2state/material/toggle_off_dis.png");
	width: 42px;
	height: 24px;
}
/* disabled, checked */
.dhxform_obj_material div.disabled div.dhxform_img.btn2state_1 {
	background-image: url("../button2state/material/toggle_on_dis.png");
	width: 42px;
	height: 24px;
}

/* common */
/* fix label align a bit */
.dhxform_obj_material div.dhxform_item_label_right div.dhxform_label div.dhxform_label_nav_link {
	padding-top: 2px;
}
.dhxform_obj_material .dhxform_select{
	width:100%!important;
}
textarea[name="correos"]{
	width:200%!important;
}
.dhxtoolbar_input{
    top:7px;
}
.dhx_toolbar_material.dhxtoolbar_icons_18 div.dhx_toolbar_btn, .dhx_toolbar_material.dhxtoolbar_icons_18 div.dhx_toolbar_arw, .dhx_toolbar_material.dhxtoolbar_icons_18 div.dhx_toolbar_text{
    line-height:12px
}
div.gridbox .filter input, div.gridbox .filter select, div.combo{
	width:calc(100% - 8px)!important;
}
	</style>
</head>
<body onload="Cargar()"></body>

<script type="text/javascript">
	var layout,menu,empresas,nombre_catalogo, grid,json_form, datastore,item,toolbar,url_get_catalogo_elemento,dhxWindow,selectedId,desde,hasta,elementoseleccionado;
    var fechaPrimerDia = moment(moment().subtract(2, 'days').format("YYYY-MM-DD")).format('YYYY-MM-DD');
	var fechaUltimoDia = moment().format("YYYY-MM-DD");
	url_get_catalogo_elemento = "../../node_hotel/historial_hotel?instruccion=leer";
	var tipoHabitaciones,clientes,habitaciones,empleados,empresas;
	LoadMenu();
	function Cargar()
	{
		$.get("../../node_hotel/tiposhabitaciones_hotel?instruccion=leer",function(thab){
			$.get("../../node_hotel/clientes_hotel?instruccion=leer",function(cli){
				$.get("../../node_hotel/habitaciones_hotel?instruccion=leer",function(hab){
					$.get("../../node_hotel/habitaciones_hotel?instruccion=leer",function(emp){
						$.get("../../node_hotel/empresas_hotel?instruccion=leer",function(empr){
							empleados = emp;
							tipoHabitaciones = thab;
							clientes = cli;
							habitaciones = hab;
							empresas = empr;
							dhxWindow = new dhtmlXWindows("material");
							datastore= new dhtmlXDataStore({
								url:url_get_catalogo_elemento,
								datatype:"json"
							});
							layout = new dhtmlXLayoutObject({
								parent: document.body,
								pattern: "1C",
								offsets: {
									top: 0,
									right: 0,
									bottom: 0,
									left: 0
								},
							});
							layout.cells("a").hideHeader();
					
							toolbar = layout.cells("a").attachToolbar();
							// toolbar.addButton("agregar", 1, "<i class='fa fa-plus-circle' style='color:#0f9038' aria-hidden='true'></i>", null, null);
							// toolbar.addButton("editar", 2, "<i class='fa fa-pencil-alt' style='color:#2475ff' aria-hidden='true'></i>", null, null);
							// toolbar.addButton("eliminar", 3, "<i class='fa fa-trash-alt' style='color:#3869ad' aria-hidden='true'></i>", null, null);
							toolbar.addButton("exportar_excel", 5, "<i class='fa fa-file-excel' style='color:#0f9038' aria-hidden='true'></i>", null, null);
							toolbar.addButton("refrescar", 7, "<i class='fa fa-sync-alt' style='color:#0000ff;font-weight:bolder' aria-hidden='true'></i>", null, null);
							toolbar.attachEvent('onClick',function(id){
								if (id=="refrescar"){
									Refresh();
								}
								if (id=="filtrar"){
									let fecha_desde=desde.getDate(true);
									let fecha_hasta=hasta.getDate(true);
									Filtrar(fecha_desde,fecha_hasta);
								}
								if(id == "agregar"){
									AgregarElemento();
								}
								if(id == "editar"){
									selectedId = grid.getSelectedRowId();
									if(selectedId !== null){
										EditarElemento(selectedId);
									}else{
										dhtmlx.alert({
											title:"Alerta",
											type:"alert-warning",
											text:"Debes seleccionar un elemento de la cuadricula primero"
										});
									}
								}
								if(id == "eliminar"){
									selectedId = grid.getSelectedRowId();
									if(selectedId !== null){
										EliminarElemento(selectedId);
									}else{
										dhtmlx.alert({
											title:"Alerta",
											type:"alert-warning",
											text:"Debes seleccionar un elemento de la cuadricula primero"
										});
									}
								}
								if(id == "exportar_pdf"){
									grid.toPDF('../codebase/grid-pdf-php/generate.php');
								}
								if(id == "exportar_excel"){
									grid.toExcel('../codebase/grid-excel-php/generate.php');	
								}
							});
					
							grid=layout.cells("a").attachGrid();
							grid.setHeader("Usuario,Fecha,Acción,Modúlo,Datos,Evento",null,
							[
								"text-align:center",
								"text-align:center",
								"text-align:center",
								"text-align:center",
								"text-align:center",
								"text-align:center"
							]);
							grid.attachHeader("#combo_filter,#text_filter,#combo_filter,#combo_filter,#text_filter,#combo_filter");
							grid.setInitWidths("*,*,*,*,*,*");
							grid.setColAlign("center,center,center,center,center,center");
							grid.setColumnIds("usuariocrea,fechacrea,accion,modulo,objeto,evento");
							grid.setColSorting("str,str,str,str,str,str");
							grid.setColTypes("usuariocrea,fecha,accion,modulo,objeto,evento");
							// grid.setColumnHidden(1, true);
							grid.enableContextMenu(menu);
							grid.setImagePath("../codebase/imgs/");
							grid.setEditable(false);
							grid.enableMultiline(true);
							grid.enableKeyboardSupport(true);
							grid.enableBlockSelection(true);
							grid.attachEvent("onKeyPress",onKeyPressed); 
							grid.init();
							grid.sync(datastore);
							grid.attachEvent("onBeforeSelect", onBeforeSelect);
						});
					});
				});
			});
		});
	}
	function LoadMenu()
	{
		menu=new dhtmlXMenuObject();
		menu.renderAsContextMenu();
		menu.attachEvent("onClick", onButtonClick);
		menu.loadStruct("../data/xml/Registro_ContextMenuReportes.xml");
	}
	function onButtonClick(menuitemId, type) {
		if (menuitemId == "excel") {
			grid.toExcel('../codebase/grid-excel-php/generate.php');
		}

		if (menuitemId == "pdf") {
			grid.toPDF('../codebase/grid-pdf-php/generate.php');
		}
		if (menuitemId == "refresh") {
			Refresh();
		}
    }
    
    function onBeforeSelect(new_row,old_row,new_col_index){
        // console.log(new_row,old_row,new_col_index)
        elementoseleccionado = datastore.item(new_row);
        // console.log(elementoseleccionado,"elementoseleccionado")
        return true;
    }
	function onEditCell(stage,rId,cInd,nValue,oValue){
        // console.log(stage,rId,cInd,nValue,oValue)
		//stage Estado del Editor
		//rId Id del Row
		//cInd Indice de la celda
		//nValue Valor nuevo solo si el stage es 2
		//oValue Valor viejo solo si el stage es 2	
        item = datastore.item(rId);
        // console.log(item);
        if(stage == 0){//El editor esta abierto
            // EditarCellElemento(rId);
			if(cInd == 4) return true;
		}
        if(stage == 1){//El editor esta abierto
            if(cInd == 4){
                EditarCellElementoActivo(rId);
                return true;
            }
		}
        else if(stage == 2){//El editor esta cerrado
            // console.log("entro")
			if(cInd != 3){
				EditarCellElemento(rId);
                return true;	
			}
		} 				
	}
	function Refresh(){
        // url_get_catalogo_elemento = node_chapel_web + "entradas_salidas?instruccion=get_registros&cliente="+client_id+"&fechadesde="+fechaPrimerDia+"&fechahasta="+fechaUltimoDia+"&tipoDocumento=4";
		datastore.clearAll();
			grid.clearAll();
			datastore.load(url_get_catalogo_elemento,function(){
			// grid.clearAll();
			grid.sync(datastore);
		});
    }
    function Filtrar(fecha_desde,fecha_hasta){
        // console.log(fecha_desde,fecha_hasta);
        url_get_catalogo_elemento = node_chapel_web + "entradas_salidas?instruccion=get_registros&cliente="+client_id+"&fechadesde="+fecha_desde+"&fechahasta="+fecha_hasta+"&tipoDocumento=9";
		datastore.clearAll();
			grid.clearAll();
			datastore.load(url_get_catalogo_elemento,function(){
			grid.clearAll();
			grid.sync(datastore);
		});
	}
	function onKeyPressed(code,ctrl,shift){
		if(code==67&&ctrl){
			if (!grid._selectionArea){
				return dhtmlx.alert(
					{
						title:"Alerta",
						type:"alert",
						text:"Debes seleccionar un bloque de la cuadricula previamente"
					}
				);
			} 
			grid.setCSVDelimiter("\t");
			grid.copyBlockToClipboard();
		}
		return true;
	}
	function AgregarElemento(){
		// $.get(node_chapel_web + "get_uuid", function(uuid){
			var width = document.body.clientWidth - 200;
        	var height = document.body.clientHeight;
			var ventanaAgregar = dhxWindow.createWindow({
				id:"ventanaAgregar",
				// left:Number(window.innerWidth * 0.1),
				text:"<b>Agregar Cliente</b>",
				// top:600,
				width:width,
				height:height,
				center:true,
				modal:true
			});
			dhxWindow.window("ventanaAgregar").button("minmax").attachEvent("onClick", function(win, button){
				var elemento = document.getElementsByClassName("dhxwin_active");
				for(var i = 0; i < elemento.length; i++){
					var elemento2 = document.getElementsByClassName("pantalla_completa");
					if(elemento2.length > 0){
						elemento[i].className = "dhxwin_active";
						dhxWindow.window("ventanaAgregar").setPosition(Number(window.innerWidth * 0.1), 0);
					}else{
						dhxWindow.window("ventanaAgregar").setPosition(0, 0);
						elemento[i].className += " pantalla_completa";
					}
				}
				return true;
			});
			document.body.onresize = ()=>{
				var width = document.body.clientWidth - 200;
				var height = document.body.clientHeight;
				try{
                	dhxWindow.window("ventanaAgregar").setDimension(width, height);
				}catch(error){
				}
			};
			var formAdd = ventanaAgregar.attachForm();
			formAdd.loadStruct("../data/json/agregar_clientes_hotel.json");
			formAdd.attachEvent("OnXLE", function(){
                formAdd.hideItem("activo");
                // formAdd.setItemValue("fecha", moment().format("YYYY-MM-DD HH:mm:ss"));
                // formAdd.setItemValue("quien_captura", usuario);
			});
			formAdd.attachEvent("onButtonClick", function(name){
				if (name=="enviar"){
					if(formAdd.validate()){
                        var values = formAdd.getFormData(true);
                        values.usuariocrea = nombreusuario;
						$.post("../../node_hotel/clientes_hotel?instruccion=agregar",values, function(data, textStatus, xhr) {
                            // console.log(data);
							if(textStatus == "success"){
								if(data !== "ERROR"){
									Refresh();
									ventanaAgregar.close();
								}else{
                                    Refresh();
                                    dhtmlx.alert({
                                        title:"ERROR",
                                        type:"alert-warning",
                                        text:"Ocurrio un error al agregar Cliente"
                                    });	
								}
							}
						});
					}
				}
			});
			formAdd.enableLiveValidation(true);			
		// });
	}
	function EditarElemento(selectedId){
		item = datastore.item(selectedId);
        delete item.id;
        // console.log(item);
		var width = document.body.clientWidth - 200;
		var height = document.body.clientHeight;
		var ventanaEditar = dhxWindow.createWindow({
			id:"ventanaEditar",
			// left:Number(window.innerWidth * 0.1),
			text:"<b>Editar Cliente</b>",
			// top:600,
			width:width,
			height:height,
			center:true,
			modal:true
		});
		dhxWindow.window("ventanaEditar").button("minmax").attachEvent("onClick", function(win, button){
			var elemento = document.getElementsByClassName("dhxwin_active");
			for(var i = 0; i < elemento.length; i++){
				var elemento2 = document.getElementsByClassName("pantalla_completa");
				if(elemento2.length > 0){
					elemento[i].className = "dhxwin_active";
					dhxWindow.window("ventanaEditar").setPosition(Number(window.innerWidth * 0.1), 0);
				}else{
					dhxWindow.window("ventanaEditar").setPosition(0, 0);
					elemento[i].className += " pantalla_completa";
				}
			}
			return true;
		});
		document.body.onresize = ()=>{
			var width = document.body.clientWidth - 200;
			var height = document.body.clientHeight;
			try{
				dhxWindow.window("ventanaEditar").setDimension(width, height);
			}catch(error){
			}
			
		};
		var formEdit = ventanaEditar.attachForm();
		formEdit.loadStruct("../data/json/editar_clientes_hotel.json");
		formEdit.attachEvent("OnXLE", function(){
            formEdit.setItemValue("itemid", item.itemid);
            formEdit.setItemValue("activo", item.activo);
            formEdit.setItemValue("nombre", item.nombre);
			formEdit.setItemValue("edad", item.edad);
			formEdit.setItemValue("correo", item.correo);
			formEdit.setItemValue("direccion", item.direccion);
			formEdit.setItemValue("estado", item.estado);
			formEdit.setItemValue("ciudad", item.ciudad);
			formEdit.setItemValue("telefono", item.telefono);
			formEdit.setItemValue("rfc", item.rfc);
            formEdit.setItemLabel("enviar","<b>EDITAR</b>");
            var dhxCombito2 = formEdit.getCombo("empresa");
            dhxCombito2.attachEvent("onXLE", function(){					
                var option=dhxCombito2.getIndexByValue(item.empresa);
                if(option !== -1){
                    dhxCombito2.selectOption(option);
                }
			});
			$.get("../../node_hotel/empresas_hotel?instruccion=leer",function(empresas){
				dhxCombito2.attachEvent("onChange", function(index,value){	
					// console.log(index,"index");
					// console.log(value,"value");		
					let filtro;
					if(value != "ND") filtro = empresas.filter((a)=>{ return a.itemid == index; })[0];
					// console.log("filtro",filtro);
					if(value != "ND") formEdit.setItemValue("rfc", filtro.rfc);
					if(value == "ND") formEdit.setItemValue("rfc", "");
					if(value == "ND")	formEdit.enableItem("rfc");
					else formEdit.disableItem("rfc");
				});
			});
		});
		formEdit.attachEvent("onButtonClick", function(name){
			if (name=="enviar"){
				if(formEdit.validate()){
                    var values = formEdit.getFormData(true);
                    values.usuarioedita = nombreusuario;
                    $.post("../../node_hotel/clientes_hotel?instruccion=actualizar",values, function(data, textStatus, xhr) {
                        if(data !== "ERROR"){
                            Refresh();
							ventanaEditar.close();
                        }else{
                            dhtmlx.alert({
                                title:"ERROR",
                                type:"alert-error",
                                text:"Ocurrio un error al editar"
                            });	
                        }
                    });					
				}
			}
		});
		formEdit.enableLiveValidation(true);	
	}
	function EliminarElemento(selectedId){
		item = datastore.item(selectedId);
		delete item.id;
		dhtmlx.confirm({
			title: "Confirmación",
			type:"confirm-warning",
			text: "Desea eliminar el registro seleccionado?",
			ok: "Si",
			cancel: "No",
			callback: function(result){
				if(result){		
                    item.usuarioelimina = nombreusuario;			
					$.post("../../node_hotel/clientes_hotel?instruccion=eliminar",item, function(data, textStatus, xhr) {
						if(textStatus == "success"){
							if(data !== "ERROR"){
								Refresh();
							}else{
								dhtmlx.alert({
									title:"ERROR",
									type:"alert-error",
									text:"Ocurrio un error al eliminar registro"
								});	
								Refresh();
							}
						}
					});
				}
			}
		});	
	}
	function EditarCellElemento(selectedId){
		item = datastore.item(selectedId);
		delete item.id;
        // item.instruccion = "ud";
        // console.log(item);
        var empresa = grid.cells(selectedId,3).getValue();
        item.empresa = empresa;
		$.post("../../node_hotel/clientes_hotel?instruccion=actualizar",item, function(data, textStatus, xhr) {
			if(textStatus == "success"){
				if(data !== "ERROR"){
					Refresh();
				}else{
                    dhtmlx.alert({
                        title:"Warning",
                        type:"alert-warning",
                        text:"Ha ocurrido un error al editar el elemento"
                    });	
                    Refresh();	
				}
			}
		});	
    }
    function EditarCellElementoActivo(selectedId){
		item = datastore.item(selectedId);
		delete item.id;
        item.instruccion = "ud";
        if(item.activo == 1) item.activo = 0;
        else item.activo = 1;
		$.post("../../node_hotel/clientes_hotel?instruccion=actualizar",item, function(data, textStatus, xhr) {
			if(textStatus == "success"){
				if(data !== "ERROR"){
					Refresh();
				}else{
                    dhtmlx.alert({
                        title:"Warning",
                        type:"alert-warning",
                        text:"Ha ocurrido un error al editar el elemento"
                    });	
                    Refresh();	
				}
			}
		});	
    }

    function eXcell_es(cell){ //the eXcell name is defined here
        if (cell){                // the default pattern, just copy it
            this.cell = cell;
            this.grid = this.cell.parentNode.grid;
        }
        this.edit = function(){}  //read-only cell doesn't have edit method
        // the cell is read-only, so it's always in the disabled state
        this.isDisabled = function(){ return true; }
        this.setValue=function(val){
            let icono,row_id;
            row_id = this.cell.parentNode.idd;
            item = datastore.item(row_id);
			if(item.entrada_salida == 0){
                this.cell.style.color = "green";
                icono = `<i style="font-weigth:bolder;font-size:1.5em;" class="fa fa-arrow-circle-left" aria-hidden="true"></i> <span style="color:black;font-weight:bolder;font-size:2em;">|</span>`;
			}else{
                this.cell.style.color = "red";
                icono = `<span style="color:black;font-weight:bolder;font-size:2em;">|</span> <i style="font-weigth:bolder;font-size:1.5em;" class="fa fa-arrow-circle-right" aria-hidden="true"></i>`;
			}
			this.setCValue(icono,val);
        }
    }
    eXcell_es.prototype = new eXcell;// nests all other methods from the base class
    function eXcell_objeto(cell){ //the eXcell name is defined here
        if (cell){                // the default pattern, just copy it
            this.cell = cell;
            this.grid = this.cell.parentNode.grid;
            // eXcell_ed.call(this);
        }
        this.edit = function(){}  //read-only cell doesn't have edit method
		// the cell is read-only, so it's always in the disabled state
		this.getValue=function(){
			// console.log(this.cell);
			return this.cell.innerText; // get button label
		}
        this.isDisabled = function(){ return true; }
        this.setValue=function(val){
			var f = JSON.parse(val);
			// console.log("FFFFFFFFFFFF")
			row_id = this.cell.parentNode.idd;
			var item = datastore.item(row_id);
	
			var entrada = "";
			var respuesta = "";
			entrada +="<div style='width:100%; word-wrap: break-word;word-break: break-all;height:auto;text-align:justify'>";
			switch (item.modulo) {
				case "InicioSesion":
					if(f.in.nombreusuario != ""  && f.in.nombreusuario != null) entrada += "<b>Usuario:</b>&nbsp;&nbsp"+f.in.nombreusuario+"<br>";
					if(f.in.clave != ""  && f.in.clave != null) entrada += "<b>Clave:</b>&nbsp;&nbsp"+f.in.clave+"<br>";
					if(f.response != ""  && f.response != null) entrada += "<b>Status:</b>&nbsp;&nbsp"+f.response+"<br>";
					if(f.user.usuariocrea != ""  && f.user.usuariocrea != null) entrada += "<b>Creó:</b>&nbsp;&nbsp"+f.user.usuariocrea+"<br>";
					if(f.user.fechacrea != ""  && f.user.fechacrea != null) entrada += "<b>F. Creó:</b>&nbsp;&nbsp"+moment(Number(f.user.fechacrea) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
					if(f.user.usuarioedita != ""  && f.user.usuarioedita != null) entrada += "<b>Editó:</b>&nbsp;&nbsp"+f.user.usuarioedita+"<br>";
					if(f.user.fechaedita != ""  && f.user.fechaedita != null) entrada += "<b>F. Editó:</b>&nbsp;&nbsp"+moment(Number(f.user.fechaedita) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
					if(f.user.usuarioelimina != ""  && f.user.usuarioelimina != null) entrada += "<b>Eliminó:</b>&nbsp;&nbsp"+f.user.usuarioelimina+"<br>";
					if(f.user.fechaelimina != ""  && f.user.fechaelimina != null) entrada += "<b>F. Eliminó:</b>&nbsp;&nbsp"+moment(Number(f.user.fechaelimina) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
					if(f.user.fechultimologin != "" && f.user.fechultimologin != null) entrada += "<b>Ultimo Inicio:</b>&nbsp;&nbsp"+moment(Number(f.user.fechultimologin) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
					if(f.user.fechaultimologout != ""  && f.user.fechaultimologout != null) entrada += "<b>Ultima Salida:</b>&nbsp;&nbsp"+moment(Number(f.user.fechaultimologout) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
					if(f.user.nombreempleado != ""  && f.user.nombreempleado != null) entrada += "<b>Empleado:</b>&nbsp;&nbsp"+f.user.nombreempleado+"<br>";
					entrada +="</div>";
					this.setCValue(entrada,val);
				break;
				case "TiposHabitaciones":
					if(item.evento == "update"){
						entrada += "<b>Estado:</b>&nbsp;&nbsp;Editado<br>";
						if(f.usuarioedita != ""  && f.usuarioedita != null) entrada += "<b>Editó:</b>&nbsp;&nbsp"+f.usuarioedita+"<br>";
						if(f.fechaedita != "" && f.fechaedita != null) entrada += "<b>F. Editó:</b>&nbsp;&nbsp"+moment(Number(f.fechaedita) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.nombre != ""  && f.nombre != null) entrada += "<b>Nombre:</b>&nbsp;&nbsp"+f.nombre+"<br>";
						if(f.tarifa != ""  && f.tarifa != null) entrada += "<b>Tarífa:</b>&nbsp;&nbsp<b>$</b>&nbsp;&nbsp;<b>$</b><span style='color:green'>"+Number(f.tarifa).toFixed(2)+"</span><br>";
					}
					if(item.evento == "insert"){
						entrada += "<b>Estado:</b>&nbsp;&nbsp;Agregado<br>";
						if(f.usuariocrea != ""  && f.usuariocrea != null) entrada += "<b>Creó:</b>&nbsp;&nbsp"+f.usuariocrea+"<br>";
						if(f.fechacrea != "" && f.fechacrea != null) entrada += "<b>F. Creó:</b>&nbsp;&nbsp"+moment(Number(f.fechacrea) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.nombre != ""  && f.nombre != null) entrada += "<b>Nombre:</b>&nbsp;&nbsp"+f.nombre+"<br>";
						if(f.tarifa != ""  && f.tarifa != null) entrada += "<b>Tarífa:</b>&nbsp;&nbsp<b>$</b>&nbsp;&nbsp;<b>$</b><span style='color:green'>"+Number(f.tarifa).toFixed(2)+"</span><br>";
					}
					if(item.evento == "delete"){						
						entrada += "<b>Estado:</b>&nbsp;&nbsp;Eliminado<br>";
						if(f.usuarioelimina != ""  && f.usuarioelimina != null) entrada += "<b>Elimíno:</b>&nbsp;&nbsp"+f.usuarioelimina+"<br>";
						if(f.fechaelimina != "" && f.fechaelimina != null) entrada += "<b>F. Eliminó:</b>&nbsp;&nbsp"+moment(Number(f.fechaelimina) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.usuarioedita != ""  && f.usuarioedita != null) entrada += "<b>Editó:</b>&nbsp;&nbsp"+f.usuarioedita+"<br>";
						if(f.fechaedita != "" && f.fechaedita != null) entrada += "<b>F. Editó:</b>&nbsp;&nbsp"+moment(Number(f.fechaedita) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.usuariocrea != ""  && f.usuariocrea != null) entrada += "<b>Creó:</b>&nbsp;&nbsp"+f.usuariocrea+"<br>";
						if(f.fechacrea != "" && f.fechacrea != null) entrada += "<b>F. Creó:</b>&nbsp;&nbsp"+moment(Number(f.fechacrea) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.nombre != ""  && f.nombre != null) entrada += "<b>Nombre:</b>&nbsp;&nbsp"+f.nombre+"<br>";
						if(f.tarifa != ""  && f.tarifa != null) entrada += "<b>Tarífa:</b>&nbsp;&nbsp<b>$</b>&nbsp;&nbsp;<b>$</b><span style='color:green'>"+Number(f.tarifa).toFixed(2)+"</span><br>";
					}
					entrada +="</div>";
					this.setCValue(entrada,val);
				break;
				case "SubirFotos":
					entrada += "<img style='width:100%' src='../../node_hotel/mostrarArchivo?tipo=tipoHabitacion&archivo="+f.image+"' onclick='abrirExterior(this)' />";
					entrada +="</div>";
					this.setCValue(entrada,val);
				break;
				case "CerrarSesion":
				
					if(f.user != ""  && f.user != null) entrada += "<b>Usuario:</b>&nbsp;&nbsp"+f.user+"<br>";
					// entrada += "<img style='width:100%' src='../../node_hotel/mostrarArchivo?tipo=tipoHabitacion&archivo="+f.image+"' onclick='abrirExterior(this)' />";
					entrada +="</div>";
					this.setCValue(entrada,val);
				break;
				case "habitaciones":
					
					
					if(item.evento == "insert"){
						entrada += "<b>Estado:</b>&nbsp;&nbsp;Agregada<br>";
						if(f.usuariocrea != ""  && f.usuariocrea != null) entrada += "<b>Creó:</b>&nbsp;&nbsp"+f.usuariocrea+"<br>";
						if(f.fechacrea != "" && f.fechacrea != null) entrada += "<b>F. Creó:</b>&nbsp;&nbsp"+moment(Number(f.fechacrea) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.nombre != ""  && f.nombre != null) entrada += "<b>Nombre:</b>&nbsp;&nbsp"+f.nombre+"<br>";
						if(f.numerohabitacion != ""  && f.numerohabitacion != null) entrada += "<b>Número de habitación:</b>&nbsp;&nbsp"+f.numerohabitacion+"<br>";
						if(f.numerorecamaras != ""  && f.numerorecamaras != null) entrada += "<b>Número de recamaras:</b>&nbsp;&nbsp"+f.numerorecamaras+"<br>";
						if(f.ocupada != ""  && f.ocupada != null) entrada += "<b>Número de recamaras:</b>&nbsp;&nbsp"+(f.ocupada?"Sí":"No")+"<br>";
						if(f.tarifa != ""  && f.tarifa != null) entrada += "<b>Tarífa:</b>&nbsp;&nbsp;<b>$</b>&nbsp;&nbsp;<span style='color:green'>"+Number(f.tarifa).toFixed(2)+"</span><br>";
						if(f.tipohabitacion != ""  && f.tipohabitacion != null) entrada += "<b>Tipo de Habitación:</b>&nbsp;&nbsp;"+ tipoHabitaciones.filter((a)=>{ return a.itemid == f.tipohabitacion; })[0].nombre +"<br>";
					
					}
					if(item.evento == "update"){
						entrada += "<b>Estado:</b>&nbsp;&nbsp;Editada<br>";
						if(f.usuarioedita != ""  && f.usuarioedita != null) entrada += "<b>Editó:</b>&nbsp;&nbsp"+f.usuarioedita+"<br>";
						if(f.fechaedita != "" && f.fechaedita != null) entrada += "<b>F. Editó:</b>&nbsp;&nbsp"+moment(Number(f.fechaedita) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.nombre != ""  && f.nombre != null) entrada += "<b>Nombre:</b>&nbsp;&nbsp"+f.nombre+"<br>";
						if(f.numerohabitacion != ""  && f.numerohabitacion != null) entrada += "<b>Número de habitación:</b>&nbsp;&nbsp"+f.numerohabitacion+"<br>";
						if(f.numerorecamaras != ""  && f.numerorecamaras != null) entrada += "<b>Número de recamaras:</b>&nbsp;&nbsp"+f.numerorecamaras+"<br>";
						if(f.ocupada != ""  && f.ocupada != null) entrada += "<b>Número de recamaras:</b>&nbsp;&nbsp"+(f.ocupada?"Sí":"No")+"<br>";
						if(f.tarifa != ""  && f.tarifa != null) entrada += "<b>Tarífa:</b>&nbsp;&nbsp;<b>$</b>&nbsp;&nbsp;<span style='color:green'>"+Number(f.tarifa).toFixed(2)+"</span><br>";
						if(f.tipohabitacion != ""  && f.tipohabitacion != null) entrada += "<b>Tipo de Habitación:</b>&nbsp;&nbsp;"+ tipoHabitaciones.filter((a)=>{ return a.itemid == f.tipohabitacion; })[0].nombre +"<br>";
					}
					if(item.evento == "delete"){
						
						entrada += "<b>Estado:</b>&nbsp;&nbsp;Eliminada<br>";
						if(f.usuarioelimina != ""  && f.usuarioelimina != null) entrada += "<b>Eliminó:</b>&nbsp;&nbsp"+f.usuarioelimina+"<br>";
						if(f.fechaelimina != "" && f.fechaelimina != null) entrada += "<b>F. Eliminó:</b>&nbsp;&nbsp"+moment(Number(f.fechaelimina) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.usuarioedita != ""  && f.usuarioedita != null) entrada += "<b>Editó:</b>&nbsp;&nbsp"+f.usuarioedita+"<br>";
						if(f.fechaedita != "" && f.fechaedita != null) entrada += "<b>F. Editó:</b>&nbsp;&nbsp"+moment(Number(f.fechaedita) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.usuariocrea != ""  && f.usuariocrea != null) entrada += "<b>Creó:</b>&nbsp;&nbsp"+f.usuariocrea+"<br>";
						if(f.fechacrea != "" && f.fechacrea != null) entrada += "<b>F. Creó:</b>&nbsp;&nbsp"+moment(Number(f.fechacrea) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.nombre != ""  && f.nombre != null) entrada += "<b>Nombre:</b>&nbsp;&nbsp"+f.nombre+"<br>";
						if(f.numerohabitacion != ""  && f.numerohabitacion != null) entrada += "<b>Número de habitación:</b>&nbsp;&nbsp"+f.numerohabitacion+"<br>";
						if(f.numerorecamaras != ""  && f.numerorecamaras != null) entrada += "<b>Número de recamaras:</b>&nbsp;&nbsp"+f.numerorecamaras+"<br>";
						if(f.ocupada != ""  && f.ocupada != null) entrada += "<b>Número de recamaras:</b>&nbsp;&nbsp"+(f.ocupada?"Sí":"No")+"<br>";
						if(f.tarifa != ""  && f.tarifa != null) entrada += "<b>Tarífa:</b>&nbsp;&nbsp;<b>$</b>&nbsp;&nbsp;<span style='color:green'>"+Number(f.tarifa).toFixed(2)+"</span><br>";
						if(f.tipohabitacion != ""  && f.tipohabitacion != null) entrada += "<b>Tipo de Habitación:</b>&nbsp;&nbsp;"+ tipoHabitaciones.filter((a)=>{ return a.itemid == f.tipohabitacion; })[0].nombre +"<br>";
					}
					entrada +="</div>";
					this.setCValue(entrada,val);
				break;
				case 'Hospedajes':
					if(item.evento == "insert"){						
						entrada += "<b>Estado:</b>&nbsp;&nbsp;Agregado<br>";
						if(f.habitaciones != ""  && f.habitaciones != null) entrada += "<b>Habitación:</b>&nbsp;&nbsp;"+ habitaciones.filter((a)=>{ return a.itemid == f.cliente; })[0].nombre +"<br>";
						if(f.usuariocrea != ""  && f.usuariocrea != null) entrada += "<b>Creó:</b>&nbsp;&nbsp"+f.usuariocrea+"<br>";
						if(f.fechacrea != "" && f.fechacrea != null) entrada += "<b>F. Creó:</b>&nbsp;&nbsp"+moment(Number(f.fechacrea) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.fechahospedaje != "" && f.fechahospedaje != null) entrada += "<b>F. Hospedaje:</b>&nbsp;&nbsp"+f.fechahospedaje+"<br>";
						if(f.diashospedaje != "" && f.diashospedaje != null) entrada += "<b>Días Hospedaje:</b>&nbsp;&nbsp"+f.dishospedaje+"<br>";
						if(f.grantotal != ""  && f.grantotal != null) entrada += "<b>Gran Total:</b>&nbsp;&nbsp;<b>$</b>&nbsp;&nbsp;<span style='color:green'>"+Number(f.grantotal).toFixed(2)+"</span><br>";
						if(f.costo != ""  && f.costo != null) entrada += "<b>Costo:</b>&nbsp;&nbsp;<b>$</b>&nbsp;&nbsp;<span style='color:green'>"+Number(f.costo).toFixed(2)+"</span><br>";
						if(f.iva != ""  && f.iva != null) entrada += "<b>Iva:</b>&nbsp;&nbsp;<b>$</b>&nbsp;&nbsp;<span style='color:green'>"+Number(f.iva).toFixed(2)+"</span><br>";
						if(f.impsobrehosp != ""  && f.impsobrehosp != null) entrada += "<b>Impuesto sobre hospedaje:</b>&nbsp;&nbsp;<b>$</b>&nbsp;&nbsp;<span style='color:green'>"+Number(f.impsobrehosp).toFixed(2)+"</span><br>";
						if(f.cantidadespersonas != ""  && f.cantidadespersonas != null) {
							let cant = JSON.parse(f.cantidadespersonas);
							let personas = JSON.parse(f.cantidadespersonas);
							entrada += "<b>Adultos Mayores?:</b>&nbsp;&nbsp;"+(personas.adultosmayores==1?"Sí":"No")+"<br>";
							entrada += "<b>Cantidad:</b>&nbsp;&nbsp"+(cant.adultosmayores==0?"<span style='color:red'>0</span>":"<span style='color:green'>"+cant.adultosmayores+"</span>")+"<br>";
							entrada += "<b>Adultos?:</b>&nbsp;&nbsp;"+(personas.adultos==1?"Sí":"No")+"<br>";
							entrada += "<b>Cantidad:</b>&nbsp;&nbsp"+(cant.adultos==0?"<span style='color:red'>0</span>":"<span style='color:green'>"+cant.adultos+"</span>")+"<br>";
							entrada += "<b>Niños?:</b>&nbsp;&nbsp;"+(personas.ninos==1?"Sí":"No")+"<br>";
							entrada += "<b>Cantidad:</b>&nbsp;&nbsp"+(cant.ninos==0?"<span style='color:red'>0</span>":"<span style='color:green'>"+cant.ninos+"</span>")+"<br>";
							entrada += "<b>Bebés?:</b>&nbsp;&nbsp;"+(personas.bebes==1?"Sí":"No")+"<br>";
							entrada += "<b>Cantidad:</b>&nbsp;&nbsp"+(cant.bebes==0?"<span style='color:red'>0</span>":"<span style='color:green'>"+cant.bebes+"</span>")+"<br>";
						}
						if(f.cliente != ""  && f.cliente != null) entrada += "<b>Cliente:</b>&nbsp;&nbsp;"+ clientes.filter((a)=>{ return a.itemid == f.cliente; })[0].nombre +"<br>";
						if(f.rfc != ""  && f.rfc != null) entrada += "<b>RFC:</b>&nbsp;&nbsp;"+ f.rfc +"<br>";
						if(f.comentarios != ""  && f.comentarios != null) entrada += "<b>Comentarios:</b>&nbsp;&nbsp;"+ f.comentarios +"<br>";
					}
					if(item.evento == "update"){
						entrada += "<b>Estado:</b>&nbsp;&nbsp;Editado<br>";
						if(f.habitaciones != ""  && f.habitaciones != null) entrada += "<b>Habitación:</b>&nbsp;&nbsp;"+ habitaciones.filter((a)=>{ return a.itemid == f.cliente; })[0].nombre +"<br>";
						if(f.usuarioedita != ""  && f.usuarioedita != null) entrada += "<b>Editó:</b>&nbsp;&nbsp"+f.usuarioedita+"<br>";
						if(f.fechaedita != "" && f.fechaedita != null) entrada += "<b>F. Editó:</b>&nbsp;&nbsp"+moment(Number(f.fechaedita) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.fechahospedaje != "" && f.fechahospedaje != null) entrada += "<b>F. Hospedaje:</b>&nbsp;&nbsp"+f.fechahospedaje+"<br>";
						if(f.diashospedaje != "" && f.diashospedaje != null) entrada += "<b>Días Hospedaje:</b>&nbsp;&nbsp"+f.dishospedaje+"<br>";
						if(f.grantotal != ""  && f.grantotal != null) entrada += "<b>Gran Total:</b>&nbsp;&nbsp;<b>$</b>&nbsp;&nbsp;<span style='color:green'>"+Number(f.grantotal).toFixed(2)+"</span><br>";
						if(f.costo != ""  && f.costo != null) entrada += "<b>Costo:</b>&nbsp;&nbsp;<b>$</b>&nbsp;&nbsp;<span style='color:green'>"+Number(f.costo).toFixed(2)+"</span><br>";
						if(f.iva != ""  && f.iva != null) entrada += "<b>Iva:</b>&nbsp;&nbsp;<b>$</b>&nbsp;&nbsp;<span style='color:green'>"+Number(f.iva).toFixed(2)+"</span><br>";
						if(f.impsobrehosp != ""  && f.impsobrehosp != null) entrada += "<b>Impuesto sobre hospedaje:</b>&nbsp;&nbsp;<b>$</b>&nbsp;&nbsp;<span style='color:green'>"+Number(f.impsobrehosp).toFixed(2)+"</span><br>";
						if(f.cantidadespersonas != ""  && f.cantidadespersonas != null) {
							let cant = JSON.parse(f.cantidadespersonas);
							let personas = JSON.parse(f.cantidadespersonas);
							entrada += "<b>Adultos Mayores?:</b>&nbsp;&nbsp;"+(personas.adultosmayores==1?"Sí":"No")+"<br>";
							entrada += "<b>Cantidad:</b>&nbsp;&nbsp"+(cant.adultosmayores==0?"<span style='color:red'>0</span>":"<span style='color:green'>"+cant.adultosmayores+"</span>")+"<br>";
							entrada += "<b>Adultos?:</b>&nbsp;&nbsp;"+(personas.adultos==1?"Sí":"No")+"<br>";
							entrada += "<b>Cantidad:</b>&nbsp;&nbsp"+(cant.adultos==0?"<span style='color:red'>0</span>":"<span style='color:green'>"+cant.adultos+"</span>")+"<br>";
							entrada += "<b>Niños?:</b>&nbsp;&nbsp;"+(personas.ninos==1?"Sí":"No")+"<br>";
							entrada += "<b>Cantidad:</b>&nbsp;&nbsp"+(cant.ninos==0?"<span style='color:red'>0</span>":"<span style='color:green'>"+cant.ninos+"</span>")+"<br>";
							entrada += "<b>Bebés?:</b>&nbsp;&nbsp;"+(personas.bebes==1?"Sí":"No")+"<br>";
							entrada += "<b>Cantidad:</b>&nbsp;&nbsp"+(cant.bebes==0?"<span style='color:red'>0</span>":"<span style='color:green'>"+cant.bebes+"</span>")+"<br>";
						}
						if(f.cliente != ""  && f.cliente != null) entrada += "<b>Cliente:</b>&nbsp;&nbsp;"+ clientes.filter((a)=>{ return a.itemid == f.cliente; })[0].nombre +"<br>";
						if(f.rfc != ""  && f.rfc != null) entrada += "<b>RFC:</b>&nbsp;&nbsp;"+ f.rfc +"<br>";
						if(f.comentarios != ""  && f.comentarios != null) entrada += "<b>Comentarios:</b>&nbsp;&nbsp;"+ f.comentarios +"<br>";
					}
					if(item.evento == "delete"){					
						entrada += "<b>Estado:</b>&nbsp;&nbsp;Eliminado<br>";
						if(f.habitaciones != ""  && f.habitaciones != null) entrada += "<b>Habitación:</b>&nbsp;&nbsp;"+ habitaciones.filter((a)=>{ return a.itemid == f.cliente; })[0].nombre +"<br>";
						if(f.usuarioelimina != ""  && f.usuarioelimina != null) entrada += "<b>Eliminó:</b>&nbsp;&nbsp"+f.usuarioelimina+"<br>";
						if(f.fechaelimina != "" && f.fechaelimina != null) entrada += "<b>F. Eliminó:</b>&nbsp;&nbsp"+moment(Number(f.fechaelimina) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.usuarioedita != ""  && f.usuarioedita != null) entrada += "<b>Editó:</b>&nbsp;&nbsp"+f.usuarioedita+"<br>";
						if(f.fechaedita != "" && f.fechaedita != null) entrada += "<b>F. Editó:</b>&nbsp;&nbsp"+moment(Number(f.fechaedita) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.usuariocrea != ""  && f.usuariocrea != null) entrada += "<b>Creó:</b>&nbsp;&nbsp"+f.usuariocrea+"<br>";
						if(f.fechacrea != "" && f.fechacrea != null) entrada += "<b>F. Creó:</b>&nbsp;&nbsp"+moment(Number(f.fechacrea) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.fechahospedaje != "" && f.fechahospedaje != null) entrada += "<b>F. Hospedaje:</b>&nbsp;&nbsp"+f.fechahospedaje+"<br>";
						if(f.diashospedaje != "" && f.diashospedaje != null) entrada += "<b>Días Hospedaje:</b>&nbsp;&nbsp"+f.dishospedaje+"<br>";
						if(f.grantotal != ""  && f.grantotal != null) entrada += "<b>Gran Total:</b>&nbsp;&nbsp;<b>$</b>&nbsp;&nbsp;<span style='color:green'>"+Number(f.grantotal).toFixed(2)+"</span><br>";
						if(f.costo != ""  && f.costo != null) entrada += "<b>Costo:</b>&nbsp;&nbsp;<b>$</b>&nbsp;&nbsp;<span style='color:green'>"+Number(f.costo).toFixed(2)+"</span><br>";
						if(f.iva != ""  && f.iva != null) entrada += "<b>Iva:</b>&nbsp;&nbsp;<b>$</b>&nbsp;&nbsp;<span style='color:green'>"+Number(f.iva).toFixed(2)+"</span><br>";
						if(f.impsobrehosp != ""  && f.impsobrehosp != null) entrada += "<b>Impuesto sobre hospedaje:</b>&nbsp;&nbsp;<b>$</b>&nbsp;&nbsp;<span style='color:green'>"+Number(f.impsobrehosp).toFixed(2)+"</span><br>";
						if(f.cantidadespersonas != ""  && f.cantidadespersonas != null) {
							let cant = JSON.parse(f.cantidadespersonas);
							let personas = JSON.parse(f.cantidadespersonas);
							entrada += "<b>Adultos Mayores?:</b>&nbsp;&nbsp;"+(personas.adultosmayores==1?"Sí":"No")+"<br>";
							entrada += "<b>Cantidad:</b>&nbsp;&nbsp"+(cant.adultosmayores==0?"<span style='color:red'>0</span>":"<span style='color:green'>"+cant.adultosmayores+"</span>")+"<br>";
							entrada += "<b>Adultos?:</b>&nbsp;&nbsp;"+(personas.adultos==1?"Sí":"No")+"<br>";
							entrada += "<b>Cantidad:</b>&nbsp;&nbsp"+(cant.adultos==0?"<span style='color:red'>0</span>":"<span style='color:green'>"+cant.adultos+"</span>")+"<br>";
							entrada += "<b>Niños?:</b>&nbsp;&nbsp;"+(personas.ninos==1?"Sí":"No")+"<br>";
							entrada += "<b>Cantidad:</b>&nbsp;&nbsp"+(cant.ninos==0?"<span style='color:red'>0</span>":"<span style='color:green'>"+cant.ninos+"</span>")+"<br>";
							entrada += "<b>Bebés?:</b>&nbsp;&nbsp;"+(personas.bebes==1?"Sí":"No")+"<br>";
							entrada += "<b>Cantidad:</b>&nbsp;&nbsp"+(cant.bebes==0?"<span style='color:red'>0</span>":"<span style='color:green'>"+cant.bebes+"</span>")+"<br>";
						}
						if(f.cliente != ""  && f.cliente != null) entrada += "<b>Cliente:</b>&nbsp;&nbsp;"+ clientes.filter((a)=>{ return a.itemid == f.cliente; })[0].nombre +"<br>";
						if(f.rfc != ""  && f.rfc != null) entrada += "<b>RFC:</b>&nbsp;&nbsp;"+ f.rfc +"<br>";
						if(f.comentarios != ""  && f.comentarios != null) entrada += "<b>Comentarios:</b>&nbsp;&nbsp;"+ f.comentarios +"<br>";
					}
					entrada +="</div>";
					this.setCValue(entrada,val);
				break;
				case 'usuarios':
					if(item.evento == "insert"){						
						entrada += "<b>Estado:</b>&nbsp;&nbsp;Agregado<br>";
						if(f.usuariocrea != ""  && f.usuariocrea != null) entrada += "<b>Creó:</b>&nbsp;&nbsp;"+f.usuariocrea+"<br>";
						if(f.fechacrea != "" && f.fechacrea != null) entrada += "<b>F. Creó:</b>&nbsp;&nbsp;"+moment(Number(f.fechacrea) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.nombreusuario != "" && f.nombreusuario != null) entrada += "<b>Nombre de usuario:</b>&nbsp;&nbsp;"+f.nombreusuario+"<br>";
						if(f.clave != "" && f.clave != null) entrada += "<b>Nombre de usuario:</b>&nbsp;&nbsp;"+f.clave+"<br>";
						if(f.empleado != ""  && f.empleado != null) entrada += "<b>Empleado:</b>&nbsp;&nbsp;"+ empleados.filter((a)=>{ return a.itemid == f.empleado; })[0].nombre +"<br>";
						else entrada += "<b>Usuario no enlazado a un empleado:</b><br>";
					}
					if(item.evento == "update"){						
						entrada += "<b>Estado:</b>&nbsp;&nbsp;Editado<br>";
						if(f.usuarioedita != ""  && f.usuarioedita != null) entrada += "<b>Editó:</b>&nbsp;&nbsp;"+f.usuarioedita+"<br>";
						if(f.fechaedita != "" && f.fechaedita != null) entrada += "<b>F. Editó:</b>&nbsp;&nbsp;"+moment(Number(f.fechaedita) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.nombreusuario != "" && f.nombreusuario != null) entrada += "<b>Nombre de usuario:</b>&nbsp;&nbsp;"+f.nombreusuario+"<br>";
						if(f.clave != "" && f.clave != null) entrada += "<b>Nombre de usuario:</b>&nbsp;&nbsp;"+f.clave+"<br>";
						if(f.empleado != ""  && f.empleado != null) entrada += "<b>Empleado:</b>&nbsp;&nbsp;"+ empleados.filter((a)=>{ return a.itemid == f.empleado; })[0].nombre +"<br>";
						else entrada += "<b>Usuario no enlazado a un empleado:</b><br>";
					}
					if(item.evento == "delete"){						
						entrada += "<b>Estado:</b>&nbsp;&nbsp;Eliminado<br>";
						if(f.usuarioelimina != ""  && f.usuarioelimina != null) entrada += "<b>Eliminó:</b>&nbsp;&nbsp;"+f.usuarioelimina+"<br>";
						if(f.fechaelimina != "" && f.fechaelimina != null) entrada += "<b>F. Eliminó:</b>&nbsp;&nbsp;"+moment(Number(f.fechaelimina) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.usuarioedita != ""  && f.usuarioedita != null) entrada += "<b>Editó:</b>&nbsp;&nbsp;"+f.usuarioedita+"<br>";
						if(f.fechaedita != "" && f.fechaedita != null) entrada += "<b>F. Editó:</b>&nbsp;&nbsp;"+moment(Number(f.fechaedita) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.usuariocrea != ""  && f.usuariocrea != null) entrada += "<b>Creó:</b>&nbsp;&nbsp;"+f.usuariocrea+"<br>";
						if(f.fechcrea != "" && f.fechcrea != null) entrada += "<b>F. Creó:</b>&nbsp;&nbsp;"+moment(Number(f.fechcrea) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.nombreusuario != "" && f.nombreusuario != null) entrada += "<b>Nombre de usuario:</b>&nbsp;&nbsp;"+f.nombreusuario+"<br>";
						if(f.clave != "" && f.clave != null) entrada += "<b>Nombre de usuario:</b>&nbsp;&nbsp;"+f.clave+"<br>";
						if(f.empleado != ""  && f.empleado != null) entrada += "<b>Empleado:</b>&nbsp;&nbsp;"+ empleados.filter((a)=>{ return a.itemid == f.empleado; })[0].nombre +"<br>";
						else entrada += "<b>Usuario no enlazado a un empleado:</b><br>";
					}
					entrada +="</div>";
					this.setCValue(entrada,val);
				break;
				case 'clientes':
					if(item.evento == "insert"){
						entrada += "<b>Estado:</b>&nbsp;&nbsp;Agregado<br>";
						if(f.usuariocrea != ""  && f.usuariocrea != null) entrada += "<b>Creó:</b>&nbsp;&nbsp;"+f.usuariocrea+"<br>";
						if(f.fechacrea != "" && f.fechacrea != null) entrada += "<b>F. Creó:</b>&nbsp;&nbsp;"+moment(Number(f.fechacrea) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.empresa != ""  && f.empresa != null) entrada += "<b>Empresa:</b>&nbsp;&nbsp;"+ empresas.filter((a)=>{ return a.itemid == f.empresa; })[0].nombre +"<br>";
						else entrada += "<b>Cliente (No vinculado a una Empresa):</b><br>";
						if(f.rfc != "" && f.rfc != null) entrada += "<b>RFC:</b>&nbsp;&nbsp;"+f.rfc+"<br>";
						if(f.nombre != "" && f.nombre != null) entrada += "<b>Nombre:</b>&nbsp;&nbsp;"+f.nombre+"<br>";
						if(f.edad != "" && f.edad != null) entrada += "<b>Edad:</b>&nbsp;&nbsp;"+f.edad+" años<br>";
						if(f.telefono != "" && f.telefono != null) entrada += "<b>Teléfono:</b>&nbsp;&nbsp;"+f.telefono+"<br>";
						if(f.correo != "" && f.correo != null) entrada += "<b>Correo:</b>&nbsp;&nbsp;"+f.correo+"<br>";
						if(f.direccion != "" && f.direccion != null) entrada += "<b>Dirección:</b>&nbsp;&nbsp;"+f.direccion+"<br>";
						if(f.estado != "" && f.estado != null) entrada += "<b>Estado:</b>&nbsp;&nbsp;"+f.estado+"<br>";
						if(f.ciudad != "" && f.ciudad != null) entrada += "<b>Ciudad:</b>&nbsp;&nbsp;"+f.ciudad+"<br>";
					}
					if(item.evento == "update"){
						entrada += "<b>Estado:</b>&nbsp;&nbsp;Editado<br>";
						if(f.usuarioedita != ""  && f.usuarioedita != null) entrada += "<b>Editó:</b>&nbsp;&nbsp;"+f.usuarioedita+"<br>";
						if(f.fechaedita != "" && f.fechaedita != null) entrada += "<b>F. Editó:</b>&nbsp;&nbsp;"+moment(Number(f.fechaedita) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.empresa != ""  && f.empresa != null) entrada += "<b>Empresa:</b>&nbsp;&nbsp;"+ empresas.filter((a)=>{ return a.itemid == f.empresa; })[0].nombre +"<br>";
						else entrada += "<b>Cliente (No vinculado a una Empresa):</b><br>";
						if(f.rfc != "" && f.rfc != null) entrada += "<b>RFC:</b>&nbsp;&nbsp;"+f.rfc+"<br>";
						if(f.nombre != "" && f.nombre != null) entrada += "<b>Nombre:</b>&nbsp;&nbsp;"+f.nombre+"<br>";
						if(f.edad != "" && f.edad != null) entrada += "<b>Edad:</b>&nbsp;&nbsp;"+f.edad+" años<br>";
						if(f.telefono != "" && f.telefono != null) entrada += "<b>Teléfono:</b>&nbsp;&nbsp;"+f.telefono+"<br>";
						if(f.correo != "" && f.correo != null) entrada += "<b>Correo:</b>&nbsp;&nbsp;"+f.correo+"<br>";
						if(f.direccion != "" && f.direccion != null) entrada += "<b>Dirección:</b>&nbsp;&nbsp;"+f.direccion+"<br>";
						if(f.estado != "" && f.estado != null) entrada += "<b>Estado:</b>&nbsp;&nbsp;"+f.estado+"<br>";
						if(f.ciudad != "" && f.ciudad != null) entrada += "<b>Ciudad:</b>&nbsp;&nbsp;"+f.ciudad+"<br>";
					}
					if(item.evento == "delete"){
						entrada += "<b>Estado:</b>&nbsp;&nbsp;Editado<br>";
						if(f.usuarioelimina != ""  && f.usuarioelimina != null) entrada += "<b>eliminó:</b>&nbsp;&nbsp;"+f.usuarioelimina+"<br>";
						if(f.fechaelimina != "" && f.fechaelimina != null) entrada += "<b>F. Editó:</b>&nbsp;&nbsp;"+moment(Number(f.fechaelimina) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.usuarioedita != ""  && f.usuarioedita != null) entrada += "<b>Editó:</b>&nbsp;&nbsp;"+f.usuarioedita+"<br>";
						if(f.fechaedita != "" && f.fechaedita != null) entrada += "<b>F. Editó:</b>&nbsp;&nbsp;"+moment(Number(f.fechaedita) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.usuariocrea != ""  && f.usuariocrea != null) entrada += "<b>Creó:</b>&nbsp;&nbsp;"+f.usuariocrea+"<br>";
						if(f.fechacrea != "" && f.fechacrea != null) entrada += "<b>F. Creó:</b>&nbsp;&nbsp;"+moment(Number(f.fechacrea) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.empresa != ""  && f.empresa != null) entrada += "<b>Empresa:</b>&nbsp;&nbsp;"+ empresas.filter((a)=>{ return a.itemid == f.empresa; })[0].nombre +"<br>";
						else entrada += "<b>Cliente (No vinculado a una Empresa):</b><br>";
						if(f.rfc != "" && f.rfc != null) entrada += "<b>RFC:</b>&nbsp;&nbsp;"+f.rfc+"<br>";
						if(f.nombre != "" && f.nombre != null) entrada += "<b>Nombre:</b>&nbsp;&nbsp;"+f.nombre+"<br>";
						if(f.edad != "" && f.edad != null) entrada += "<b>Edad:</b>&nbsp;&nbsp;"+f.edad+" años<br>";
						if(f.telefono != "" && f.telefono != null) entrada += "<b>Teléfono:</b>&nbsp;&nbsp;"+f.telefono+"<br>";
						if(f.correo != "" && f.correo != null) entrada += "<b>Correo:</b>&nbsp;&nbsp;"+f.correo+"<br>";
						if(f.direccion != "" && f.direccion != null) entrada += "<b>Dirección:</b>&nbsp;&nbsp;"+f.direccion+"<br>";
						if(f.estado != "" && f.estado != null) entrada += "<b>Estado:</b>&nbsp;&nbsp;"+f.estado+"<br>";
						if(f.ciudad != "" && f.ciudad != null) entrada += "<b>Ciudad:</b>&nbsp;&nbsp;"+f.ciudad+"<br>";
					}
					entrada +="</div>";
					this.setCValue(entrada,val);
				break;
				case 'empleados':
					if(item.evento == "insert"){
						
						entrada += "<b>Estado:</b>&nbsp;&nbsp;Agregado<br>";
						if(f.usuariocrea != ""  && f.usuariocrea != null) entrada += "<b>Creó:</b>&nbsp;&nbsp;"+f.usuariocrea+"<br>";
						if(f.fechacrea != "" && f.fechacrea != null) entrada += "<b>F. Creó:</b>&nbsp;&nbsp;"+moment(Number(f.fechacrea) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						var nombrecompleto = [];
					
						
						if(f.primer_nombre != "" && f.primer_nombre != null) nombrecompleto.push( f.primer_nombre );
						if(f.segundo_nombre != "" && f.segundo_nombre != null) nombrecompleto.push( f.segundo_nombre );
						if(f.apellido_paterno != "" && f.apellido_paterno != null) nombrecompleto.push( f.apellido_paterno );
						if(f.apellido_materno != "" && f.apellido_materno != null) nombrecompleto.push( f.apellido_materno );
						if(f.edad != "" && f.edad != null) entrada += "<b>Edad:</b>&nbsp;&nbsp;"+f.edad+"<br>";
						if(f.direccion != "" && f.direccion != null) entrada += "<b>Dirección:</b>&nbsp;&nbsp;"+f.direccion+"<br>";
						if(f.estadocivil != "" && f.estadocivil != null) entrada += "<b>Estado Civil:</b>&nbsp;&nbsp;"+f.estadocivil+"<br>";
						nombrecompleto = nombrecompleto.join(",");
						while(nombrecompleto.indexOf(",") != -1){
							nombrecompleto = nombrecompleto.replace(","," ");
						}
						if(nombrecompleto != "") entrada += "<b>Nombre:</b>&nbsp;&nbsp;"+nombrecompleto+"<br>";
						if(f.RFC != "" && f.RFC != null) entrada += "<b>RFC:</b>&nbsp;&nbsp;"+f.RFC+"<br>";
						if(f.cp != "" && f.cp != null) entrada += "<b>C.P.:</b>&nbsp;&nbsp;"+f.cp+"<br>";
						if(f.fecha_ingreso != "" && f.fecha_ingreso != null) entrada += "<b>Ingresó:</b>&nbsp;&nbsp;"+moment(Number(f.fecha_ingreso) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.puesto != "" && f.puesto != null) entrada += "<b>Puesto:</b>&nbsp;&nbsp;"+f.puesto+"<br>";
						if(f.telefono != "" && f.telefono != null) entrada += "<b>Teléfono:</b>&nbsp;&nbsp;"+f.telefono+"<br>";
					}
					if(item.evento == "update"){
						
						entrada += "<b>Estado:</b>&nbsp;&nbsp;Editado<br>";
						if(f.usuarioedita != ""  && f.usuarioedita != null) entrada += "<b>Editó:</b>&nbsp;&nbsp;"+f.usuarioedita+"<br>";
						if(f.fechaedita != "" && f.fechaedita != null) entrada += "<b>F. Editó:</b>&nbsp;&nbsp;"+moment(Number(f.fechaedita) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						var nombrecompleto = [];
					
						
						if(f.primer_nombre != "" && f.primer_nombre != null) nombrecompleto.push( f.primer_nombre );
						if(f.segundo_nombre != "" && f.segundo_nombre != null) nombrecompleto.push( f.segundo_nombre );
						if(f.apellido_paterno != "" && f.apellido_paterno != null) nombrecompleto.push( f.apellido_paterno );
						if(f.apellido_materno != "" && f.apellido_materno != null) nombrecompleto.push( f.apellido_materno );
						if(f.edad != "" && f.edad != null) entrada += "<b>Edad:</b>&nbsp;&nbsp;"+f.edad+"<br>";
						if(f.direccion != "" && f.direccion != null) entrada += "<b>Dirección:</b>&nbsp;&nbsp;"+f.direccion+"<br>";
						if(f.estadocivil != "" && f.estadocivil != null) entrada += "<b>Estado Civil:</b>&nbsp;&nbsp;"+f.estadocivil+"<br>";
						nombrecompleto = nombrecompleto.join(",");
						while(nombrecompleto.indexOf(",") != -1){
							nombrecompleto = nombrecompleto.replace(","," ");
						}
						if(nombrecompleto != "") entrada += "<b>Nombre:</b>&nbsp;&nbsp;"+nombrecompleto+"<br>";
						if(f.RFC != "" && f.RFC != null) entrada += "<b>RFC:</b>&nbsp;&nbsp;"+f.RFC+"<br>";
						if(f.cp != "" && f.cp != null) entrada += "<b>C.P.:</b>&nbsp;&nbsp;"+f.cp+"<br>";
						if(f.fecha_ingreso != "" && f.fecha_ingreso != null) entrada += "<b>Ingresó:</b>&nbsp;&nbsp;"+moment(Number(f.fecha_ingreso) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.puesto != "" && f.puesto != null) entrada += "<b>Puesto:</b>&nbsp;&nbsp;"+f.puesto+"<br>";
						if(f.telefono != "" && f.telefono != null) entrada += "<b>Teléfono:</b>&nbsp;&nbsp;"+f.telefono+"<br>";
					}
					if(item.evento == "delete"){
						
						entrada += "<b>Estado:</b>&nbsp;&nbsp;Eliminado<br>";
						if(f.usuarioelimina != ""  && f.usuarioelimina != null) entrada += "<b>Eliminó:</b>&nbsp;&nbsp;"+f.usuarioelimina+"<br>";
						if(f.fechaelimina != "" && f.fechaelimina != null) entrada += "<b>F. Eliminó:</b>&nbsp;&nbsp;"+moment(Number(f.fechaelimina) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.usuarioedita != ""  && f.usuarioedita != null) entrada += "<b>Editó:</b>&nbsp;&nbsp;"+f.usuarioedita+"<br>";
						if(f.fechaedita != "" && f.fechaedita != null) entrada += "<b>F. Editó:</b>&nbsp;&nbsp;"+moment(Number(f.fechaedita) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.usuariocrea != ""  && f.usuariocrea != null) entrada += "<b>Creó:</b>&nbsp;&nbsp;"+f.usuariocrea+"<br>";
						if(f.fechacrea != "" && f.fechacrea != null) entrada += "<b>F. Creó:</b>&nbsp;&nbsp;"+moment(Number(f.fechacrea) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						var nombrecompleto = [];
					
						
						if(f.primer_nombre != "" && f.primer_nombre != null) nombrecompleto.push( f.primer_nombre );
						if(f.segundo_nombre != "" && f.segundo_nombre != null) nombrecompleto.push( f.segundo_nombre );
						if(f.apellido_paterno != "" && f.apellido_paterno != null) nombrecompleto.push( f.apellido_paterno );
						if(f.apellido_materno != "" && f.apellido_materno != null) nombrecompleto.push( f.apellido_materno );
						if(f.edad != "" && f.edad != null) entrada += "<b>Edad:</b>&nbsp;&nbsp;"+f.edad+"<br>";
						if(f.direccion != "" && f.direccion != null) entrada += "<b>Dirección:</b>&nbsp;&nbsp;"+f.direccion+"<br>";
						if(f.estadocivil != "" && f.estadocivil != null) entrada += "<b>Estado Civil:</b>&nbsp;&nbsp;"+f.estadocivil+"<br>";
						nombrecompleto = nombrecompleto.join(",");
						while(nombrecompleto.indexOf(",") != -1){
							nombrecompleto = nombrecompleto.replace(","," ");
						}
						if(nombrecompleto != "") entrada += "<b>Nombre:</b>&nbsp;&nbsp;"+nombrecompleto+"<br>";
						if(f.RFC != "" && f.RFC != null) entrada += "<b>RFC:</b>&nbsp;&nbsp;"+f.RFC+"<br>";
						if(f.cp != "" && f.cp != null) entrada += "<b>C.P.:</b>&nbsp;&nbsp;"+f.cp+"<br>";
						if(f.fecha_ingreso != "" && f.fecha_ingreso != null) entrada += "<b>Ingresó:</b>&nbsp;&nbsp;"+moment(Number(f.fecha_ingreso) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.puesto != "" && f.puesto != null) entrada += "<b>Puesto:</b>&nbsp;&nbsp;"+f.puesto+"<br>";
						if(f.telefono != "" && f.telefono != null) entrada += "<b>Teléfono:</b>&nbsp;&nbsp;"+f.telefono+"<br>";
					}
					entrada +="</div>";
					this.setCValue(entrada,val);
				break;
				case 'empresas':
					if(item.evento == "insert"){
						if(f.usuariocrea != ""  && f.usuariocrea != null) entrada += "<b>Creó:</b>&nbsp;&nbsp"+f.usuariocrea+"<br>";
						if(f.fechacrea != "" && f.fechacrea != null) entrada += "<b>F. Creó:</b>&nbsp;&nbsp"+moment(Number(f.fechacrea) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.rfc != "" && f.rfc != null) entrada += "<b>RFC:</b>&nbsp;&nbsp;"+f.rfc+"<br>";
						if(f.nombre != "" && f.nombre != null) entrada += "<b>Nombre:</b>&nbsp;&nbsp;"+f.nombre+"<br>";
						if(f.direccion != "" && f.direccion != null) entrada += "<b>Dirección:</b>&nbsp;&nbsp;"+f.direccion+"<br>";
						if(f.colonia != "" && f.colonia != null) entrada += "<b>Dirección:</b>&nbsp;&nbsp;"+f.colonia+"<br>";
						if(f.cp != "" && f.cp != null) entrada += "<b>Dirección:</b>&nbsp;&nbsp;"+f.cp+"<br>";
						if(f.puesto != "" && f.puesto != null) entrada += "<b>Puesto:</b>&nbsp;&nbsp;"+f.puesto+"<br>";
						if(f.ciudad != "" && f.ciudad != null) entrada += "<b>Ciudad:</b>&nbsp;&nbsp;"+f.ciudad+"<br>";
					}
					if(item.evento == "update"){
						if(f.usuarioedita != ""  && f.usuarioedita != null) entrada += "<b>Editó:</b>&nbsp;&nbsp"+f.usuarioedita+"<br>";
						if(f.fechedita != "" && f.fechedita != null) entrada += "<b>F. Editó:</b>&nbsp;&nbsp"+moment(Number(f.fechedita) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.rfc != "" && f.rfc != null) entrada += "<b>RFC:</b>&nbsp;&nbsp;"+f.rfc+"<br>";
						if(f.nombre != "" && f.nombre != null) entrada += "<b>Nombre:</b>&nbsp;&nbsp;"+f.nombre+"<br>";
						if(f.direccion != "" && f.direccion != null) entrada += "<b>Dirección:</b>&nbsp;&nbsp;"+f.direccion+"<br>";
						if(f.colonia != "" && f.colonia != null) entrada += "<b>Dirección:</b>&nbsp;&nbsp;"+f.colonia+"<br>";
						if(f.cp != "" && f.cp != null) entrada += "<b>Dirección:</b>&nbsp;&nbsp;"+f.cp+"<br>";
						if(f.puesto != "" && f.puesto != null) entrada += "<b>Puesto:</b>&nbsp;&nbsp;"+f.puesto+"<br>";
						if(f.ciudad != "" && f.ciudad != null) entrada += "<b>Ciudad:</b>&nbsp;&nbsp;"+f.ciudad+"<br>";
					}
					if(item.evento == "delete"){
						if(f.usuarioelimina != ""  && f.usuarioelimina != null) entrada += "<b>Eliminó:</b>&nbsp;&nbsp"+f.usuarioelimina+"<br>";
						if(f.fechaelimina != "" && f.fechaelimina != null) entrada += "<b>F. Eliminó:</b>&nbsp;&nbsp"+moment(Number(f.fechaelimina) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.usuarioedita != ""  && f.usuarioedita != null) entrada += "<b>Eliminó:</b>&nbsp;&nbsp"+f.usuarioedita+"<br>";
						if(f.fechedita != "" && f.fechedita != null) entrada += "<b>F. Eliminó:</b>&nbsp;&nbsp"+moment(Number(f.fechedita) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.usuariocrea != ""  && f.usuariocrea != null) entrada += "<b>Creó:</b>&nbsp;&nbsp"+f.usuariocrea+"<br>";
						if(f.fechedita != "" && f.fechedita != null) entrada += "<b>F. Creo:</b>&nbsp;&nbsp"+moment(Number(f.fechedita) * 1000).format("YYYY-MM-DD HH:mm:ss")+"<br>";
						if(f.rfc != "" && f.rfc != null) entrada += "<b>RFC:</b>&nbsp;&nbsp;"+f.rfc+"<br>";
						if(f.nombre != "" && f.nombre != null) entrada += "<b>Nombre:</b>&nbsp;&nbsp;"+f.nombre+"<br>";
						if(f.direccion != "" && f.direccion != null) entrada += "<b>Dirección:</b>&nbsp;&nbsp;"+f.direccion+"<br>";
						if(f.colonia != "" && f.colonia != null) entrada += "<b>Dirección:</b>&nbsp;&nbsp;"+f.colonia+"<br>";
						if(f.cp != "" && f.cp != null) entrada += "<b>Dirección:</b>&nbsp;&nbsp;"+f.cp+"<br>";
						if(f.puesto != "" && f.puesto != null) entrada += "<b>Puesto:</b>&nbsp;&nbsp;"+f.puesto+"<br>";
					}
					entrada +="</div>";
					this.setCValue(entrada,val);
				break;
				default:
					// console.log(val)
					this.setCValue("Datos",val);
				break;
			}
        }
    }
    eXcell_objeto.prototype = new eXcell;// nests all other methods from the base class
    function eXcell_fecha(cell){ //the eXcell name is defined here
        if (cell){                // the default pattern, just copy it
            this.cell = cell;
            this.grid = this.cell.parentNode.grid;
            // eXcell_ed.call(this);
        }
        this.edit = function(){}  //read-only cell doesn't have edit method
        // the cell is read-only, so it's always in the disabled state
        this.isDisabled = function(){ return true; }
        this.setValue=function(val){
			this.cell.style.fontWeight="bolder";
            this.setCValue(moment(Number(val) * 1000).format("YYYY-MM-DD HH:mm:ss"),val);
        }
    }
    eXcell_fecha.prototype = new eXcell;// nests all other methods from the base class
    function eXcell_accion(cell){ //the eXcell name is defined here
        if (cell){                // the default pattern, just copy it
            this.cell = cell;
            this.grid = this.cell.parentNode.grid;
            // eXcell_ed.call(this);
        }
        this.edit = function(){}  //read-only cell doesn't have edit method
        // the cell is read-only, so it's always in the disabled state
		this.isDisabled = function(){ return true; }
		this.getValue=function(){
			// console.log(this.cell);
			return this.cell.innerText; // get button label
		}
        this.setValue=function(val){
			this.cell.style.color = "green";
			this.cell.style.fontWeight="bolder";
            this.setCValue(val,val);
        }
    }
    eXcell_accion.prototype = new eXcell;// nests all other methods from the base class
    function eXcell_modulo(cell){ //the eXcell name is defined here
        if (cell){                // the default pattern, just copy it
            this.cell = cell;
            this.grid = this.cell.parentNode.grid;
            // eXcell_ed.call(this);
        }
		this.getValue=function(){
			// console.log(this.cell);
			return this.cell.innerText; // get button label
		}
        this.edit = function(){}  //read-only cell doesn't have edit method
        // the cell is read-only, so it's always in the disabled state
        this.isDisabled = function(){ return true; }
        this.setValue=function(val){
			this.cell.style.fontWeight="bolder";
            this.setCValue(val,val);
        }
    }
	eXcell_modulo.prototype = new eXcell;// nests all other methods from the base class
	function eXcell_evento(cell){ //the eXcell name is defined here
        if (cell){                // the default pattern, just copy it
            this.cell = cell;
            this.grid = this.cell.parentNode.grid;
            // eXcell_ed.call(this);
        }
		this.getValue=function(){
			// console.log(this.cell);
			return this.cell.innerText; // get button label
		}
        this.edit = function(){}  //read-only cell doesn't have edit method
        // the cell is read-only, so it's always in the disabled state
        this.isDisabled = function(){ return true; }
        this.setValue=function(val){
			this.cell.style.fontWeight="bolder";
            this.setCValue(val,val);
        }
    }
	eXcell_evento.prototype = new eXcell;// nests all other methods from the base class
	function eXcell_usuariocrea(cell){ //the eXcell name is defined here
        if (cell){                // the default pattern, just copy it
            this.cell = cell;
            this.grid = this.cell.parentNode.grid;
            // eXcell_ed.call(this);
        }
		this.getValue=function(){
			// console.log(this.cell);
			return this.cell.innerText; // get button label
		}
        this.edit = function(){}  //read-only cell doesn't have edit method
        // the cell is read-only, so it's always in the disabled state
        this.isDisabled = function(){ return true; }
        this.setValue=function(val){
			this.cell.style.fontWeight="bolder";
            this.setCValue(val,val);
        }
    }
	eXcell_usuariocrea.prototype = new eXcell;// nests all other methods from the base class
	function mostrarImagen(archivo,tipo){
		// console.log("se abrirá la imagen",archivo,tipo);
		var width = Number(window.innerWidth / 2);
		var ventanaIntercambio = dhxWindow.createWindow({
			id:"abrirImage",
			// left:Number(window.innerWidth * 0.1),
			text:"Imagen",
			// top:0,
			width:width,
			height:600,
			center:true,
			modal:true
		});
		dhxWindow.window("abrirImage").button("minmax").attachEvent("onClick", function(win, button){

			var elemento = document.getElementsByClassName("dhxwin_active");
			for(var i = 0; i < elemento.length; i++){
				var elemento2 = document.getElementsByClassName("pantalla_completa");
				if(elemento2.length > 0){
					elemento[i].className = "dhxwin_active";
					dhxWindow.window("abrirImage").setPosition(Number(window.innerWidth * 0.1), 0);
				}else{
					dhxWindow.window("abrirImage").setPosition(0, 0);
					elemento[i].className += " pantalla_completa";
				}
			}
			return true;
		});
		dhxWindow.window("abrirImage").attachHTMLString("<div style='width:calc(100%)!important;height:100%!important'><iframe allowfullscreen src='"+node_chapel_web +"mostrarArchivo?tipo="+tipo+"&archivo="+archivo+"' width='100%' height='100%'"+"></iframe></div>");
		ventanaIntercambio.button("close").attachEvent("onClick", function(){
			// Refresh();
			return true; // deny default action
		});
	}
	function eXcell_fotito(cell){ //the eXcell name is defined here
        if (cell){                // the default pattern, just copy it
            this.cell = cell;
            this.grid = this.cell.parentNode.grid;
        }
        this.edit = function(){}  //read-only cell doesn't have edit method
        // the cell is read-only, so it's always in the disabled state
        this.isDisabled = function(){ return true; }
        this.setValue=function(val){
            let icono,row_id;
            row_id = this.cell.parentNode.idd;
			item = datastore.item(row_id);
			// console.log(item.fotos);
			if(item.fotos && item.fotos != ""){
				let fotos = JSON.parse(item.fotos);
				let todaslasfotos = "";
				for (let key in fotos) {
					// console.log(key);
					if (fotos.hasOwnProperty(key)) {
						let element = fotos[key];	
						// console.log(element);
						for (const llave in element) {
							if (element.hasOwnProperty(llave)) {
								let element2 = element[llave];								
								// console.log(element2,llave);					
								let foto = `<i style="font-weigth:bolder;font-size:1.5em;display:inline-block;padding-right:1px" onclick="mostrarImagen('`+element2+`','Imagen')" class="fa fa-picture-o" aria-hidden="true"></i>`;
								todaslasfotos += foto;
							}
						}
					}
				}
				//tipo=Imagen,archivo=foto
				this.cell.style.color = "green";
				this.setCValue("<div style='width:100%;'>" + todaslasfotos + "</div>",fotos.length);
			}
        }
    }
	eXcell_fotito.prototype = new eXcell;// nests all other methods from the base class
	function rfcValido(rfc, aceptarGenerico = true) {
		const re       = /^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$/;
		var   validado = rfc.match(re);

		if (!validado)  //Coincide con el formato general del regex?
			return false;

		//Separar el dígito verificador del resto del RFC
		const digitoVerificador = validado.pop(),
			rfcSinDigito      = validado.slice(1).join(''),
			len               = rfcSinDigito.length,

		//Obtener el digito esperado
			diccionario       = "0123456789ABCDEFGHIJKLMN&OPQRSTUVWXYZ Ñ",
			indice            = len + 1;
		var   suma,
			digitoEsperado;

		if (len == 12) suma = 0
		else suma = 481; //Ajuste para persona moral

		for(var i=0; i<len; i++)
			suma += diccionario.indexOf(rfcSinDigito.charAt(i)) * (indice - i);
		digitoEsperado = 11 - suma % 11;
		if (digitoEsperado == 11) digitoEsperado = 0;
		else if (digitoEsperado == 10) digitoEsperado = "A";

		//El dígito verificador coincide con el esperado?
		// o es un RFC Genérico (ventas a público general)?
		if ((digitoVerificador != digitoEsperado)
		&& (!aceptarGenerico || rfcSinDigito + digitoVerificador != "XAXX010101000"))
			return false;
		else if (!aceptarGenerico && rfcSinDigito + digitoVerificador == "XEXX010101000")
			return false;
		return rfcSinDigito + digitoVerificador;
	}
	function abrirExterior(esto){
		window.open(esto.src);
	}
</script>
</html>