<?php require_once 'auth.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Dashboard</title>
	<script type="text/javascript" src="../js/moment.js"></script>
	<link rel="stylesheet" href="../codebase/fonts/font_roboto/roboto.css">
	<link rel="stylesheet" href="../codebase/dhtmlx.css">
	<link rel="stylesheet" href="../fonts/css/font-awesome.css">
	<script type="text/javascript" src="../codebase/dhtmlx.js"></script>
	<script type="text/javascript" src="../js/jquery.min.js"></script>
	<script type="text/javascript" src="../js/rutas_node.js"></script>
	<link rel="stylesheet" href="../css/styles_general.css">
	<link rel="stylesheet" href="../icomoon/demo-files/demo.css">
	<link rel="stylesheet" href="../icomoon/style.css">
	<!-- <script src="https://www.google.com/maps/embed/v1/MODE?key=AIzaSyB5UEY8q-8Mhq1ZsGwcr9aCAUO5ixoHuFE&places"></script> -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">
	<style>
	.dhx_toolbar_material div.dhx_toolbar_btn input.dhxtoolbar_input {
		margin-top:6px;
	}
	.dhx_canvas_text.dhx_axis_item_x{
		vertical-align:middle;
		text-align:left
		/* writing-mode: vertical-lr;
    	transform: rotate(180deg); */
	}
	.elements_in_yard{
		width: 100%;
		height: 100%;
		font-size: 10em;
		text-align: center;
		font-weight: bolder;
		vertical-align: middle;
		/* margin-top: 12%; */
		/* margin-top: 3rem; */
		padding: 0;
		color:black!important;
		margin-top:25%
		/* line-height: 150%; */
	}
	.center_title{
		width:100%;
		height:100%;
		text-align:center;
		padding:30%;
		vertical-align:middle;
	}
	</style>
</head>
<body onload="Cargar()"></body>
<script type="text/javascript">
	var fechaPrimerDia = moment(moment().subtract(30, 'days').format("YYYY-MM-DD")).format('YYYY-MM-DD');
	var fechaUltimoDia = moment().format("YYYY-MM-DD");
	var layoutA,layoutB,layoutC,layout,chartCajas,chartTractores, grid, datastore,desde,hasta,dhxComboClienteDir,dhxComboInOut,dhxComboCleanDirty,dhxComboMotorCarrier,dhxComboTrailerOwner,dhxComboTrailerType,dhxComboTrailerIs,dhxComboConsign,toolbar,ruta_get_es,dhxWindow,dhxComboTractorwc,index2,datoses,datos_raiz;
	// ruta_get_es = node_chapel_web + "entradas_salidas?instruccion=get&cliente="+client_id+"&fechadesde="+fechaPrimerDia+"&fechahasta="+fechaUltimoDia;
	// LoadMenu();
	function Cargar()
	{
		layout = new dhtmlXLayoutObject({
			parent: document.body,
			pattern: "2E",
			offsets: {
				top: 0,
				right: 0,
				bottom: 0,
				left: 0
			},
		});
		layoutA = layout.cells("a").attachLayout({
			pattern: "2U",
			offsets: {
				top: 0,
				right: 0,
				bottom: 0,
				left: 0
			},
		});
		layoutA.cells("a").attachURL("ventas.php");
		var w = layoutA.cells("b").getWidth();
		var h = layoutA.cells("b").getHeight()-40;
		layoutA.cells("b").attachHTMLString(`<iframe width="`+w+`px" height="`+h+`px" frameborder="0" style="border:0"
src="https://www.google.com/maps/embed/v1/place?q=Gral.%20Victoriano%20Cepeda%20Sur%20410%2C%20Zona%20Centro%2C%2025000%20Saltillo%2C%20Coah.&key=AIzaSyB5UEY8q-8Mhq1ZsGwcr9aCAUO5ixoHuFE" allowfullscreen></iframe>`);
		layoutA.cells("a").setText("Ventas por año");
		layoutA.cells("b").setText("Ubicación");
		layoutB = layout.cells("b").attachLayout({
			pattern: "2U",
			offsets: {
				top: 0,
				right: 0,
				bottom: 0,
				left: 0
			},
		});
		layoutB.cells("a").attachURL("empresas-ly.php");
		layoutB.cells("b").attachURL("clientes-ly.php");
		layoutB.cells("a").setText("Empresas");
		layoutB.cells("b").setText("Clientes");
		/* dhxWindow = new dhtmlXWindows("material");
		datastore= new dhtmlXDataStore({
		    url:ruta_get_es,
		    datatype:"json"
		});
		layoutA = new dhtmlXLayoutObject({
			parent: document.body,
			pattern: "2E"
		});
		layoutA.cells("a").setText("Dashboard");
		layoutA.cells("a").fixSize(true, true);
		layoutA.cells("b").fixSize(true, true);
		layoutB = new dhtmlXLayoutObject({
			parent: layoutA.cells("a"),
			pattern: "2U"
		});
		layoutB.cells("a").showHeader();
		layoutB.cells("b").hideHeader();
		layoutB.cells("a").setText(`<div style='width:100%;text-align:center;font-weight:bolder'><span class="icomoon icon-trailer" style="color:black;font-weight:bolder;font-size:2.4em" aria-hidden='true'></span></div>`);
		layoutB.cells("a").setWidth(300);
		layoutB.cells("a").fixSize(true, true);
		layoutB.cells("b").fixSize(true, true);
		layoutC = new dhtmlXLayoutObject({
			parent: layoutA.cells("b"),
			pattern: "2U"
		});
		layoutC.cells("a").showHeader();
		layoutC.cells("b").hideHeader();
		layoutC.cells("a").setText(`<div style='width:100%;text-align:center;font-weight:bolder'><span class="icomoon icon-ttruck" style="color:black;font-weight:bolder;font-size:3em" aria-hidden='true'></span></div>`);
		layoutC.cells("a").setWidth(300);
		layoutC.cells("a").fixSize(true, true);
		layoutC.cells("b").fixSize(true, true);
		// layoutA.cells("a").showHeader();
		var values = {};
		values.cliente = client_id;
		values.tipoDocumento = 2;
		values.instruccion = "get_elements_en_patio";
		$.post(node_chapel_web + "dashboard",values,function(trailercant,textStatus,xhr){
			layoutB.cells("a").attachHTMLString(`<div class="elements_in_yard">`+trailercant+`</div>`);
		});
		values = {};
		values.cliente = client_id;
		values.tipoDocumento = 2;
		values.instruccion = "get_chart";
		$.post(node_chapel_web + "dashboard",values,function(cajasporcliente,textStatus,xhr){			
			chartCajas = layoutB.cells("b").attachChart({
				view:"barH",
				value:"#cant#",
				label:"#cant#",
				color:"#016836",
				barWidth:15,
				radius:2,
				tooltip:{
					template:"#cliente_name#"
				},
				yAxis:{
					template:"#cliente#",
					title:"Clientes"
				},
				xAxis:{
					start:1,
					end:cajasporcliente.lenght,
					step:1,
					template:"{obj}",
					title:"Numero de cajas"
				},
				padding:{
					left: 300
				}
				// value:"#cant#",
				// color:"#016836",
				// width:20,
				// height:100,
				// tooltip: "#cliente_name#",
				// xAxis:{
				// 	title:"Clientes",
				// 	template:"#cliente#"
				// },
				// yAxis:{
				// 	start:1,
				// 	end:cajasporcliente.lenght,
				// 	step:1,
				// 	template:"{obj}",
				// 	title:"Number of trailers"
				// }
			});
			chartCajas.parse(cajasporcliente,"json");
		});
		values = {};
		values.cliente = client_id;
		values.tipoDocumento = 3;
		values.instruccion = "get_elements_en_patio";
		$.post(node_chapel_web + "dashboard",values,function(tractorcant,textStatus,xhr){
			layoutC.cells("a").attachHTMLString(`<div class="elements_in_yard">`+tractorcant+`</div>`);
		});
		values = {};
		values.cliente = client_id;
		values.tipoDocumento = 3;
		values.instruccion = "get_chart";
		$.post(node_chapel_web + "dashboard",values,function(tractoresporcliente,textStatus,xhr){			
			chartTractores = layoutC.cells("b").attachChart({
				view:"barH",
				value:"#cant#",
				label:"#cant#",
				color:"#016836",
				barWidth:15,
				radius:2,
				tooltip:{
					template:"#cliente_name#"
				},
				yAxis:{
					template:"#cliente#",
					title:"Clientes"
				},
				xAxis:{
					start:1,
					end:tractoresporcliente.lenght,
					step:1,
					template:"{obj}",
					title:"Numero de tractores"
				},
				padding:{
					left: 300
				}
				// view:"bar",
				// value:"#cant#",
				// color:"#016836",
				// width:20,
				// tooltip: "#cliente_name#",
				// xAxis:{
				// 	title:"Clientes",
				// 	template:"#cliente#"
				// },
				// yAxis:{
				// 	start:1,
				// 	end:tractoresporcliente.lenght,
				// 	step:1,
				// 	template:"{obj}",
				// 	title:"Number of trucks"
				// }
			});
			chartTractores.parse(tractoresporcliente,"json");
		}); */
	}
</script>
</html>