<?php 
require_once 'auth.php'; 
if($_GET){
	if(isset($_GET["idCliente"]) && !empty($_GET["idCliente"])){
		echo "<script>";
		echo "var idCliente = '".$_GET["idCliente"] ."';";
		if(isset($_GET["client_name"]) && !empty($_GET["client_name"])){
			echo "var client_name = '".$_GET["client_name"] ."';";
		}
		echo "</script>".PHP_EOL;
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Clientes</title>
	<link rel="stylesheet" href="../codebase/dhtmlx.css">
	<script type="text/javascript" src="../js/moment.js"></script>
	<script src="../codebase/dhtmlx.js"></script>
    <link href="../css/fontawesome.css" rel="stylesheet">
    <link href="../css/brands.css" rel="stylesheet">
    <link href="../css/solid.css" rel="stylesheet">
	<script src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="../js/rutas_node.js"></script>
    <!-- <script src="../js/functions.js"></script> -->
	<link rel="stylesheet" href="../css/others_styles.css">
	<style>
html, body {
    width: 100%;
    height: 100%;
    margin: 0;
    padding: 0;
}
div.gridbox_material.gridbox .xhdr{
    margin:0!important;
}
.hdr{
    margin-right:0!important;
    padding-right:0!important;
    width:100%!important
}
/* width */
::-webkit-scrollbar {
        width: 10px;
    }
    /* Track */
    ::-webkit-scrollbar-track {
        background: #fefefe; 
    }
    /* Handle */
    ::-webkit-scrollbar-thumb {
        background: rgb(86, 66, 201)!important; 
    }
    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
        background: gray; 
    }
	</style>
</head>
<body onload="Cargar()"></body>

<script type="text/javascript">
	var layout,menu,empresas,nombre_catalogo, grid,json_form, datastore,item,toolbar,url_get_catalogo_elemento,dhxWindow,selectedId,desde,hasta,elementoseleccionado;
	// nombre_catalogo = catalogo;
	// json_form = "agregar_catalogo.json";
    // titulo_elemento = titulo;
    var fechaPrimerDia = moment(moment().subtract(2, 'days').format("YYYY-MM-DD")).format('YYYY-MM-DD');
	var fechaUltimoDia = moment().format("YYYY-MM-DD");
	url_get_catalogo_elemento = "../../node_hotel/empresas_hotel?instruccion=leer";

	LoadMenu();
	function Cargar()
	{
        // $.get("../../node_hotel/empresas_hotel?instruccion=leer",function(empresasactuales){
            // var neweact = [];
            // empresasactuales.forEach(element => {
            //     neweact[element.itemid] = element.nombre;
            // });
            // empresas = neweact;
            dhxWindow = new dhtmlXWindows("material");
            datastore= new dhtmlXDataStore({
                url:url_get_catalogo_elemento,
                datatype:"json"
            });
            layout = new dhtmlXLayoutObject({
                parent: document.body,
                pattern: "1C",
				offsets: {
					top: 0,
					right: 0,
					bottom: 0,
					left: 0
				},
            });
			layout.cells("a").hideHeader();
    
            // toolbar.addSeparator("sep1",8);
            // toolbar.addInput("desde", 9, fechaPrimerDia, 80);		
            // toolbar.addInput("hasta", 10,fechaUltimoDia, 80);
            // toolbar.addButton("filtrar", 11, "<i class='fa fa-filter' style='color:#0000ff;font-weight:bolder' aria-hidden='true'></i>", null, null);
            // var desdeInputt = toolbar.getInput("desde");
            // desde = new dhtmlXCalendarObject([desdeInputt]);
            // desde.setDate(fechaPrimerDia);
            // desde.hideTime();
            // desde.showToday();
            // var hastaInputt = toolbar.getInput("hasta");
            // hasta = new dhtmlXCalendarObject([hastaInputt]);
            // hasta.setDate(fechaUltimoDia);
            // hasta.hideTime();
            // hasta.showToday();
           
    
            grid=layout.cells("a").attachGrid();
			grid.setHeader("Nombre",null,
			[
				"text-align:center"
			]);
            // grid.attachHeader("#text_filter,,#combo_filter,#combo_filter,#text_filter,#text_filter,#text_filter,#combo_filter,#combo_filter,#text_filter,#text_filter,");
            grid.setInitWidths("*");
            grid.setColAlign("center");
            grid.setColumnIds("nombre");
            grid.setColSorting("str");
            grid.setColTypes("ro");
            // grid.setColumnHidden(1, true);
            grid.enableContextMenu(menu);
            grid.setImagePath("../codebase/imgs/");
            grid.setEditable(false);
            grid.enableMultiline(true);
            grid.enableKeyboardSupport(true);
            grid.enableBlockSelection(true);
            grid.attachEvent("onKeyPress",onKeyPressed); 
            grid.init();
            grid.sync(datastore);
            grid.attachEvent("onBeforeSelect", onBeforeSelect);
            // grid.attachEvent("onEditCell", onEditCell);
        // });
	}
	function LoadMenu()
	{
		menu=new dhtmlXMenuObject();
		menu.renderAsContextMenu();
		menu.attachEvent("onClick", onButtonClick);
		menu.loadStruct("../data/xml/Registro_ContextMenuReportes.xml");
	}
	function onButtonClick(menuitemId, type) {
		if (menuitemId == "excel") {
			grid.toExcel('../codebase/grid-excel-php/generate.php');
		}

		if (menuitemId == "pdf") {
			grid.toPDF('../codebase/grid-pdf-php/generate.php');
		}
		if (menuitemId == "refresh") {
			Refresh();
		}
    }
    
    function onBeforeSelect(new_row,old_row,new_col_index){
        console.log(new_row,old_row,new_col_index)
        elementoseleccionado = datastore.item(new_row);
        console.log(elementoseleccionado,"elementoseleccionado")
        return true;
    }
	function onEditCell(stage,rId,cInd,nValue,oValue){
        console.log(stage,rId,cInd,nValue,oValue)
		//stage Estado del Editor
		//rId Id del Row
		//cInd Indice de la celda
		//nValue Valor nuevo solo si el stage es 2
		//oValue Valor viejo solo si el stage es 2	
        item = datastore.item(rId);
        console.log(item);
        if(stage == 0){//El editor esta abierto
            // EditarCellElemento(rId);
			if(cInd == 4) return true;
		}
        if(stage == 1){//El editor esta abierto
            if(cInd == 4){
                EditarCellElementoActivo(rId);
                return true;
            }
		}
        else if(stage == 2){//El editor esta cerrado
            console.log("entro")
			if(cInd != 3){
				EditarCellElemento(rId);
                return true;	
			}
		} 				
	}
	function Refresh(){
        // url_get_catalogo_elemento = node_chapel_web + "entradas_salidas?instruccion=get_registros&cliente="+client_id+"&fechadesde="+fechaPrimerDia+"&fechahasta="+fechaUltimoDia+"&tipoDocumento=4";
		datastore.clearAll();
			grid.clearAll();
			datastore.load(url_get_catalogo_elemento,function(){
			// grid.clearAll();
			grid.sync(datastore);
		});
    }
    function Filtrar(fecha_desde,fecha_hasta){
        console.log(fecha_desde,fecha_hasta);
        url_get_catalogo_elemento = node_chapel_web + "entradas_salidas?instruccion=get_registros&cliente="+client_id+"&fechadesde="+fecha_desde+"&fechahasta="+fecha_hasta+"&tipoDocumento=9";
		datastore.clearAll();
			grid.clearAll();
			datastore.load(url_get_catalogo_elemento,function(){
			grid.clearAll();
			grid.sync(datastore);
		});
	}
	function onKeyPressed(code,ctrl,shift){
		if(code==67&&ctrl){
			if (!grid._selectionArea){
				return dhtmlx.alert(
					{
						title:"Alerta",
						type:"alert",
						text:"Debes seleccionar un bloque de la cuadricula previamente"
					}
				);
			} 
			grid.setCSVDelimiter("\t");
			grid.copyBlockToClipboard();
		}
		return true;
	}
	function AgregarElemento(){
		// $.get(node_chapel_web + "get_uuid", function(uuid){
			var width = document.body.clientWidth - 200;
        	var height = document.body.clientHeight;
			var ventanaAgregar = dhxWindow.createWindow({
				id:"ventanaAgregar",
				// left:Number(window.innerWidth * 0.1),
				text:"<b>Agregar Cliente</b>",
				// top:600,
				width:width,
				height:height,
				center:true,
				modal:true
			});
			dhxWindow.window("ventanaAgregar").button("minmax").attachEvent("onClick", function(win, button){
				var elemento = document.getElementsByClassName("dhxwin_active");
				for(var i = 0; i < elemento.length; i++){
					var elemento2 = document.getElementsByClassName("pantalla_completa");
					if(elemento2.length > 0){
						elemento[i].className = "dhxwin_active";
						dhxWindow.window("ventanaAgregar").setPosition(Number(window.innerWidth * 0.1), 0);
					}else{
						dhxWindow.window("ventanaAgregar").setPosition(0, 0);
						elemento[i].className += " pantalla_completa";
					}
				}
				return true;
			});
			document.body.onresize = ()=>{
				var width = document.body.clientWidth - 200;
				var height = document.body.clientHeight;
				try{
                	dhxWindow.window("ventanaAgregar").setDimension(width, height);
				}catch(error){
				}
			};
			var formAdd = ventanaAgregar.attachForm();
			formAdd.loadStruct("../data/json/agregar_clientes_hotel.json");
			formAdd.attachEvent("OnXLE", function(){
                formAdd.hideItem("activo");
                // formAdd.setItemValue("fecha", moment().format("YYYY-MM-DD HH:mm:ss"));
                // formAdd.setItemValue("quien_captura", usuario);
			});
			formAdd.attachEvent("onButtonClick", function(name){
				if (name=="enviar"){
					if(formAdd.validate()){
                        var values = formAdd.getFormData(true);
                        values.usuariocrea = nombreusuario;
						$.post("../../node_hotel/clientes_hotel?instruccion=agregar",values, function(data, textStatus, xhr) {
                            console.log(data, typeof data)
                            if(typeof data !== "string" ){
                                Refresh();
                                $("#nombre").val("foto_perfil"+data.insertId);
                                $("#btnSubmit").click();
                                ventanaAgregar.close();
                            }else{
                                dhtmlx.alert({
                                    title:"ERROR",
                                    type:"alert-error",
                                    text:data
                                });	
                            }
						});
					}
				}
			});
			formAdd.enableLiveValidation(true);			
		// });
	}
	function EditarElemento(selectedId){
		item = datastore.item(selectedId);
        delete item.id;
        console.log(item);
		var width = document.body.clientWidth - 200;
		var height = document.body.clientHeight;
		var ventanaEditar = dhxWindow.createWindow({
			id:"ventanaEditar",
			// left:Number(window.innerWidth * 0.1),
			text:"<b>Editar Cliente</b>",
			// top:600,
			width:width,
			height:height,
			center:true,
			modal:true
		});
		dhxWindow.window("ventanaEditar").button("minmax").attachEvent("onClick", function(win, button){
			var elemento = document.getElementsByClassName("dhxwin_active");
			for(var i = 0; i < elemento.length; i++){
				var elemento2 = document.getElementsByClassName("pantalla_completa");
				if(elemento2.length > 0){
					elemento[i].className = "dhxwin_active";
					dhxWindow.window("ventanaEditar").setPosition(Number(window.innerWidth * 0.1), 0);
				}else{
					dhxWindow.window("ventanaEditar").setPosition(0, 0);
					elemento[i].className += " pantalla_completa";
				}
			}
			return true;
		});
		document.body.onresize = ()=>{
			var width = document.body.clientWidth - 200;
			var height = document.body.clientHeight;
			try{
				dhxWindow.window("ventanaEditar").setDimension(width, height);
			}catch(error){
			}
			
		};
		var formEdit = ventanaEditar.attachForm();
		formEdit.loadStruct("../data/json/editar_clientes_hotel.json");
		formEdit.attachEvent("OnXLE", function(){
            formEdit.setItemValue("itemid", item.itemid);
            formEdit.setItemValue("activo", item.activo);
            formEdit.setItemValue("nombre", item.nombre);
			formEdit.setItemValue("edad", item.edad);
			formEdit.setItemValue("correo", item.correo);
			formEdit.setItemValue("direccion", item.direccion);
			formEdit.setItemValue("estado", item.estado);
			formEdit.setItemValue("ciudad", item.ciudad);
			formEdit.setItemValue("telefono", item.telefono);
			formEdit.setItemValue("rfc", item.rfc);
            formEdit.setItemLabel("enviar","<b>EDITAR</b>");
            var dhxCombito2 = formEdit.getCombo("empresa");
            dhxCombito2.attachEvent("onXLE", function(){					
                var option=dhxCombito2.getIndexByValue(item.empresa);
                if(option !== -1){
                    dhxCombito2.selectOption(option);
                }
			});
			$.get("../../node_hotel/empresas_hotel?instruccion=leer",function(empresas){
				dhxCombito2.attachEvent("onChange", function(index,value){	
					console.log(index,"index");
					console.log(value,"value");		
					let filtro;
					if(value != "ND") filtro = empresas.filter((a)=>{ return a.itemid == index; })[0];
					console.log("filtro",filtro);
					if(value != "ND") formEdit.setItemValue("rfc", filtro.rfc);
					if(value == "ND") formEdit.setItemValue("rfc", "");
					if(value == "ND")	formEdit.enableItem("rfc");
					else formEdit.disableItem("rfc");
				});
			});
		});
		formEdit.attachEvent("onButtonClick", function(name){
			if (name=="enviar"){
				if(formEdit.validate()){
                    var values = formEdit.getFormData(true);
                    values.usuarioedita = nombreusuario;
                    $.post("../../node_hotel/clientes_hotel?instruccion=actualizar",values, function(data, textStatus, xhr) {
                        if(data !== "ERROR"){
                            Refresh();
							ventanaEditar.close();
                        }else{
                            dhtmlx.alert({
                                title:"ERROR",
                                type:"alert-error",
                                text:"Ocurrio un error al editar"
                            });	
                        }
                    });					
				}
			}
		});
		formEdit.enableLiveValidation(true);	
	}
	function EliminarElemento(selectedId){
		item = datastore.item(selectedId);
		delete item.id;
		dhtmlx.confirm({
			title: "Confirmación",
			type:"confirm-warning",
			text: "Desea eliminar el registro seleccionado?",
			ok: "Si",
			cancel: "No",
			callback: function(result){
				if(result){		
                    item.usuarioelimina = nombreusuario;			
					$.post("../../node_hotel/clientes_hotel?instruccion=eliminar",item, function(data, textStatus, xhr) {
						if(textStatus == "success"){
							if(data !== "ERROR"){
								Refresh();
							}else{
								dhtmlx.alert({
									title:"ERROR",
									type:"alert-error",
									text:"Ocurrio un error al eliminar registro"
								});	
								Refresh();
							}
						}
					});
				}
			}
		});	
	}
	function EditarCellElemento(selectedId){
		item = datastore.item(selectedId);
		delete item.id;
        // item.instruccion = "ud";
        console.log(item);
        var empresa = grid.cells(selectedId,3).getValue();
        item.empresa = empresa;
		$.post("../../node_hotel/clientes_hotel?instruccion=actualizar",item, function(data, textStatus, xhr) {
			if(textStatus == "success"){
				if(data !== "ERROR"){
					Refresh();
				}else{
                    dhtmlx.alert({
                        title:"Warning",
                        type:"alert-warning",
                        text:"Ha ocurrido un error al editar el elemento"
                    });	
                    Refresh();	
				}
			}
		});	
    }
    function EditarCellElementoActivo(selectedId){
		item = datastore.item(selectedId);
		delete item.id;
        item.instruccion = "ud";
        if(item.activo == 1) item.activo = 0;
        else item.activo = 1;
		$.post("../../node_hotel/clientes_hotel?instruccion=actualizar",item, function(data, textStatus, xhr) {
			if(textStatus == "success"){
				if(data !== "ERROR"){
					Refresh();
				}else{
                    dhtmlx.alert({
                        title:"Warning",
                        type:"alert-warning",
                        text:"Ha ocurrido un error al editar el elemento"
                    });	
                    Refresh();	
				}
			}
		});	
    }

    function eXcell_es(cell){ //the eXcell name is defined here
        if (cell){                // the default pattern, just copy it
            this.cell = cell;
            this.grid = this.cell.parentNode.grid;
        }
        this.edit = function(){}  //read-only cell doesn't have edit method
        // the cell is read-only, so it's always in the disabled state
        this.isDisabled = function(){ return true; }
        this.setValue=function(val){
            let icono,row_id;
            row_id = this.cell.parentNode.idd;
            item = datastore.item(row_id);
			if(item.entrada_salida == 0){
                this.cell.style.color = "green";
                icono = `<i style="font-weigth:bolder;font-size:1.5em;" class="fa fa-arrow-circle-left" aria-hidden="true"></i> <span style="color:black;font-weight:bolder;font-size:2em;">|</span>`;
			}else{
                this.cell.style.color = "red";
                icono = `<span style="color:black;font-weight:bolder;font-size:2em;">|</span> <i style="font-weigth:bolder;font-size:1.5em;" class="fa fa-arrow-circle-right" aria-hidden="true"></i>`;
			}
			this.setCValue(icono,val);
        }
    }
    eXcell_es.prototype = new eXcell;// nests all other methods from the base class
    function eXcell_empresa(cell){ //the eXcell name is defined here
        if (cell){                // the default pattern, just copy it
            this.cell = cell;
            this.grid = this.cell.parentNode.grid;
            // eXcell_ed.call(this);
        }
        /* this.edit = function(){
            this.val = this.getValue(); // save current value
            // alert(this.val)
            var valor = this.val;
            var html = "<select class='select1' style='width:150px;'>";
            html += "<option value=''>ND</option>";
            empresas.forEach((element,i) => {
                if(this.val == element) html += "<option value='"+i+"' selected>" + element + "</option>";
                else html += "<option value='"+i+"'>" + element + "</option>";
            });
            this.cell.innerHTML = html + "</select>"; // editor's html
            // this.cell.firstChild.value=parseInt(val); // set the first part of data
            // if (val.indexOf("PM")!=-1) this.cell.childNodes[1].value="PM";
            // blocks onclick event
            this.cell.childNodes[0].onclick=function(e){ (e||event).cancelBubble=true;} 
            console.log(this.cell.childNodes[0],"this.cell.childNodes")
            // $('.select1').click();
            // this.cell.childNodes[0].onclick=function(e){ 
            //     alert("clicko")
            //     (e||event).cancelBubble=true;
            // } 
            // blocks onclick event
            // this.cell.childNodes[1].onclick=function(e){ (e||event).cancelBubble=true;}
        }  //read-only cell doesn't have edit method */
        // the cell is read-only, so it's always in the disabled state
        this.isDisabled = function(){ return true; }
        /* this.detach=function(){
            console.log("detach",this)
            console.log(this.cell.childNodes);
            this.setValue(this.cell.childNodes[0].value);
            return this.val!=this.getValue();
            // sets the new value
            // this.setValue(this.cell.childNodes[0].value+" "+this.cell.childNodes[1].value); 
            // return this.val!=this.getValue(); // compares the new and the old values
        } */
        this.getValue=function(){
            return this.cell.innerHTML; // get value
        }
        this.setValue=function(val){
            if(typeof empresas[val] != "undefined") this.setCValue(empresas[val],val);
            else this.setCValue("",val);
        }
    }
	eXcell_empresa.prototype = new eXcell;// nests all other methods from the base class
	function mostrarImagen(archivo,tipo){
		console.log("se abrirá la imagen",archivo,tipo);
		var width = Number(window.innerWidth / 2);
		var ventanaIntercambio = dhxWindow.createWindow({
			id:"abrirImage",
			// left:Number(window.innerWidth * 0.1),
			text:"Imagen",
			// top:0,
			width:width,
			height:600,
			center:true,
			modal:true
		});
		dhxWindow.window("abrirImage").button("minmax").attachEvent("onClick", function(win, button){

			var elemento = document.getElementsByClassName("dhxwin_active");
			for(var i = 0; i < elemento.length; i++){
				var elemento2 = document.getElementsByClassName("pantalla_completa");
				if(elemento2.length > 0){
					elemento[i].className = "dhxwin_active";
					dhxWindow.window("abrirImage").setPosition(Number(window.innerWidth * 0.1), 0);
				}else{
					dhxWindow.window("abrirImage").setPosition(0, 0);
					elemento[i].className += " pantalla_completa";
				}
			}
			return true;
		});
		dhxWindow.window("abrirImage").attachHTMLString("<div style='width:calc(100%)!important;height:100%!important'><iframe allowfullscreen src='"+node_chapel_web +"mostrarArchivo?tipo="+tipo+"&archivo="+archivo+"' width='100%' height='100%'"+"></iframe></div>");
		ventanaIntercambio.button("close").attachEvent("onClick", function(){
			// Refresh();
			return true; // deny default action
		});
	}
	function eXcell_fotito(cell){ //the eXcell name is defined here
        if (cell){                // the default pattern, just copy it
            this.cell = cell;
            this.grid = this.cell.parentNode.grid;
        }
        this.edit = function(){}  //read-only cell doesn't have edit method
        // the cell is read-only, so it's always in the disabled state
        this.isDisabled = function(){ return true; }
        this.setValue=function(val){
            let icono,row_id;
            row_id = this.cell.parentNode.idd;
			item = datastore.item(row_id);
			console.log(item.fotos);
			if(item.fotos && item.fotos != ""){
				let fotos = JSON.parse(item.fotos);
				let todaslasfotos = "";
				for (let key in fotos) {
					console.log(key);
					if (fotos.hasOwnProperty(key)) {
						let element = fotos[key];	
						console.log(element);
						for (const llave in element) {
							if (element.hasOwnProperty(llave)) {
								let element2 = element[llave];								
								console.log(element2,llave);					
								let foto = `<i style="font-weigth:bolder;font-size:1.5em;display:inline-block;padding-right:1px" onclick="mostrarImagen('`+element2+`','Imagen')" class="fa fa-picture-o" aria-hidden="true"></i>`;
								todaslasfotos += foto;
							}
						}
					}
				}
				//tipo=Imagen,archivo=foto
				this.cell.style.color = "green";
				this.setCValue("<div style='width:100%;'>" + todaslasfotos + "</div>",fotos.length);
			}
        }
    }
	eXcell_fotito.prototype = new eXcell;// nests all other methods from the base class
	function rfcValido(rfc, aceptarGenerico = true) {
		const re       = /^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$/;
		var   validado = rfc.match(re);

		if (!validado)  //Coincide con el formato general del regex?
			return false;

		//Separar el dígito verificador del resto del RFC
		const digitoVerificador = validado.pop(),
			rfcSinDigito      = validado.slice(1).join(''),
			len               = rfcSinDigito.length,

		//Obtener el digito esperado
			diccionario       = "0123456789ABCDEFGHIJKLMN&OPQRSTUVWXYZ Ñ",
			indice            = len + 1;
		var   suma,
			digitoEsperado;

		if (len == 12) suma = 0
		else suma = 481; //Ajuste para persona moral

		for(var i=0; i<len; i++)
			suma += diccionario.indexOf(rfcSinDigito.charAt(i)) * (indice - i);
		digitoEsperado = 11 - suma % 11;
		if (digitoEsperado == 11) digitoEsperado = 0;
		else if (digitoEsperado == 10) digitoEsperado = "A";

		//El dígito verificador coincide con el esperado?
		// o es un RFC Genérico (ventas a público general)?
		if ((digitoVerificador != digitoEsperado)
		&& (!aceptarGenerico || rfcSinDigito + digitoVerificador != "XAXX010101000"))
			return false;
		else if (!aceptarGenerico && rfcSinDigito + digitoVerificador == "XEXX010101000")
			return false;
		return rfcSinDigito + digitoVerificador;
	}
</script>
</html>