global.comparaciontractores = `
<div style="width: 8cm;height:20.79cm;display:inline-block;line-height: 100%;">
<table cellspacing="0" cellpadding="0" style="width:8cm;letter-spacing: 0">
<tbody>
<tr>
    <td class="logo"><img class="logo" alt="icono" src="{{url_icono}}"></td>
    <td style="text-align: right" class="titulo2 font-13">{{titulo_header}}</td>
</tr>
</tbody>
</table>
<table style="width: 8cm;" cellspacing="0" cellpadding="0">
<tbody>
    <tr>
        <td class="motor_carrier">
            <table cellpadding="1" cellspacing="0" stye="width:4cm">
                <tbody>
                    <tr>
                        <td class="font-7" style="line-height:100%;width: 2cm;text-align:left;font-weight:bolder">Transportista:</td>
                        <td class="font-7" style="line-height:100%;width: 2cm;text-align:right">{{transportista}}</td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td class="tractor_num">
            <table cellpadding="1" cellspacing="0" stye="width:4cm">
                <tbody>
                    <tr>
                        <td class="font-7" style="line-height:100%;width: 2cm;text-align:left;font-weight:bolder">N° Tractor:</td>
                        <td class="font-7" style="line-height:100%;width: 2cm;text-align:right">{{numero_tractor}}</td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</tbody>
</table>
<table style="vertical-align:top;width:8cm" cellspacing="0" cellpadding="2" >
    <tbody>
        <tr>
            
            <td style="display: table;vertical-align:top;width: 2cm!important;font-size:7px;">
                <table style="vertical-align:top;width:100%" cellspacing="0" cellpadding="0" >
                    <tbody>
                        <tr>
                            <td style="width:50%;font-size:6px;font-weight:bolder;line-height:100%;text-overflow: ellipsis;">Nombre de operador:</td>
                            <td style="width:50%;font-size:6px;line-height:100%;word-break: break-all;text-align:right">{{nombre_operador_value}}</td>
                        </tr>
                    </tbody>
                </table>       
                <table style="vertical-align:top;width:100%" cellspacing="0" cellpadding="0" >
                    <tbody>
                        <tr>
                            <td style="width:50%;font-size:6px;font-weight:bolder;line-height:100%;text-overflow: ellipsis;">Estado de unidad:</td>
                            <td style="width:50%;font-size:6px;line-height:100%;word-break: break-all;text-align:right">{{limpia_sucia_value}}</td>
                        </tr>
                    </tbody>
                </table>  
                <table style="vertical-align:top;width:100%" cellspacing="0" cellpadding="0" >
                    <tbody>
                        <tr>
                            <td style="width:50%;font-size:6px;font-weight:bolder;line-height:100%;text-overflow: ellipsis;">Patio:</td>
                            <td style="width:50%;font-size:6px;line-height:100%;word-break: break-all;text-align:right">{{patio_value}}</td>
                        </tr>
                    </tbody>
                </table>          
                <table style="vertical-align:top;width:100%" cellspacing="0" cellpadding="0" >
                    <tbody>
                        <tr>
                            <td style="width:50%;font-size:6px;font-weight:bolder;line-height:100%;text-overflow: ellipsis;">{{origin_destiny_title}}</td>
                            <td style="width:50%;font-size:6px;line-height:100%;word-break: break-all;text-align:right">{{origin_destiny_value}}</td>
                        </tr>
                    </tbody>
                </table>         
                <table style="vertical-align:top;width:100%" cellspacing="0" cellpadding="0" >
                    <tbody>
                        <tr>
                            <td style="width:50%;font-size:6px;font-weight:bolder;line-height:100%;text-overflow: ellipsis;">Fecha de Recibido:</td>
                            <td style="width:50%;font-size:6px;line-height:100%;word-break: break-all;text-align:right">{{date_time_ingreso}}</td>
                        </tr>
                    </tbody>
                </table>       
                <table style="vertical-align:top;width:2.3cm" cellspacing="0" cellpadding="0" >
                    <tbody>
                    <td class="hereby">
                    <p class="font-6" style="text-align:center;white-space: pre-line;width:100%;margin:0;padding: 0;vertical-align: top;line-height: 100%">Por la presente declaro que en la fecha anterior, he inspeccionado cuidadosamente el equipo y que este informe es verdadero y correcto como resultado de dicha inspección.
                    </p>
                    <table>
                        <tbody>
                            <tr>
                                <td style="height:20px;text-align: center;vertical-align: middle">
                                    <label class="font-7" style="text-align: center;vertical-align: middle" for="firma_user">{{firma_user}}</label>
                                </td>
                            </tr>
                            <!-- <tr>
                                <td class="line">
                                    <hr class="hr">
                                </td>
                            </tr> -->
                            <tr style="line-height: 100%">
                                <td style="font-weight: bolder;text-align: center;vertical-align: middle" class="herebyconcepts font-7">
                                    Firma del personal de seguridad
                                </td>                                                                
                            </tr>
                        </tbody>
                    </table>
                </td>
                    </tbody>
                </table>                     
            </td>
    <td style="width:6cm!important;vertical-align: top;">
        <table cellspacing="0" cellpadding="0" style="padding:0;vertical-align: top;">
            <tbody>
                <tr>
                    <td style="width:3cm!important;vertical-align:top;">
                        <table style="vertical-align: top;" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td style="width:100%;margin:0;padding:0;vertical-align: top;height:75px">
                                        <table style="width:100%;">
                                            <tbody>
                                                <tr>
                                                    <td style="width:50%;height:75px;vertical-align: top;">
                                                        <img style="width:100%;height:100%" src="{{FRONTAL}}" alt="front" srcset="{{FRONTAL}}">
                                                    </td>                                                                     
                                                    <td style="width:50%;height:75px;vertical-align: top;">
                                                        <img style="width:100%;height:100%" src="{{LADO_DERECHOT}}" alt="right" srcset="{{LADO_DERECHOT}}">
                                                    </td>
                                                </tr>
                                                
                                            </tbody>
                                        </table>                                                            
                                    </td>                                                       
                                    
                                </tr>
                                <tr>
                                    <td style="width:100%;margin:0;padding:0;vertical-align: top;height:75px">
                                        <table style="width:100%;">
                                            <tbody>
                                                <tr>
                                                    <td style="width:50%;height:75px;vertical-align: top;;">
                                                        <img style="width:100%;height:100%" src="{{LADO_IZQUIERDOT}}" alt="left" srcset="{{LADO_IZQUIERDOT}}">
                                                    </td>                 
                                                    <td style="width:50%;height:75px;vertical-align: top;">
                                                        <img style="width:100%;height:100%" src="{{TRASEROT}}" alt="rear" srcset="{{TRASEROT}}">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>                                                            
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:100%;margin:0;padding:0;vertical-align: top;height:75px">
                                        <table>
                                            <tbody>   
                                                <tr>
                                                    <td style="width:50%;height:75px;vertical-align: top;;">
                                                        <img style="width:100%;height:100%" src="{{INTERIOR}}" alt="interior" srcset="{{INTERIOR}}">
                                                    </td>
                                                    <td style="width:50%;height:75px;vertical-align: top;">
                                                        
                                                    </td>                 
                                                </tr> 
                                            </tbody>
                                        </table>                                                         
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td style="width:3cm;vertical-align: top;display: block">
                        <div style="width:100%;display: flex;">
                            <div style="width:100%;line-height: 100%;word-break: break-all;display:inline-bock">
                                <div class="font-7" style="width:100%;background-color:black;color:white;text-align:center;font-weight: bolder;vertical-align: top;">DEFECTOS</div>   
                                
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;vertical-align: top;">
                                        {{checkeddefensa_defecto}} Defensa
                                    </div>                                                          
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;vertical-align: top;">
                                        {{checkedplaca_defecto}} Placa
                                    </div>
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;vertical-align: top;">
                                        {{checkedluces_defecto}} Luces
                                    </div>
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;vertical-align: top;">
                                        {{checkedparabisas_defecto}} Parabrisas
                                    </div>
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;vertical-align: top;">
                                        {{checkedcofre_defecto}} Cofre
                                    </div>
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;vertical-align: top;">
                                        {{checkedvolante_defecto}} Volante
                                    </div>
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;vertical-align: top;">
                                        {{checkedpalanca_cambios_defecto}} Palanca de cambios
                                    </div>
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;vertical-align: top;">
                                        {{checkedtablero_defecto}} Tablero
                                    </div>
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;vertical-align: top;">
                                        {{checkedtapetes_defecto}} Tapetes
                                    </div>
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;vertical-align: top;">
                                        {{checkedluces_interiores_defecto}} Luces interiores
                                    </div>
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;vertical-align: top;">
                                        {{checkedasientos_defecto}} Asientos
                                    </div>
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;vertical-align: top;">
                                        {{checkedcolchon_defecto}} Colchón
                                    </div>
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;vertical-align: top;">
                                        {{checkedlimpieza_defecto}} Limpieza
                                    </div>
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;vertical-align: top;">
                                        {{checkedvidrioi_defecto}} Vidrio Izquierdo
                                    </div>
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;vertical-align: top;">
                                        {{checkedestribosi_defecto}} Estribos izquierdos
                                    </div>
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;vertical-align: top;">
                                        {{checkedtanque_combustiblei_defecto}} Tanque de combustible izquierdo
                                    </div>
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;vertical-align: top;">
                                        {{checkedtapon_combustiblei_defecto}} Tapón de combustible izquierdo
                                    </div>
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;vertical-align: top;">
                                        {{checkedmangueras_defecto}} Mangueras
                                    </div>
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;vertical-align: top;">
                                        {checkedchirrion_defecto}} Chirrión
                                    </div>
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;vertical-align: top;">
                                        {{checkedquinta_rueda_defecto}} Quinta rueda
                                    </div>
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;vertical-align: top;">
                                        {{checkedplaca_trasera_defecto}} Placa trasera
                                    </div>
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;vertical-align: top;">
                                        {{checkedvidriod_defecto}} Vidrio derecho
                                    </div>
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;vertical-align: top;">
                                        {{checkedestribosd_defecto}} Estribos derechos
                                    </div>
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;vertical-align: top;">
                                        {{checkedtanque_combustibled_defecto}} Tanques de combustibe derechos
                                    </div>
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;vertical-align: top;">
                                        {{checkedtapon_combustibled_defecto}} Tapón de combustible derecho
                                    </div>

                                <div class="font-7" style="width:100%;background-color:black;color:white;text-align:center;font-weight: bolder;vertical-align: top;">P. CTPAT</div>   
                                
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                        {{checkeddefensa_tractor}} Defensa de tractor
                                    </div>
                    
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                        {{checkedmotor}} Motor
                                    </div>
                    
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                        {{checkedllantas_camion}} Llantas del camión
                                    </div>
            
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                        {{checkedpiso_interior_camion}} Piso interior del camión
                                    </div>
                        
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                        {{checkedtanques_combustible}} Tanques de combustible
                                    </div>
                        
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                        {{checkedcabina_comportamientos}} Cabina
                                    </div>
                            
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                        {{checkedtanques_aire}} Tanques de aire
                                    </div>
                            
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                        {{checkedchasis_camion}} Chasis del camión
                                    </div>
                            
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                        {{checkedquinta}} Quinta rueda
                                    </div>
                            
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                        {{checkedchasis_remolque}} Chasis del remolque
                                    </div>
                                
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                        {{checkedllantas_remolque}} Defensas del remolque
                                    </div>
                                
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                        {{checkeddefensas_remolque}} Puertas del remolque
                                    </div>
                            
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                        {{checkedpuertas_remolque}} Sellos de alta seguridad
                                    </div>
                        
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                    {{checkedsellos_alta_seguridad}} Paredes laterales del remolque
                                    </div>
                                
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                        {{checkedparedes_laterales_remolque}} Pared frontal del remolque
                                    </div>
                                
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                        {{checkedpared_frontal_remolque}} Techo de caja
                                    </div>
                                
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                        {{checkedtecho_caja}} Escape de tractor
                                    </div>
                                
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                        {{checkedescape_tractor}} Piso interior del remolque
                                    </div>
                                
                                
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                        {{checkedpiso_interior_remolque}} Paredes internas del remolque
                                    </div>
                                
                                
                                    <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                        {{checkedparedes_internas_remolque}} Paredes internas del remolque
                                    </div>
                                
                            </div>                                                  
                        </div>
                    </td>
                </tr>                                               
            </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<div style="width:8cm!important;">
<b style="width:100%;padding-top:0" class="font-7">Comentarios:</b>
<p class="font-7" style="width:100%;padding-top:0;white-space: pre-line;
margin: 0!important;vertical-align: top;line-height: 90%">{{notes}}</p>
<b style="width:100%;padding-top:0" class="font-7">Llantas:</b>
<table cellpadding="1" cellspacing="0" stye="width:100%">
<thead>
    <tr>
        <th class="font-6" style="line-height:100%;width: 16.66%;background-color:black;color:white;text-align:center;">LLANTA</th>
        <th class="font-6" style="line-height:100%;width: 16.66%;background-color:black;color:white;text-align:center;">CONDICIÓN</th>
        <th class="font-6" style="line-height:100%;width: 16.66%;background-color:black;color:white;text-align:center;">PROFUNDIDAD</th>
        <th class="font-6" style="line-height:100%;width: 16.66%;background-color:black;color:white;text-align:center;">LLANTA</th>
        <th class="font-6" style="line-height:100%;width: 16.66%;background-color:black;color:white;text-align:center;">CONDICIÓN</th>
        <th class="font-6" style="line-height:100%;width: 16.66%;background-color:black;color:white;text-align:center;">PROFUNDIDAD</th>
    </tr>
</thead>
<tbody>
    <tr>
        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">1</td>
        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{condicionllanta1}}</td>
        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{prof1}}</td>
        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">2</td>
        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{condicionllanta2}}</td>
        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{prof2}}</td>
    </tr>
    <tr>
        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">3</td>
        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{condicionllanta3}}</td>
        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{prof3}}</td>
        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">4</td>
        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{condicionllanta4}}</td>
        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{prof4}}</td>
    </tr>
    <tr>
        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">5</td>
        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{condicionllanta5}}</td>
        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{prof5}}</td>
        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">6</td>
        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{condicionllanta6}}</td>
        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{prof6}}</td>
    </tr>
    <tr>
        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">7</td>
        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{condicionllanta7}}</td>
        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{prof7}}</td>
        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">8</td>
        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{condicionllanta8}}</td>
        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{prof8}}</td>
    </tr>
    <tr>
        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">9</td>
        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{condicionllanta9}}</td>
        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{prof9}}</td>
        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">10</td>
        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{condicionllanta10}}</td>
        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{prof10}}</td>
    </tr>
</tbody>
</table>
<p class="font-7" style="margin-left:25%;width:50%;margin-right:25%;white-space: pre-line;padding: 0!important;vertical-align: top;line-height: 90%;text-align: center;">Por la presente declaro mis presencias durante la inspección del equipo y reconozco que las condiciones mencionadas en estas inspecciones son verdaderas y correctas.</p>
<div style="width:8cm">
<img src="{{Firma_operador}}" style="margin-left:25%;margin-right:25%;width:50%;height:50px" alt="firma" srcset="{{Firma_operador}}">
</div>
<div style="width:8cm">
<p style="font-weight:bolder;margin-left:25%;width:50%;margin-right:25%;text-align: center;vertical-align: middle" class="font-7">Firma del Operador</p>
</div>
</div>
</div>
{{fotos_grid}}
`;
global.comparacioncajas =  `
<div style="width: 8cm;height:20.75cm;display:inline-block;line-height: 100%;">
<table cellspacing="0" cellpadding="0" style="width:8cm;letter-spacing: 0">
<tbody>
    <tr>
        <td class="logo"><img class="logo" alt="icono" src="{{url_icono}}"></td>
        <td style="text-align: right" class="titulo2 font-13">{{titulo_header}}</td>
    </tr>
</tbody>
</table>
<table style="width: 8cm;" cellspacing="0" cellpadding="0">
<tbody>
    <tr>
        <td class="motor_carrier">
            <table cellpadding="1" cellspacing="0" stye="width:4cm">
                <tbody>
                    <tr>
                        <td class="font-7" style="line-height:100%;width: 2cm;text-align:left;font-weight:bolder">Dueño de Caja:</td>
                        <td class="font-7" style="line-height:100%;width: 2cm;text-align:right">{{dueno_caja}}</td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td class="tractor_num">
            <table cellpadding="1" cellspacing="0" stye="width:4cm">
                <tbody>
                    <tr>
                        <td class="font-7" style="line-height:100%;width: 2cm;text-align:left;font-weight:bolder">N° Caja:</td>
                        <td class="font-7" style="line-height:100%;width: 2cm;text-align:right">{{numero_caja}}</td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</tbody>
</table>
<table style="display: inline-table;vertical-align:top;width:8cm" cellspacing="0" cellpadding="2" >
<tbody>
    <tr>
        <td style="vertical-align:top;width: 2cm!important;font-size:9px;">
            <table cellpadding="1" cellspacing="0" stye="width:100%">
                <tbody>
                    <tr>
                        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:left;font-weight:bolder"># Tractor:</td>
                        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:right;">{{numero_tractor_value}}</td>
                    </tr>
                    <tr>
                        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:left;font-weight:bolder">Transportista:</td>
                        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:right;">{{transportista_value}}</td>
                    </tr>
                    <tr>
                        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:left;font-weight:bolder">{{origin_destiny_title}}</td>
                        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:right;">{{origin_destiny_value}}</td>
                    </tr>
                    <tr>
                        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:left;font-weight:bolder">Fecha de Recibido:</td>
                        <td class="font-6" style="line-height:100%;width: 16.66%;text-align:right;">{{date_time_ingreso}}</td>
                    </tr>
                </tbody>
            </table>
            <table style="vertical-align:top;width:2.3cm" cellspacing="0" cellpadding="0" >
                <tbody>
                    <tr>                    
                        <td class="hereby">
                            <p class="font-6" style="text-align:center;white-space: pre-line;width:100%;margin:0;padding: 0;vertical-align: top;line-height: 100%">Por la presente declaro que en la fecha anterior, he inspeccionado cuidadosamente el equipo y que este informe es verdadero y correcto como resultado de dicha inspección.
                            </p>
                            <table>
                                <tbody>
                                    <tr>
                                        <td style="height:20px;text-align: center;vertical-align: middle">
                                            <label class="font-7" style="text-align: center;vertical-align: middle" for="firma_user">{{firma_user}}</label>
                                        </td>
                                    </tr>
                                    <!-- <tr>
                                        <td class="line">
                                            <hr class="hr">
                                        </td>
                                    </tr> -->
                                    <tr style="line-height: 100%">
                                        <td style="font-weight: bolder;text-align: center;vertical-align: middle" class="herebyconcepts font-7">
                                            Firma del personal de seguridad
                                        </td>                                                                
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
         
           
            <div style="width:100%;display:inline-flex">
            
                <div  class="font-6" style="width:1cm;display: inline-block;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:100%">
                    Estado de caja:
                </div>
                <div style="width:1cm;display: inline-block;text-align: right;text-overflow: ellipsis;line-height:100%;word-break: break-all;">
                    <div style="width:100%;" class="font-6">Vacia {{ckeckedempty}}</div>
                    <div style="width:100%;" class="font-6">Cargada {{checkedloaded}}</div>
                </div>
                
            </div>
            <div style="width:100%;display:inline-flex">
                <div class="font-6" style="width:1cm;word-break: break-all;display: inline-block;text-align: left;font-weight: bolder;line-height:100%">
                    Consignatario:
                </div>
                <div class="font-6" style="width:1cm;display: inline-block;text-align: right;text-overflow: ellipsis;line-height:100%;word-break: break-all;">
                    {{consign}}
                </div>
                
            </div>
            <div style="width:100%;display:inline-flex">
                <div class="font-6" style="width:1cm;display: inline-block;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:100%">
                    Sello:
                </div>
                <div class="font-6" style="width:1cm;display: inline-block;text-align: right;text-overflow: ellipsis;line-height:100%;word-break: break-all;">
                    {{sealnumber}}
                </div>
                
            </div>
            <div style="width:100%;display:inline-flex">
                <div class="font-6" style="width:1cm;display: inline-block;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:100%">
                    Placa?
                </div>
                <div class="font-6" style="width:1cm;display: inline-block;text-align: right;text-overflow: ellipsis;line-height:100%;word-break: break-all;">
                    {{Placa}}
                </div>                                        
            </div>
            <div style="width:100%;display:inline-flex">
                <div class="font-6" style="width:1cm;display: inline-block;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:100%">
                    Placa de vin?
                </div>
                <div class="font-6" style="width:1cm;display: inline-block;text-align: right;text-overflow: ellipsis;line-height:100%;word-break: break-all;">
                    {{placa_vin}}
                </div>                                        
            </div>
            <div style="width:100%;display:inline-flex">
                <div class="font-6" style="width:1cm;display: inline-block;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:100%">
                    Manitas de aire?
                </div>
                <div class="font-6" style="width:1cm;display: inline-block;text-align: right;text-overflow: ellipsis;line-height:100%;word-break: break-all;">
                    {{manitas_aire}}
                </div>                                        
            </div>                                      
        </td>
        <td style="width:6cm!important;vertical-align: top;">
            <table cellspacing="0" cellpadding="0" style="padding:0;vertical-align: top;">
                <tbody>
                    <tr>
                        <td style="width:3cm!important;vertical-align:top;">
                            <table style="vertical-align: top;" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <td style="width:100%;margin:0;padding:0;vertical-align: top;height:75px">
                                            <table style="width:100%;">
                                                <tbody>
                                                    <tr>
                                                        <td style="width:50%;height:75px;vertical-align: top;">
                                                            <img style="width:100%;height:100%" src="{{LADO_FRONTAL}}" alt="front" srcset="{{LADO_FRONTAL}}">
                                                        </td>                                                                     
                                                        <td style="width:50%;height:75px;vertical-align: top;">
                                                            <img style="width:100%;height:100%" src="{{LADO_DERECHO}}" alt="right" srcset="{{LADO_DERECHO}}">
                                                        </td>
                                                    </tr>
                                                    
                                                </tbody>
                                            </table>                                                            
                                        </td>                                                       
                                        
                                    </tr>
                                    <tr>
                                        <td style="width:100%;margin:0;padding:0;vertical-align: top;height:75px">
                                            <table style="width:100%;">
                                                <tbody>
                                                    <tr>
                                                        <td style="width:50%;height:75px;vertical-align: top;;">
                                                            <img style="width:100%;height:100%" src="{{LADO_IZQUIERDO}}" alt="left" srcset="{{LADO_IZQUIERDO}}">
                                                        </td>                 
                                                        <td style="width:50%;height:75px;vertical-align: top;">
                                                            <img style="width:100%;height:100%" src="{{LADO_TRASERO}}" alt="rear" srcset="{{LADO_TRASERO}}">
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>                                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:100%;margin:0;padding:0;vertical-align: top;height:75px">
                                            <table>
                                                <tbody>   
                                                    <tr>
                                                        <td style="width:50%;height:75px;vertical-align: top;;">
                                                            <img style="width:100%;height:100%" src="{{TECHO_PISO}}" alt="topandfloor" srcset="{{TECHO_PISO}}">
                                                        </td>
                                                        <td style="width:50%;height:75px;vertical-align: top;">
                                                            
                                                        </td>                 
                                                    </tr> 
                                                </tbody>
                                            </table>                                                         
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td style="width:3cm;vertical-align: top;display: block">
                            <div style="width:100%;display: flex;">
                                <div style="width:100%;line-height: 100%;word-break: break-all;display:inline-bock">
                                    <div class="font-7" style="width:100%;background-color:black;color:white;text-align:center;font-weight: bolder;vertical-align: top;">DEFECTOS</div>   
                                  
                                        <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;vertical-align: top;">
                                            {{checkedairlinesdefect}} Manitas de aire
                                        </div>
                              
                                        <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                            {{checkeddoorsdefect}} Puertas
                                        </div>
                           
                                        <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                            {{checkedlandinggeardefect}} Tren de aterrizaje
                                        </div>
                             
                                        <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                            {{checkedreflectorsalldefect}} Todos los reflectores
                                        </div>
                           
                                        <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                            {{checkedslideroftandemsdefect}} Deslizador de tandems
                                        </div>
                                
                                        <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                            {{checkedtwistlooksdefect}} Cerraduras
                                        </div>
                            
                                        <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                            {{checkedundercarriagedefect}} Carro Bajo
                                        </div>
                                
                                    <div class="font-7" style="width:100%;background-color:black;color:white;text-align:center;font-weight: bolder;vertical-align: top;">P. CTPAT</div>   
                                   
                                        <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                            {{checkeddefensa_tractor}} Defensa de tractor
                                        </div>
                        
                                        <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                            {{checkedmotor}} Motor
                                        </div>
                      
                                        <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                            {{checkedllantas_camion}} Llantas del camión
                                        </div>
              
                                        <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                            {{checkedpiso_interior_camion}} Piso interior del camión
                                        </div>
                            
                                        <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                            {{checkedtanques_combustible}} Tanques de combustible
                                        </div>
                          
                                        <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                            {{checkedcabina_comportamientos}} Cabina
                                        </div>
                                
                                        <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                            {{checkedtanques_aire}} Tanques de aire
                                        </div>
                               
                                        <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                            {{checkedchasis_camion}} Chasis del camión
                                        </div>
                             
                                        <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                            {{checkedquinta}} Quinta rueda
                                        </div>
                               
                                        <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                            {{checkedchasis_remolque}} Chasis del remolque
                                        </div>
                                  
                                        <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                            {{checkedllantas_remolque}} Defensas del remolque
                                        </div>
                                   
                                        <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                            {{checkeddefensas_remolque}} Puertas del remolque
                                        </div>
                               
                                        <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                            {{checkedpuertas_remolque}} Sellos de alta seguridad
                                        </div>
                         
                                        <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                        {{checkedsellos_alta_seguridad}} Paredes laterales del remolque
                                        </div>
                                   
                                        <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                            {{checkedparedes_laterales_remolque}} Pared frontal del remolque
                                        </div>
                                    
                                        <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                            {{checkedpared_frontal_remolque}} Techo de caja
                                        </div>
                                 
                                        <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                            {{checkedtecho_caja}} Escape de tractor
                                        </div>
                                    
                                        <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                            {{checkedescape_tractor}} Piso interior del remolque
                                        </div>
                                   
                                   
                                        <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                            {{checkedpiso_interior_remolque}} Paredes internas del remolque
                                        </div>
                                   
                                    
                                        <div class="font-6" style="width:100%;text-align: left;font-weight: bolder;text-overflow: ellipsis;line-height:130%;word-break: break-all;">
                                            {{checkedparedes_internas_remolque}} Paredes internas del remolque
                                        </div>
                                   
                                </div>                                                  
                            </div>
                        </td>
                    </tr>                                               
                </tbody>
            </table>
        </td>
    </tr>
</tbody>
</table>
<div style="width:8cm!important;">
<b style="width:100%;padding-top:0" class="font-7">Comentarios:</b>
<p class="font-7" style="width:100%;padding-top:0;white-space: pre-line;
margin: 0!important;vertical-align: top;line-height: 90%">{{notes}}</p>
<b style="width:100%;padding-top:0" class="font-7">Llantas:</b>
<table cellpadding="1" cellspacing="0" stye="width:100%">
    <thead>
        <tr>
            <th class="font-6" style="line-height:100%;width: 16.66%;background-color:black;color:white;text-align:center;word-break: break-all;">LLANTA</th>
            <th class="font-6" style="line-height:100%;width: 16.66%;background-color:black;color:white;text-align:center;word-break: break-all;">CONDICIÓN</th>
            <th class="font-6" style="line-height:100%;width: 16.66%;background-color:black;color:white;text-align:center;word-break: break-all;">PROFUNDIDAD</th>
            <th class="font-6" style="line-height:100%;width: 16.66%;background-color:black;color:white;text-align:center;word-break: break-all;">LLANTA</th>
            <th class="font-6" style="line-height:100%;width: 16.66%;background-color:black;color:white;text-align:center;word-break: break-all;">CONDICIÓN</th>
            <th class="font-6" style="line-height:100%;width: 16.66%;background-color:black;color:white;text-align:center;word-break: break-all;">PROFUNDIDAD</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">1</td>
            <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{condicionllanta1}}</td>
            <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{prof1}}</td>
            <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">2</td>
            <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{condicionllanta2}}</td>
            <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{prof2}}</td>
        </tr>
        <tr>
            <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">3</td>
            <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{condicionllanta3}}</td>
            <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{prof3}}</td>
            <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">4</td>
            <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{condicionllanta4}}</td>
            <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{prof4}}</td>
        </tr>
        <tr>
            <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">5</td>
            <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{condicionllanta5}}</td>
            <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{prof5}}</td>
            <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">6</td>
            <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{condicionllanta6}}</td>
            <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{prof6}}</td>
        </tr>
        <tr>
            <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">7</td>
            <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{condicionllanta7}}</td>
            <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{prof7}}</td>
            <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">8</td>
            <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{condicionllanta8}}</td>
            <td class="font-6" style="line-height:100%;width: 16.66%;text-align:center;">{{prof8}}</td>
        </tr>
    </tbody>
</table>
<p class="font-7" style="margin-left:25%;width:50%;margin-right:25%;white-space: pre-line;padding: 0!important;vertical-align: top;line-height: 90%;text-align: center;">Por la presente declaro mis presencias durante la inspección del equipo y reconozco que las condiciones mencionadas en estas inspecciones son verdaderas y correctas.</p>
<div style="width:8cm">
    <img src="{{Firma_operador}}" style="margin-left:25%;margin-right:25%;width:50%;height:50px" alt="firma" srcset="{{Firma_operador}}">
</div>
<div style="width:8cm">
    <p style="font-weight:bolder;margin-left:25%;width:50%;margin-right:25%;text-align: center;vertical-align: middle" class="font-7">Firma del Operador</p>
</div>
</div>
</div>
{{fotos_grid}}
`;
global.formulariocajas = `
<style>
td,th{
    width:50%!important;
}
</style>
<table style="width:200px;height:auto;max-width:300px;font-famili:Arial;font-size:10px;">
        <thead>
            <tr>
                <th style="background-color:black;color:white;padding:5px;font-weight: bolder;font-size:10px;text-align:left;vertical-align:middle">CONCEPTO</th>
                <th style="background-color:black;color:white;padding:5px;font-weight: bolder;font-size:10px;text-align:right;vertical-align:middle">DEFINICIÓN</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Numero de Caja</td>
                <td style="text-align:right">{{columna1}}</td>
            </tr>
            <tr>
                <td>Dueño de Caja</td>
                <td style="text-align:right">{{columna2}}</td>
            </tr>
            <tr>
                <td>Usuario</td>
                <td style="text-align:right">{{columna3}}</td>
            </tr>
            <tr>
                <td>Consignatario</td>
                <td style="text-align:right">{{columna4}}</td>
            </tr>
            <tr>
                <td>Cliente</td>
                <td style="text-align:right">{{columna5}}</td>
            </tr>
            <tr>
                <td>{{origen_destino}}</td>
                <td style="text-align:right">{{columna6}}</td>
            </tr>
            <tr>
                <td>Sello</td>
                <td style="text-align:right">{{columna7}}</td>
            </tr>
            <tr>
                <td>Estado de Caja</td>
                <td style="text-align:right">{{columna8}}</td>
            </tr>
            <tr>
                <td>Tipo de Caja</td>
                <td style="text-align:right">{{columna9}}</td>
            </tr>
            <tr>
                <td>Notas</td>
                <td style="text-align:right;text-overflow: ellipsis">{{columna10}}</td>
            </tr>
            <tr>
                <td>Fecha</td>
                <td style="text-align:right">{{date_time_ingreso}}</td>
            </tr>
            <tr>
                <td>Cargada?</td>
                <td style="text-align:right">{{columna12}}</td>
            </tr>
            <tr>
                <td>Patio</td>
                <td style="text-align:right">{{columna13}}</td>
            </tr>
            <tr>
                <td>Transportista</td>
                <td style="text-align:right">{{columna14}}</td>
            </tr>
            <tr>
                <td>Numero de Tractor</td>
                <td style="text-align:right">{{columna15}}</td>
            </tr>
        </tbody>
    </table>
    <h5>{{causas}}</h5>
`;
global.tablaformurariotractor = `
<style>
td,th{
    width:50%!important;
}
</style>
<table style="width:200px;height:auto;max-width:300px;font-famili:Arial;font-size:10px;">
        <thead>
            <tr>
                <th style="background-color:black;color:white;padding:5px;font-weight: bolder;font-size:10px;text-align:left;vertical-align:middle">CONCEPTO</th>
                <th style="background-color:black;color:white;padding:5px;font-weight: bolder;font-size:10px;text-align:right;vertical-align:middle">DEFINICIÓN</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Transportista</td>
                <td style="text-align:right">{{columna1}}</td>
            </tr>
            <tr>
                <td>Numero de tractor</td>
                <td style="text-align:right">{{columna2}}</td>
            </tr>
            <tr>
                <td>Nombre del Operador</td>
                <td style="text-align:right">{{columna3}}</td>
            </tr>
            <tr>
                <td>Estado de la unidad</td>
                <td style="text-align:right">{{columna4}}</td>
            </tr>
            <tr>
                <td>Fecha</td>
                <td style="text-align:right">{{date_time_ingreso}}</td>
            </tr>
            <tr>
                <td>Cliente</td>
                <td style="text-align:right">{{columna6}}</td>
            </tr>
            <tr>
                <td>Usuario</td>
                <td style="text-align:right">{{columna7}}</td>
            </tr>
            <tr>
                <td>Patio</td>
                <td style="text-align:right">{{columna8}}</td>
            </tr>
            <tr>
                <td>{{origen_destino}}</td>
                <td style="text-align:right">{{columna9}}</td>
            </tr>
            <tr>
                <td>Notas</td>
                <td style="text-align:right;text-overflow: ellipsis">{{columna10}}</td>
            </tr>
        </tbody>
    </table>
    <h1>{{causas}}</h1>
`;
global.tablaformularioproveedor = `
<style>
td,th{
    width:50%!important;
}
</style>
<table style="width:200px;height:auto;max-width:300px;font-famili:Arial;font-size:10px;">
        <thead>
            <tr>
                <th style="background-color:black;color:white;padding:5px;font-weight: bolder;font-size:10px;text-align:left;vertical-align:middle">CONCEPTO</th>
                <th style="background-color:black;color:white;padding:5px;font-weight: bolder;font-size:10px;text-align:right;vertical-align:middle">DEFINICIÓN</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Nombre</td>
                <td style="text-align:right">{{columna1}}</td>
            </tr>
            <tr>
                <td>Empresa</td>
                <td style="text-align:right">{{columna2}}</td>
            </tr>
            <tr>
                <td>Asunto</td>
                <td style="text-align:right">{{columna3}}</td>
            </tr>
            <tr>
                <td>Gafete Asignado</td>
                <td style="text-align:right">{{columna4}}</td>
            </tr>
            <tr>
                <td>Quien captura?</td>
                <td style="text-align:right">{{columna5}}</td>
            </tr>
            <tr>
                <td>Patio</td>
                <td style="text-align:right">{{columna6}}</td>
            </tr>
            <tr>
                <td>Fecha</td>
                <td style="text-align:right">{{date_time_ingreso}}</td>
            </tr>
        </tbody>
    </table>
    <h1>{{causas}}</h1>
`;
global.tablaformulariovisitantes = `
<style>
td,th{
    width:50%!important;
}
</style>
<table style="width:200px;height:auto;max-width:300px;font-famili:Arial;font-size:10px;">
<thead>
<tr>
    <th style="background-color:black;color:white;padding:5px;font-weight: bolder;font-size:10px;text-align:left;vertical-align:middle">CONCEPTO</th>
    <th style="background-color:black;color:white;padding:5px;font-weight: bolder;font-size:10px;text-align:right;vertical-align:middle">DEFINICIÓN</th>
</tr>
</thead>
<tbody>
<tr>
    <td>Motivo de visita</td>
    <td style="text-align:right">{{columna2}}</td>
</tr>
<tr>
    <td>Nombre</td>
    <td style="text-align:right">{{columna3}}</td>
</tr>
<tr>
    <td>Patio</td>
    <td style="text-align:right">{{columna4}}</td>
</tr>
<tr>
    <td>Quien visita?</td>
    <td style="text-align:right">{{columna5}}</td>
</tr>
<tr>
    <td>Quien captura?</td>
    <td style="text-align:right">{{columna6}}</td>
</tr>
<tr>
    <td>Fecha</td>
    <td style="text-align:right">{{date_time_ingreso}}</td>
</tr>
</tbody>
</table>
<h1>{{causas}}</h1>
`;
global.tablaformularioincidencias = `
<style>
td,th{
    width:50%!important;
}
</style>
<table style="width:200px;height:auto;max-width:300px;font-famili:Arial;font-size:10px;">
<thead>
<tr>
    <th style="background-color:black;color:white;padding:5px;font-weight: bolder;font-size:10px;text-align:left;vertical-align:middle">CONCEPTO</th>
    <th style="background-color:black;color:white;padding:5px;font-weight: bolder;font-size:10px;text-align:right;vertical-align:middle">DEFINICIÓN</th>
</tr>
</thead>
<tbody>
<tr>
    <td>Detalle</td>
    <td style="text-align:right">{{columna1}}</td>
</tr>
<tr>
    <td>Notas</td>
    <td style="text-align:right">{{columna2}}</td>
</tr>
<tr>
    <td>Quien captura?</td>
    <td style="text-align:right">{{columna3}}</td>
</tr>
<tr>
    <td>Patio</td>
    <td style="text-align:right">{{columna4}}</td>
</tr>
<tr>
    <td>Fecha</td>
    <td style="text-align:right">{{date_time_ingreso}}</td>
</tr>
</tbody>
</table>
<h1>{{causas}}</h1>
`;
global.columnatitulo_columnavalue = `<tr>
<td style="line-height: 100%" class="conceptscolumnuno">
    <table>
        <tbody>
            <tr>
                <td class="font-9" style="width:50%"><b style="text-align:left">{{titulo}}</b></td>
                <td class="font-9" style="width:50%;text-align:right;text-overflow: ellipsis;">{{value}}</td>
            </tr>
        </tbody>
    </table> 
</td>                                    
</tr>`;
global.html_for_fotos = `<div style="width:16cm;height:20.90cm;margin:0!important;padding:0!important;line-height: 100%;">
    {{fotos}}
</div>`;
global.html_for_fotosm = `<div style="width:8cm;height:20.90cm;margin:0!important;padding:0!important;line-height: 100%;">
    {{fotos}}
</div>`;
global.fotosestruct = `
<div style="width:100%;">
    {{fotoind1}}
    {{fotoind2}}
    {{fotoind3}}
</div>
<div style="width:100%;">
    {{descind1}}
    {{descind2}}
    {{descind3}}
</div> 
{{fotos}} 
`;
global.fotoind = `
<img style="width:5cm;height:3.2cm!important;margin-top:10px;margin-left:6px" src="{{src}}" alt="{{alt}}"/>
`;
global.descind = `
<div class="font-9" style="display:inline-block;margin-left:5px;width:5cm;height:auto;text-align: center;vertical-align: middle;text-transform: uppercase;font-weight: bolder;text-overflow: ellipsis;">
    {{titulo}}
</div>
`;
global.fotoindm = `
<img style="width:29%;height:2cm!important;margin-top:10px;margin-left:6px" src="{{src}}" alt="{{alt}}"/>
`;
global.descindm = `
<div class="font-6" style="line-height:100%;display:inline-block;margin-left:6px;width:29%;height:auto;text-align: center;vertical-align: middle;text-transform: uppercase;font-weight: bolder;text-overflow: ellipsis;">
    {{titulo}}
</div>
`;
global.fotoopcional1 = `                
<table style="height: 75%;">
    <tbody>
        <tr>
            <td>
                <img style="width:5cm;height:4cm!important;" src="{{foto_fotoopcional1}}" alt="{{alt_opcional1}}"/>
            </td>
        </tr>
    </tbody>
    </table>
    <table style="height: 20%;">
    <tbody>
        <tr>
            <td class="font-9" style="text-align: center;vertical-align: middle;text-transform: uppercase;font-weight: bolder;text-overflow: ellipsis;">{{titulo_fotoopcional1}}</td>
        </tr>
    </tbody>
</table>`;
global.masfotosopcionales = `
<table style="height:25%">
    <tbody>
        <tr>
            <td style="width:33.33%;">
                {{foto_opcional_2}}
            </td>
            <td style="width:33.33%;">
                {{foto_opcional_3}}
            </td>
            <td style="width:33.33%;">

            </td>
        </tr>
    </tbody>
</table>`;
global.foto_opcional_2 = `
<table style="height: 75%;">
<tbody>
    <tr>
        <td>
            <img style="width:5cm;height:4cm!important;" src="{{foto_opcional2}}" alt="{{alt_opcional2}}"/>
        </td>
    </tr>
</tbody>
</table>
<table style="height: 20%;">
<tbody>
    <tr>
        <td class="font-9" style="text-align: center;vertical-align: middle;text-transform: uppercase;font-weight: bolder">{{titulo_opcional2}}</td>
    </tr>
</tbody>
</table>`;
global.foto_opcional_3 = `
<table style="height: 75%;">
<tbody>
    <tr>
        <td>
            <img style="width:5cm;height:4cm!important;" src="{{foto_opcional3}}" alt="{{alt_opcional3}}"/>
        </td>
    </tr>
</tbody>
</table>
<table style="height: 20%;">
<tbody>
    <tr>
        <td class="font-9" style="text-align: center;vertical-align: middle;text-transform: uppercase;font-weight: bolder">{{titulo_opcional3}}</td>
    </tr>
</tbody>
</table>`;
global.fotoopcionalio = `
<table style="height:25%">
    <tbody>
        <tr>
            <td style="width:33.33%;">
                <table style="height: 75%;">
                    <tbody>
                        <tr>
                            <td>
                                <img style="width:5cm;height:4cm!important;" src="{{foto_opcional}}" alt="{{alt_opcional}}"/>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table style="height: 20%;">
                    <tbody>
                        <tr>
                            <td style="text-align: center;vertical-align: middle;text-transform: uppercase;font-weight: bolder">{{titulo_opcional}}</td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td style="width:33.33%;"></td>
            <td style="width:33.33%;"></td>
        </tr>
    </tbody>
</table>`;
global.fotorondin = `<img class="foto" src="{{foto}}" alt="{{titulo}}">`;