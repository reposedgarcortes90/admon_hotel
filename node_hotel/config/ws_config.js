global.bodyParser = require('body-parser');
global.express = require('express');
global.http = require("http");
global.app = express();
global.port=32000;
global.moment=require('moment');
global.md5 = require('md5');
global.pdf = require('html-pdf');
global.fs  = require("fs");
global.request = require('request');
global.uuidv1 = require('uuid/v1');
global.nodemailer = require('nodemailer');
global.asyncLoop = require('node-async-loop');
global.fileUpload = require('express-fileupload');
global.path = require("path");
global.correo_datos = {
    host: "mail.dvtrace.com",
    port: 587,
    auth: {
        user: 'edgar@dvtrace.com',
        pass: '.Ed$gAr2017'
    }
};
global.smtpTransport = require('nodemailer-smtp-transport');
global.nombres_fotosc = [];
nombres_fotosc["foto_opcional"] = "Opcional";
nombres_fotosc["opcional_uno_foto"] = "Opcional";
nombres_fotosc["opcional_dos_foto"] = "Opcional";
nombres_fotosc["lado_frontal_foto"] = "Lado Frontal";
nombres_fotosc["lado_izquierdo_foto"] = "Lado Izquierdo";
nombres_fotosc["llanta_izquierda_uno_foto"] = "Llanta 1 (lado izquierdo)";
nombres_fotosc["llanta_izquierda_dos_foto"] = "Llanta 2 (lado izquierdo)";
nombres_fotosc["llanta_izquierda_cinco_foto"] = "Llanta 5 (lado izquierdo)";
nombres_fotosc["llanta_izquierda_seis_foto"] = "Llanta 6 (lado izquierdo)";
nombres_fotosc["lado_trasero_foto"] = "Lado trasero";
nombres_fotosc["lado_derecho_foto"] = "Lado derecho";
nombres_fotosc["llanta_derecha_cuatro_foto"] = "Llanta 4 (lado derecho)";
nombres_fotosc["llanta_derecha_tres_foto"] = "Llanta 3 (lado derecho)";
nombres_fotosc["llanta_derecha_siete_foto"] = "Llanta 7 (lado derecho)";
nombres_fotosc["llanta_derecha_ocho_foto"] = "Llanta 8 (lado derecho)";
nombres_fotosc["llanta_derecha_ocho_foto"] = "Llanta 8 (lado derecho)";
nombres_fotosc["llanta_derecha_ocho_foto"] = "Llanta 8 (lado derecho)";
nombres_fotosc["techo_foto"] = "Arriba";
nombres_fotosc["piso_foto"] = "Abajo";
nombres_fotosc["techo_piso_foto"] = "Arriba y Abajo";
nombres_fotosc["licencia_conducir_foto"] = "Licencia de conducir";
global.nombres_fotost = [];  
nombres_fotost["foto_opcional"] = "Opcional";
nombres_fotost["opcional_uno_foto"] = "Opcional";
nombres_fotost["opcional_dos_foto"] = "Opcional";
nombres_fotost["lado_frontal_foto"] = "Lado Frontal";
nombres_fotost["lado_izquierdo_foto"] = "Lado Izquierdo";
nombres_fotost["llanta_derecha_dos_foto"] = "Llanta 2 (lado derecho)";
nombres_fotost["llanta_derecha_cinco_foto"] = "Llanta 5 (lado derecho)";
nombres_fotost["llanta_derecha_seis_foto"] = "Llanta 6 (lado derecho)";
nombres_fotost["llanta_derecha_ocho_foto"] = "Llanta 8 (lado derecho)";
nombres_fotost["llanta_derecha_diez_foto"] = "Llanta 10 (lado derecho)";
nombres_fotost["lado_trasero_foto"] = "Lado trasero";
nombres_fotost["lado_derecho_foto"] = "Lado derecho";
nombres_fotost["llanta_izquierda_uno_foto"] = "Llanta 1 (lado izquierdo)";
nombres_fotost["llanta_izquierda_tres_foto"] = "Llanta 3 (lado izquierdo)";
nombres_fotost["llanta_izquierda_cuatro_foto"] = "Llanta 4 (lado izquierdo)";
nombres_fotost["llanta_izquierda_siete_foto"] = "Llanta 7 (lado izquierdo)";
nombres_fotost["llanta_izquierda_nueve_foto"] = "Llanta 9 (lado izquierdo)";
nombres_fotost["interior_foto"] = "Interior";
nombres_fotost["licencia_conducir_foto"] = "Licencia de conducir";

