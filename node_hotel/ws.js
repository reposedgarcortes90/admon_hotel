require('./config/mysql_configlocal.js');
require('./config/ws_configlocal.js');
require('./config/paths_configlocal.js');
require('./config/grid_fotos_config.js');
app.listen(port, function () {
    console.log('Server listening on port '+port+' :D');
});
app.use(fileUpload());
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/', function (req, res) {
    res.header("viewport", "width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0");
    res.status(200).send('<img src="http://cdn.bulbagarden.net/upload/thumb/3/39/007Squirtle.png/250px-007Squirtle.png" alt="Este es un Vamo a calmarno" /> <br />Vamo a empezar con esto! ADMINISTRACION DE HOTEL');
    res.end();
});
app.get("/get_uuid", function(req, res){
    res.status(200).send(uuidv1().toUpperCase());
    res.end();
});
app.all("/usuarios_hotel",function(req,res){
    var datos_llegan;
    var modulo = "usuarios";
    if (req.method.toUpperCase()=="POST"){
		datos_llegan=req.body;
	}
	if(req.method.toUpperCase()=="GET"){
		datos_llegan=req.query;
    }
    var instruccion = "leer";
    if(req.query){
        instruccion = req.query.instruccion;
        delete datos_llegan.instruccion;
    }
    switch(instruccion){
        case 'leer':
            var eliminado = 0;
            if(req.query.eliminado) eliminado = req.query.eliminado;
            connection.query("SELECT * FROM usuarios WHERE eliminado = ? order by fechacrea desc",[eliminado],function(error,data){
                if(error){
                    console.log(error);
                    res.status(200).send("ERROR");
                    res.end();
                }else{
                    data.forEach(element => {
                        element.fechacrea = moment(Number(element.fechacrea) * 1000).format("YYYY-MM-DD HH:mm:ss")
                    });
                    res.status(200).send(data);
                    res.end();
                }
            });
        break;
        case 'agregar':
            datos_llegan.eliminado=0;
            datos_llegan.fechacrea = moment().unix();
            connection.query("SELECT * FROM usuarios WHERE nombreusuario = ? AND eliminado = ?",[datos_llegan.nombreusuario,0],function(error,data){
                if(error){
                    console.log(error);
                    var data =  {};
                    data.error= error;
                    data.in = datos_llegan;
                    guardarHistoria(datos_llegan.usuariocrea,"error",modulo,data,modulo,function(val){
                        console.log(val);
                        res.status(200).send("ERROR EN DATOS");
                        res.end();
                    });
                }else{
                    if(data.length == 0){
                        connection.query("INSERT INTO usuarios SET ?",[datos_llegan],function(error,data){
                            if(error){
                                console.log(error)
                                var data =  {};
                                data.error= error;
                                data.in = datos_llegan;
                                guardarHistoria(datos_llegan.usuarioedita,"error",modulo,data,modulo,function(val){
                                    console.log(val);
                                    res.status(200).send("ERROR");
                                    res.end();
                                });
                            }else{
                                guardarHistoria(datos_llegan.usuariocrea,"agregar",modulo,datos_llegan,"insert",function(val){
                                    console.log(val);
                                    res.status(200).send(data);
                                    res.end();
                                });
                            }
                        });
                    }else{
                        var data =  {};
                        data.error= "DUPLICADO";
                        data.in = datos_llegan;
                        guardarHistoria(datos_llegan.usuariocrea,"error",modulo,data,modulo,function(val){
                            console.log(val);
                            res.status(200).send("Registro Duplicado!!");
                            res.end();
                        });
                    }
                }
            });
            
        break;
        case 'actualizar':
            var itemid = datos_llegan.itemid;
            delete datos_llegan.itemid;
            datos_llegan.fechaedita = moment().unix();
            console.log(datos_llegan,itemid);
            connection.query("UPDATE usuarios Set ? WHERE itemid = ?",[datos_llegan,itemid],function(error,data){
                if(error){
                    console.log(error);
                    var data =  {};
                    data.error= error;
                    data.in = datos_llegan;
                    guardarHistoria(datos_llegan.usuarioedita,"error",modulo,data,modulo,function(val){
                        console.log(val);
                        res.status(200).send("ERROR");
                        res.end();
                    });
                }else{
                    guardarHistoria(datos_llegan.usuarioedita,"actualizar",modulo,datos_llegan,"update",function(val){
                        console.log(val);
                        res.status(200).send(data);
                        res.end();
                    });
                }
            });
        break;
        case 'eliminar':
            var itemid = datos_llegan.itemid;
            delete datos_llegan.itemid;
            datos_llegan.fechaelimina = moment().unix();
            connection.query("UPDATE usuarios Set ? WHERE itemid = ?",[datos_llegan,itemid],function(error,data){
                if(error){
                    console.log(error);
                    console.log(error);
                    var data =  {};
                    data.error= error;
                    data.in = datos_llegan;
                    guardarHistoria(datos_llegan.usuarioelimina,"error",modulo,data,modulo,function(val){
                        console.log(val);
                        res.status(200).send("ERROR");
                        res.end();
                    });
                }else{
                    guardarHistoria(datos_llegan.usuarioelimina,"eliminar",modulo,datos_llegan,"delete",function(val){
                        console.log(val);
                        res.status(200).send(data);
                        res.end();
                    });
                }
            });
        break;
    }
});
app.all("/empleados_hotel",function(req,res){
    var datos_llegan;
    var modulo = "empleados";
    if (req.method.toUpperCase()=="POST"){
		datos_llegan=req.body;
	}
	if(req.method.toUpperCase()=="GET"){
		datos_llegan=req.query;
    }
    var instruccion = "leer";
    if(req.query){
        instruccion = req.query.instruccion;
        delete datos_llegan.instruccion;
    }
    switch(instruccion){
        case 'leer':
            var activo = 1;
            if(req.query.activo) activo = req.query.activo;
            connection.query("SELECT * FROM empleados WHERE activo = ? order by fechacrea desc",[activo],function(error,data){
                if(error){
                    console.log(error);
                    res.status(200).send("ERROR");
                    res.end();
                }else{
                    data.forEach(element => {
                        element.fechacrea = moment(Number(element.fechacrea) * 1000).format("YYYY-MM-DD HH:mm:ss")
                    });
                    res.status(200).send(data);
                    res.end();
                }
            });
        break;
        case 'agregar':
            datos_llegan.activo=1;
            datos_llegan.fechacrea = moment().unix();
            connection.query("SELECT * FROM empleados WHERE rfc = ? AND eliminado = ?",[datos_llegan.rfc,0],function(error,data){
                if(error){
                    console.log(error);
                    var data =  {};
                    data.error= error;
                    data.in = datos_llegan;
                    guardarHistoria(datos_llegan.usuariocrea,"error",modulo,data,modulo,function(val){
                        console.log(val);
                        res.status(200).send("ERROR EN DATOS");
                        res.end();
                    });
                }else{
                    if(data.length == 0){
                        connection.query("INSERT INTO empleados SET ?",[datos_llegan],function(error,data){
                            if(error){
                                console.log(error);
                                var data =  {};
                                data.error= error;
                                data.in = datos_llegan;
                                guardarHistoria(datos_llegan.usuariocrea,"error",modulo,data,modulo,function(val){
                                    console.log(val);
                                    res.status(200).send("ERROR AL INSERTAR");
                                    res.end();
                                });
                            }else{
                                guardarHistoria(datos_llegan.usuariocrea,"agregar",modulo,datos_llegan,"insert",function(val){
                                    console.log(val);
                                    res.status(200).send(data);
                                    res.end();
                                });
                            }
                        });
                    }else{
                        var data =  {};
                        data.error= "DUPLICADO";
                        data.in = datos_llegan;
                        guardarHistoria(datos_llegan.usuariocrea,"error",modulo,data,modulo,function(val){
                            console.log(val);
                            res.status(200).send("Registro Duplicado!!");
                            res.end();
                        });
                    }
                }
            });
        break;
        case 'actualizar':
            var itemid = datos_llegan.itemid;
            delete datos_llegan.itemid;
            datos_llegan.fechaedita = moment().unix();
            console.log(datos_llegan,itemid);
            connection.query("UPDATE empleados Set ? WHERE itemid = ?",[datos_llegan,itemid],function(error,data){
                if(error){
                    console.log(error);
                    var data =  {};
                    data.error= error;
                    data.in = datos_llegan;
                    guardarHistoria(datos_llegan.usuarioedita,"error",modulo,data,modulo,function(val){
                        console.log(val);
                        res.status(200).send("ERROR");
                        res.end();
                    });
                }else{
                    guardarHistoria(datos_llegan.usuarioedita,"actualizar",modulo,datos_llegan,"update",function(val){
                        console.log(val);
                        res.status(200).send(data);
                        res.end();
                    });
                }
            });
        break;
        case 'eliminar':
            var itemid = datos_llegan.itemid;
            delete datos_llegan.itemid;
            datos_llegan.fechaelimina = moment().unix();
            connection.query("UPDATE empleados Set ? WHERE itemid = ?",[datos_llegan,itemid],function(error,data){
                if(error){
                    console.log(error);
                    var data =  {};
                    data.error= error;
                    data.in = datos_llegan;
                    guardarHistoria(datos_llegan.usuarioelimina,"error",modulo,data,modulo,function(val){
                        console.log(val);
                        res.status(200).send("ERROR");
                        res.end();
                    });
                }else{
                    guardarHistoria(datos_llegan.usuarioelimina,"eliminar",modulo,datos_llegan,"delete",function(val){
                        console.log(val);
                        res.status(200).send(data);
                        res.end();
                    });
                }
            });
        break;
    }
});
app.all("/clientes_hotel",function(req,res){
    var datos_llegan;
    var modulo = "clientes";
    if (req.method.toUpperCase()=="POST"){
		datos_llegan=req.body;
	}
	if(req.method.toUpperCase()=="GET"){
		datos_llegan=req.query;
    }
    var instruccion = "leer";
    if(req.query){
        instruccion = req.query.instruccion;
        delete datos_llegan.instruccion;
    }
    switch(instruccion){
        case 'leer':
            var eliminado = 0;
            var activo = 1;
            if(req.query.eliminado) eliminado = req.query.eliminado;
            if(req.query.activo) activo = req.query.activo;
            connection.query("SELECT * FROM clientes WHERE eliminado = ? order by fechacrea desc",[eliminado],function(error,data){
                if(error){
                    console.log(error);
                    res.status(200).send("ERROR");
                    res.end();
                }else{
                    res.status(200).send(data);
                    res.end();
                }
            });
        break;
        case 'agregar':
            datos_llegan.eliminado=0;
            datos_llegan.fechacrea = moment().unix();
            connection.query("SELECT * FROM clientes WHERE rfc = ? AND eliminado = ?",[datos_llegan.rfc,0],function(error,data){
                if(error){
                    console.log(error);
                    var data =  {};
                    data.error= error;
                    data.in = datos_llegan;
                    guardarHistoria(datos_llegan.usuariocrea,"error",modulo,data,modulo,function(val){
                        console.log(val);
                        res.status(200).send("ERROR EN DATOS");
                        res.end();
                    });
                }else{
                    if(data.length == 0){
                        connection.query("INSERT INTO clientes SET ?",[datos_llegan],function(error,data){
                            if(error){
                                console.log(error);
                                var data =  {};
                                data.error= error;
                                data.in = datos_llegan;
                                guardarHistoria(datos_llegan.usuariocrea,"error",modulo,data,modulo,function(val){
                                    console.log(val);
                                    res.status(200).send("ERROR");
                                    res.end();
                                });
                            }else{
                                guardarHistoria(datos_llegan.usuariocrea,"agregar",modulo,datos_llegan,"insert",function(val){
                                    console.log(val);
                                    res.status(200).send(data);
                                    res.end();
                                });
                            }
                        });
                    }else{
                        var data =  {};
                        data.error= "DUPLICADO";
                        data.in = datos_llegan;
                        guardarHistoria(datos_llegan.usuariocrea,"error",modulo,data,modulo,function(val){
                            console.log(val);
                            res.status(200).send("Registro Duplicado!!");
                            res.end();
                        });
                    }
                }
            });
        break;
        case 'actualizar':
            var itemid = datos_llegan.itemid;
            delete datos_llegan.itemid;
            datos_llegan.fechaedita = moment().unix();
            console.log(datos_llegan);
            connection.query("UPDATE clientes Set ? WHERE itemid = ?",[datos_llegan,itemid],function(error,data){
                if(error){
                    console.log(error);
                    var data =  {};
                    data.error= error;
                    data.in = datos_llegan;
                    guardarHistoria(datos_llegan.usuarioedita,"error",modulo,data,modulo,function(val){
                        console.log(val);
                        res.status(200).send("ERROR");
                        res.end();
                    });
                }else{
                    guardarHistoria(datos_llegan.usuarioedita,"actualizar",modulo,datos_llegan,"update",function(val){
                        console.log(val);
                        res.status(200).send(data);
                        res.end();
                    });
                }
            });
        break;
        case 'eliminar':
            var itemid = datos_llegan.itemid;
            delete datos_llegan.itemid;
            datos_llegan.fechaelimina = moment().unix();
            connection.query("UPDATE clientes Set ? WHERE itemid = ?",[{eliminado:1,fechaelimina:datos_llegan.fechaelimina,usuarioelimina:datos_llegan.usuarioelimina},itemid],function(error,data){
                if(error){
                    console.log(error);
                    var data =  {};
                    data.error= error;
                    data.in = datos_llegan;
                    guardarHistoria(datos_llegan.usuarioelimina,"error",modulo,data,modulo,function(val){
                        console.log(val);
                        res.status(200).send("ERROR");
                        res.end();
                    });
                }else{
                    guardarHistoria(datos_llegan.usuarioelimina,"eliminar",modulo,datos_llegan,"delete",function(val){
                        console.log(val);
                        res.status(200).send(data);
                        res.end();
                    });
                }
            });
        break;
    }
});
app.all("/tiposhabitaciones_hotel",function(req,res){
    var datos_llegan;
    var modulo = "TiposHabitaciones";
    if (req.method.toUpperCase()=="POST"){
		datos_llegan=req.body;
	}
	if(req.method.toUpperCase()=="GET"){
		datos_llegan=req.query;
    }
    var instruccion = "leer";
    if(req.query){
        instruccion = req.query.instruccion;
        delete datos_llegan.instruccion;
    }
    // console.trace(datos_llegan)
    switch(instruccion){
        case 'get_combo_data':
            var option = {};            
            connection.query("Select itemid AS value, nombre AS text From tipo_habitaciones Where  eliminado = ? order by fechacrea desc",[0],function(err,catalogo){//client_id = ? And
                if(err){
                    console.log(err,"ERROR")
                    res.status(200).send("ERROR");
                    res.end();
                }else{
                    // console.log("catalogo:", catalogo);
                    if(catalogo.length==0){
                        res.status(200).send([]);
                        res.end();
                    }
                    else{
                        var newcat = [];
                        newcat.push({value:"",text:"ND",selected:true})
                        catalogo.forEach((e,i)=>{
                            newcat.push(e)
                        });
                        // catalogo[0].selected = true;
                        option.options = (newcat);
                        res.status(200).send(JSON.stringify(option));
                        res.end();
                    }
                }
            });
        break;
        case 'leer':
            var eliminado = 0;
            var activo = 1;
            if(req.query.eliminado) eliminado = req.query.eliminado;
            if(req.query.activo) activo = req.query.activo;
            connection.query("SELECT * FROM tipo_habitaciones WHERE eliminado = ?",[eliminado],function(error,data){
                if(error){
                    console.log(error);
                    res.status(200).send("ERROR");
                    res.end();
                }else{
                    res.status(200).send(data);
                    res.end();
                }
            });
        break;
        case 'agregar':
            datos_llegan.eliminado=0;
            datos_llegan.fechacrea = moment().unix();
            connection.query("INSERT INTO tipo_habitaciones SET ?",[datos_llegan],function(error,data){
                if(error){
                    console.log(error);
                    var data =  {};
                    data.error= error;
                    data.in = datos_llegan;
                    guardarHistoria(datos_llegan.usuariocrea,"error",modulo,data,modulo,function(val){
                        console.log(val);
                        res.status(200).send("ERROR");
                        res.end();
                    });
                }else{
                    guardarHistoria(datos_llegan.usuariocrea,"agregar",modulo,datos_llegan,"insert",function(val){
                        console.log(val);
                        res.status(200).send(data);
                        res.end();
                    });
                }
            });
        break;
        case 'actualizar':
            var itemid = datos_llegan.itemid;
            delete datos_llegan.itemid;
            datos_llegan.fechaedita = moment().unix();
            console.log(datos_llegan);
            connection.query("UPDATE tipo_habitaciones Set ? WHERE itemid = ?",[datos_llegan,itemid],function(error,data){
                if(error){
                    console.log(error);
                    var data =  {};
                    data.error= error;
                    data.in = datos_llegan;
                    guardarHistoria(datos_llegan.usuarioedita,"error",modulo,data,modulo,function(val){
                        console.log(val);
                        res.status(200).send("ERROR");
                        res.end();
                    });
                }else{
                    guardarHistoria(datos_llegan.usuarioedita,"actualizar",modulo,datos_llegan,"update",function(val){
                        console.log(val);
                        res.status(200).send(data);
                        res.end();
                    });
                }
            });
        break;
        case 'eliminar':
            var itemid = datos_llegan.itemid;
            delete datos_llegan.itemid;
            datos_llegan.fechaelimina = moment().unix();
            connection.query("UPDATE tipo_habitaciones Set ? WHERE itemid = ?",[{eliminado:1,fechaelimina:datos_llegan.fechaelimina,usuarioelimina:datos_llegan.usuarioelimina},itemid],function(error,data){
                if(error){
                    console.log(error);
                    var data =  {};
                    data.error= error;
                    data.in = datos_llegan;
                    guardarHistoria(datos_llegan.usuarioelimina,"error",modulo,data,modulo,function(val){
                        console.log(val);
                        res.status(200).send("ERROR");
                        res.end();
                    });
                }else{
                    guardarHistoria(datos_llegan.usuarioelimina,"eliminar",modulo,datos_llegan,"delete",function(val){
                        console.log(val);
                        res.status(200).send(data);
                        res.end();
                    });
                }
            });
        break;
    }
});

app.all("/habitaciones_hotel",function(req,res){
    var datos_llegan;
    var modulo = "habitaciones";
    if (req.method.toUpperCase()=="POST"){
		datos_llegan=req.body;
	}
	if(req.method.toUpperCase()=="GET"){
		datos_llegan=req.query;
    }
    var instruccion = "leer";
    if(req.query){
        instruccion = req.query.instruccion;
        delete datos_llegan.instruccion;
    }
    switch(instruccion){

        case 'leer':
            var eliminado = 0;
            var activo = 1;
            if(req.query.eliminado) eliminado = req.query.eliminado;
            if(req.query.activo) activo = req.query.activo;
            connection.query("SELECT habitaciones.*,tipo_habitaciones.nombre AS tipohabitaciontext FROM habitaciones LEFT JOIN tipo_habitaciones ON habitaciones.tipohabitacion = tipo_habitaciones.itemid WHERE habitaciones.eliminado = ? order by fechacrea desc",[eliminado],function(error,data){
                if(error){
                    console.log(error);
                    res.status(200).send("ERROR");
                    res.end();
                }else{
                    res.status(200).send(data);
                    res.end();
                }
            });
        break;
        case 'agregar':
            datos_llegan.eliminado=0;
            if(datos_llegan.activo) delete datos_llegan.activo;
            datos_llegan.fechacrea = moment().unix();
            connection.query("INSERT INTO habitaciones SET ?",[datos_llegan],function(error,data){
                if(error){
                    console.log(error);
                    var data =  {};
                    data.error= error;
                    data.in = datos_llegan;
                    guardarHistoria(datos_llegan.usuariocrea,"error",modulo,data,modulo,function(val){
                        console.log(val);
                        res.status(200).send("ERROR");
                        res.end();
                    });
                }else{
                    guardarHistoria(datos_llegan.usuariocrea,"agregar",modulo,datos_llegan,"insert",function(val){
                        console.log(val);
                        res.status(200).send(data);
                        res.end();
                    });
                }
            });
        break;
        case 'actualizar':
            var itemid = datos_llegan.itemid;
            delete datos_llegan.itemid;
            datos_llegan.fechaedita = moment().unix();
            console.log(datos_llegan);
            connection.query("UPDATE habitaciones Set ? WHERE itemid = ?",[datos_llegan,itemid],function(error,data){
                if(error){
                    console.log(error);
                    var data =  {};
                    data.error= error;
                    data.in = datos_llegan;
                    guardarHistoria(datos_llegan.usuarioedita,"error",modulo,data,modulo,function(val){
                        console.log(val);
                        res.status(200).send("ERROR");
                        res.end();
                    });
                }else{
                    guardarHistoria(datos_llegan.usuarioedita,"actualizar",modulo,datos_llegan,"update",function(val){
                        console.log(val);
                        res.status(200).send(data);
                        res.end();
                    });
                }
            });
        break;
        case 'eliminar':
            var itemid = datos_llegan.itemid;
            delete datos_llegan.itemid;
            datos_llegan.fechaelimina = moment().unix();
            connection.query("UPDATE habitaciones Set ? WHERE itemid = ?",[{eliminado:1,fechaelimina:datos_llegan.fechaelimina,usuarioelimina:datos_llegan.usuarioelimina},itemid],function(error,data){
                if(error){
                    console.log(error);
                    var data =  {};
                    data.error= error;
                    data.in = datos_llegan;
                    guardarHistoria(datos_llegan.usuarioelimina,"error",modulo,data,modulo,function(val){
                        console.log(val);
                        res.status(200).send("ERROR");
                        res.end();
                    });
                }else{
                    guardarHistoria(datos_llegan.usuarioelimina,"eliminar",modulo,datos_llegan,"delete",function(val){
                        console.log(val);
                        res.status(200).send(data);
                        res.end();
                    });
                }
            });
        break;
    }
});
app.all("/empresas_hotel",function(req,res){
    var datos_llegan;
    var modulo = "empresas";
    if (req.method.toUpperCase()=="POST"){
		datos_llegan=req.body;
	}
	if(req.method.toUpperCase()=="GET"){
		datos_llegan=req.query;
    }
    var instruccion = "leer";
    if(req.query){
        instruccion = req.query.instruccion;
        delete datos_llegan.instruccion;
    }
    switch(instruccion){
        case 'leer':
            var eliminado = 0;
            var activo = 1;
            if(req.query.eliminado) eliminado = req.query.eliminado;
            if(req.query.activo) activo = req.query.activo;
            connection.query("SELECT * FROM empresa WHERE eliminado = ? order by fechacrea desc",[eliminado],function(error,data){
                if(error){
                    console.log(error);
                    res.status(200).send("ERROR");
                    res.end();
                }else{
                    res.status(200).send(data);
                    res.end();
                }
            });
        break;
        case 'agregar':
            datos_llegan.eliminado=0;
            datos_llegan.fechacrea = moment().unix();
            connection.query("SELECT * FROM empresa WHERE rfc = ? AND eliminado = ?",[datos_llegan.rfc,0],function(error,data){
                if(error){
                    console.log(error);
                    var data =  {};
                    data.error= error;
                    data.in = datos_llegan;
                    guardarHistoria(datos_llegan.usuariocrea,"error",modulo,data,modulo,function(val){
                        console.log(val);
                        res.status(200).send("ERROR EN DATOS");
                        res.end();
                    });
                }else{
                    if(data.length == 0){
                        connection.query("INSERT INTO empresa SET ?",[datos_llegan],function(error,data){
                            if(error){
                                console.log(error);
                                var data =  {};
                                data.error= error;
                                data.in = datos_llegan;
                                guardarHistoria(datos_llegan.usuariocrea,"error",modulo,data,modulo,function(val){
                                    console.log(val);
                                    res.status(200).send("ERROR");
                                    res.end();
                                });
                            }else{
                                guardarHistoria(datos_llegan.usuariocrea,"agregar",modulo,datos_llegan,"insert",function(val){
                                    console.log(val);
                                    res.status(200).send(data);
                                    res.end();
                                });
                            }
                        });
                    }else{
                        var data =  {};
                        data.error= "DUPLICADO";
                        data.in = datos_llegan;
                        guardarHistoria(datos_llegan.usuariocrea,"error",modulo,data,modulo,function(val){
                            console.log(val);
                            res.status(200).send("Registro Duplicado!!");
                            res.end();
                        });
                    }
                }
            });
            
        break;
        case 'actualizar':
            var itemid = datos_llegan.itemid;
            delete datos_llegan.itemid;
            datos_llegan.fechaedita = moment().unix();
            connection.query("UPDATE empresa Set ? WHERE itemid = ?",[datos_llegan,itemid],function(error,data){
                if(error){
                    console.log(error);
                    var data =  {};
                    data.error= error;
                    data.in = datos_llegan;
                    guardarHistoria(datos_llegan.usuarioedita,"error",modulo,data,modulo,function(val){
                        console.log(val);
                        res.status(200).send("ERROR");
                        res.end();
                    });
                }else{
                    guardarHistoria(datos_llegan.usuarioedita,"actualizar",modulo,datos_llegan,"update",function(val){
                        console.log(val);
                        res.status(200).send(data);
                        res.end();
                    });
                }
            });
        break;
        case 'eliminar':
            var itemid = datos_llegan.itemid;
            delete datos_llegan.itemid;
            datos_llegan.fechaelimina = moment().unix();
            connection.query("UPDATE empresa Set ? WHERE itemid = ?",[{eliminado:1,fechaelimina:datos_llegan.fechaelimina,usuarioelimina:datos_llegan.usuarioelimina},itemid],function(error,data){
                if(error){
                    console.log(error);
                    var data =  {};
                    data.error= error;
                    data.in = datos_llegan;
                    guardarHistoria(datos_llegan.usuarioelimina,"error",modulo,data,modulo,function(val){
                        console.log(val);
                        res.status(200).send("ERROR");
                        res.end();
                    });
                }else{
                    guardarHistoria(datos_llegan.usuarioelimina,"eliminar",modulo,datos_llegan,"delete",function(val){
                        console.log(val);
                        res.status(200).send(data);
                        res.end();
                    });
                }
            });
        break;
    }
});
app.all("/historial_hotel",function(req,res){
    var datos_llegan;
    var modulo = "Historial";
    if (req.method.toUpperCase()=="POST"){
		datos_llegan=req.body;
	}
	if(req.method.toUpperCase()=="GET"){
		datos_llegan=req.query;
    }
    var instruccion = "leer";
    if(req.query){
        instruccion = req.query.instruccion;
        delete datos_llegan.instruccion;
    }
    switch(instruccion){
        case 'leer':
            connection.query("SELECT * FROM historial WHERE accion <> 'error' order by fechacrea desc",function(error,data){
                if(error){
                    console.log(error);
                    res.status(200).send("ERROR");
                    res.end();
                }else{
                    res.status(200).send(data);
                    res.end();
                }
            });
        break;
        default:
            res.status(200).send("OPCION NO CONTROLADA");
            res.end();
        break;
    }
});
function cambiarStatusHabitacion(habitacion,status,usuario,callback){
    connection.query("UPDATE habitaciones Set ? WHERE itemid = ?",[{ocupada:status},habitacion],function(error,data){
        if(error){
            console.log(error);
            var data =  {};
            data.error= error;
            data.in = {ocupada:status};
            guardarHistoria(usuario,"error",modulo,data,"Habitaciones",function(val){
                callback(error);
            });
        }else{
            guardarHistoria(usuario,"actualizar","Habitaciones",{ocupada:status},"update",function(val){
                callback(data);
            });
        }
    });
}
app.all("/estados_municipios",function(req,res){
    var datos_llegan;
    if (req.method.toUpperCase()=="POST"){
		datos_llegan=req.body;
	}
	if(req.method.toUpperCase()=="GET"){
		datos_llegan=req.query;
    }
    var instruccion = "ventas";
    if(req.query){
        instruccion = req.query.instruccion;
        delete datos_llegan.instruccion;
    }
    switch(instruccion){
        case 'getall':
            connection.query("SELECT * FROM estados",function(error,estados){
                if(error){
                    res.status(200).send("ERROR");
                    res.end();
                }else{
                    connection.query("SELECT * FROM municipios",function(error,municipios){
                        if(error){
                            res.status(200).send("ERROR");
                            res.end();
                        }else{
                            connection.query("SELECT estados.id AS id_estado, estados.estado AS nombreestado,municipios.id AS id_municipio, municipios.municipio AS nombremunicipio FROM hotel.estados_municipios inner join municipios on estados_municipios.municipios_id = municipios.id inner join estados on estados_municipios.estados_id = estados.id;",function(error,estados_municipios){
                                if(error){
                                    res.status(200).send("ERROR");
                                    res.end();
                                }else{
                                    var data = {};
                                    data.estados = estados;
                                    data.municipios = municipios;
                                    data.estados_municipios = estados_municipios
                                    res.status(200).send(data);
                                    res.end();
                                }
                            });
                        }
                    });
                }
            });
        break;
    }
});
app.all("/obtener_graficas",function(req,res){
    var datos_llegan;
    if (req.method.toUpperCase()=="POST"){
		datos_llegan=req.body;
	}
	if(req.method.toUpperCase()=="GET"){
		datos_llegan=req.query;
    }
    var instruccion = "ventas";
    if(req.query){
        instruccion = req.query.instruccion;
        delete datos_llegan.instruccion;
    }
    switch(instruccion){
        case 'ventas':
            connection.query("SELECT SUM(grantotal) AS sales,left(fechahospedaje,4) AS year FROM hotel.hospedajes group by left(fechahospedaje,4) order by left(fechahospedaje,4) asc",function(error,data){
                if(error){
                    console.log(error);
                    res.status(200).send("ERROR");
                    res.end();
                }else{
                    res.status(200).send(data);
                    res.end();
                }
            });
        break;
    }
});
app.all("/hospedajes_hotel",function(req,res){
    var datos_llegan;
    var modulo = "Hospedajes";
    if (req.method.toUpperCase()=="POST"){
		datos_llegan=req.body;
	}
	if(req.method.toUpperCase()=="GET"){
		datos_llegan=req.query;
    }
    var instruccion = "leer";
    if(req.query){
        instruccion = req.query.instruccion;
        delete datos_llegan.instruccion;
    }
    switch(instruccion){
        case 'leer':
            var eliminado = 0;
            var activo = 1;
            if(req.query.eliminado) eliminado = req.query.eliminado;
            if(req.query.activo) activo = req.query.activo;
            connection.query("SELECT * FROM hospedajes WHERE eliminado = ? and activo = ? order by fechacrea desc",[eliminado,activo],function(error,data){
                if(error){
                    console.log(error);
                    res.status(200).send("ERROR");
                    res.end();
                }else{
                    res.status(200).send(data);
                    res.end();
                }
            });
        break;
        case 'agregar':
            datos_llegan.eliminado=0;
            datos_llegan.activo=1;
            datos_llegan.fechacrea = moment().unix();
            connection.query("SELECT * FROM hospedajes WHERE rfc = ? AND eliminado = ?",[datos_llegan.rfc,0],function(error,data){
                if(error){
                    console.log(error,"ERROR");
                    var data =  {};
                    data.error= error;
                    data.in = datos_llegan;
                    guardarHistoria(datos_llegan.usuariocrea,"error",modulo,data,modulo,function(val){
                        console.log(val);
                        res.status(200).send("ERROR EN DATOS");
                        res.end();
                    });
                }else{
                    if(data.length == 0){
                        cambiarStatusHabitacion(datos_llegan.habitacion,1,datos_llegan.usuariocrea,function(listo){
                            console.log(listo,"INSERTO");
                            connection.query("INSERT INTO hospedajes SET ?",[datos_llegan],function(error,data){
                                if(error){
                                    console.log(error);
                                    var data =  {};
                                    data.error= error;
                                    data.in = datos_llegan;
                                    guardarHistoria(datos_llegan.usuariocrea,"error",modulo,data,modulo,function(val){
                                        console.log(val);
                                        res.status(200).send("ERROR");
                                        res.end();
                                    });
                                }else{
                                    guardarHistoria(datos_llegan.usuariocrea,"agregar",modulo,datos_llegan,"insert",function(val){
                                        console.log(val);
                                        res.status(200).send(data);
                                        res.end();
                                    });
                                }
                            });
                        })
                    }else{
                        console.log("DUPLICADO")
                        var data =  {};
                        data.error= "DUPLICADO";
                        data.in = datos_llegan;
                        guardarHistoria(datos_llegan.usuariocrea,"error",modulo,data,modulo,function(val){
                            console.log(val);
                            res.status(200).send("Registro Duplicado!!");
                            res.end();
                        });
                    }
                }
            });
            
        break;
        case 'actualizar':
            var itemid = datos_llegan.itemid;
            delete datos_llegan.itemid;
            datos_llegan.fechaedita = moment().unix();
            connection.query("UPDATE hospedajes Set ? WHERE itemid = ?",[datos_llegan,itemid],function(error,data){
                if(error){
                    console.log(error);
                    var data =  {};
                    data.error= error;
                    data.in = datos_llegan;
                    guardarHistoria(datos_llegan.usuarioedita,"error",modulo,data,modulo,function(val){
                        console.log(val);
                        res.status(200).send("ERROR");
                        res.end();
                    });
                }else{
                    guardarHistoria(datos_llegan.usuarioedita,"actualizar",modulo,datos_llegan,"update",function(val){
                        console.log(val);
                        res.status(200).send(data);
                        res.end();
                    });
                }
            });
        break;
        case 'eliminar':
            var itemid = datos_llegan.itemid;
            delete datos_llegan.itemid;
            datos_llegan.fechaelimina = moment().unix();
            datos_llegan.eliminado = 1;
            datos_llegan.activo = 0;
            datos_llegan.fechasalida = moment().format("YYYY-MM-DD");
            cambiarStatusHabitacion(datos_llegan.habitacion,0,datos_llegan.usuarioelimina,function(listo){
                connection.query("UPDATE hospedajes Set ? WHERE itemid = ?",[datos_llegan,itemid],function(error,data){
                    if(error){
                        console.log(error);
                        var data =  {};
                        data.error= error;
                        data.in = datos_llegan;
                        guardarHistoria(datos_llegan.usuarioelimina,"error",modulo,data,modulo,function(val){
                            console.log(val);
                            res.status(200).send("ERROR");
                            res.end();
                        });
                    }else{
                        guardarHistoria(datos_llegan.usuarioelimina,"eliminar",modulo,datos_llegan,"delete",function(val){
                            console.log(val);
                            res.status(200).send(data);
                            res.end();
                        });
                    }
                });
            });
        break;
    }
});
app.all('/datos_cat', function(req,res) {
    var datos_llegan;
    if (req.method.toUpperCase()=="POST"){
		datos_llegan=req.body;
	}
	if(req.method.toUpperCase()=="GET"){
		datos_llegan=req.query;
    }
	var instruccion=datos_llegan.instruccion;
	if (req.query.instruccion){
		instruccion=req.query.instruccion;
    }
    delete datos_llegan.instruccion;

    switch(instruccion){
        case 'combo_habitaciones':
            var option = {};  
            var where = "";
            if(req.query.tipoHabitacion) where += " AND tipohabitacion = "+req.query.tipoHabitacion;          
            connection.query("Select itemid AS value, nombre AS text From habitaciones Where  eliminado = ? AND ocupada = ?"+where,[0,0],function(err,catalogo){//client_id = ? And
                if(err){
                    res.status(200).send("ERROR");
                    res.end();
                }else{
                    // console.log("catalogo:", catalogo);
                    if(catalogo.length==0){
                        var newcat = [];
                        newcat.push({value:"",text:"HABITACIONES NO DISPONIBLES",selected:true})
                        option.options = (newcat);
                        res.status(200).send(JSON.stringify(option));
                        res.end();
                    }
                    else{
                        var newcat = [];
                        newcat.push({value:"",text:"ND",selected:true})
                        catalogo.forEach((e,i)=>{
                            newcat.push(e)
                        });
                        // catalogo[0].selected = true;
                        option.options = (newcat);
                        res.status(200).send(JSON.stringify(option));
                        res.end();
                    }
                }
            });
        break;
        case 'combo_empresa':
            var option = {};            
            connection.query("Select itemid AS value, nombre AS text From empresa Where  eliminado = ?",[0],function(err,catalogo){//client_id = ? And
                if(err){
                    res.status(200).send("ERROR");
                    res.end();
                }else{
                    // console.log("catalogo:", catalogo);
                    if(catalogo.length==0){
                        res.status(200).send([]);
                        res.end();
                    }
                    else{
                        var newcat = [];
                        newcat.push({value:"",text:"ND",selected:true})
                        catalogo.forEach((e,i)=>{
                            newcat.push(e)
                        });
                        // catalogo[0].selected = true;
                        option.options = (newcat);
                        res.status(200).send(JSON.stringify(option));
                        res.end();
                    }
                }
            });
        break;
        case 'combo_empleados':
            var option = {};            
            connection.query("Select itemid AS value, CONCAT(primer_nombre,' ',segundo_nombre,' ',apellido_paterno,' ',apellido_materno) AS text From empleados Where  activo = ?",[1],function(err,catalogo){//client_id = ? And
                if(err){
                    res.status(200).send("ERROR");
                    res.end();
                }else{
                    // console.log("catalogo:", catalogo);
                    if(catalogo.length==0){
                        res.status(200).send([]);
                        res.end();
                    }
                    else{
                        var newcat = [];
                        newcat.push({value:"",text:"ND",selected:true})
                        catalogo.forEach((e,i)=>{
                            newcat.push(e)
                        });
                        // catalogo[0].selected = true;
                        option.options = (newcat);
                        res.status(200).send(JSON.stringify(option));
                        res.end();
                    }
                }
            });
        break;
        case 'combo_clientes':
            var option = {};            
            connection.query("Select itemid AS value, nombre AS text From clientes Where eliminado = ? AND activo = ?",[0,1],function(err,catalogo){//client_id = ? And
                if(err){
                    res.status(200).send("ERROR");
                    res.end();
                }else{
                    // console.log("catalogo:", catalogo);
                    if(catalogo.length==0){
                        res.status(200).send([]);
                        res.end();
                    }
                    else{
                        var newcat = [];
                        newcat.push({value:"",text:"ND",selected:true})
                        catalogo.forEach((e,i)=>{
                            newcat.push(e)
                        });
                        // catalogo[0].selected = true;
                        option.options = (newcat);
                        res.status(200).send(JSON.stringify(option));
                        res.end();
                    }
                }
            });
        break;
    }
       
});
app.post('/upload', function(req, res){
    console.log("Entro",req);
    var modulo = "SubirFotos";
    console.log(req.files.foto_perfil)
    if(!req.files.foto_perfil) {res.status(200).send('No files were uploaded'); res.end();}
    var sampleFile = req.files.foto_perfil;
    // console.log("Entro",req.sampleFile,req.files);
    // console.log("objeto req", req);
    var path = require('path');
    var imagen = req.body.nombre + ".jpg";
    console.log("Archivo que se pretende subir: " , path.join(ruta_archivos,imagen),ruta_archivos + imagen);
    console.log(sampleFile);
    let str = sampleFile.data.toString('base64')
    let data = Buffer.from(str, 'base64');
    console.log(str,data);
    sampleFile.mv(ruta_archivos +imagen, function(err){
        if(err){ 
            console.log(err);
            var d = {};
            d.ruta = ruta_archivos;
            d.subio = false;
            d.image = imagen;
            d.error = err;
            guardarHistoria(req.body.usuario,"error",modulo,d,"media",function(val){
                console.log(val);
                fs.writeFile("../../imagenesrespaldadas/"+imagen, data, function (errr) {
                    if (errr) {console.log(errr);return res.status(200).send();}
                    console.log('Hello World > '+imagen);
                    return res.status(200).send("Error");
                });
            });
        }

        if(fs.existsSync(ruta_archivos + imagen)){
            console.log("Se subio correctamente: " + path.join(ruta_archivos,imagen));
            //correrfuncion(req.body.id_form);/* Esta función busca si ya estan subidad todas las fotos de algun reporte incompleto, de ser así crea el PDF */ 
            var d = {};
            d.ruta = ruta_archivos;
            d.subio = true;
            d.image = imagen;
            d.error = false;
            guardarHistoria(req.body.usuario,"upload",modulo,d,"media",function(val){
                console.log(val);
                res.status(200).send(imagen);
                res.end();
            });
        }else{
            var d = {};
            d.ruta = ruta_archivos;
            d.subio = false;
            d.image = imagen;
            d.error = "No se subio la imagen";
            guardarHistoria(req.body.usuario,"error",modulo,d,"media",function(val){
                console.log(val);
                console.log("no se subio");
                fs.writeFile("../../imagenesrespaldadas/"+imagen, data, function (errr) {
                    if (errr){console.log(errr); return res.status(200).send(errr);}
                    console.log('Hello World > '+imagen,"OCURRIO UN ERROR "+d.error);
                    return res.status(200).send("OCURRIO UN ERROR "+d.error);
                });
            });
        }
    }); 
});

app.all('/login_web', function(req, res){
    var datos_llegan;
    var modulo = "InicioSesion";
    if (req.method.toUpperCase()=="POST"){
		datos_llegan=req.body;
	}
	if(req.method.toUpperCase()=="GET"){
		datos_llegan=req.query;
    }
    connection.query(`Select usuarios.*,concat(empleados.primer_nombre," ",empleados.segundo_nombre," ",empleados.apellido_paterno," ",empleados.apellido_materno) AS nombreempleado From usuarios left Join empleados on usuarios.empleado = empleados.itemid Where usuarios.nombreusuario = ? And usuarios.clave = ? `,[datos_llegan.nombreusuario,datos_llegan.clave],function(err,rows){
        if (err) {
            console.log(err);
            var data = {};
            data.response = "ERROR";
            data.in = datos_llegan;
            data.user = "ERROR";
            data.data = err;
            guardarHistoria(datos_llegan.nombreusuario,"error",modulo,data,"login",function(val){
                console.log(val);
                res.status(200).send("ERROR");
                res.end();
            });
        } else {
            console.log(rows);
            if(rows.length > 0){
                rows = rows[0];      
                var fechaultimologin = moment().unix();
                datos_llegan.fechaultimologin = fechaultimologin;
                connection.query(`Update usuarios Set ? Where itemid = ?`,[{activo:1,fechaultimologin:fechaultimologin},datos_llegan.itemid],function(errr,result){
                    if (errr) {
                        var data = {};
                        data.response = "ERROR";
                        data.in = datos_llegan;
                        data.user = rows;
                        data.data = errr;
                        guardarHistoria(datos_llegan.nombreusuario,"error",modulo,data,"login",function(val){
                            console.log(val);
                            res.status(200).send("ERROR");
                            res.end();
                        });  
                    } else {
                        var data = {};
                        data.response = "EXITÓ";
                        data.in = datos_llegan;
                        data.user = rows;
                        data.data = "EXITÓ";
                        guardarHistoria(datos_llegan.nombreusuario,"login",modulo,data,"login",function(val){
                            console.log(val);
                            res.status(200).send(rows);
                            res.end(); 
                        });       
                    }
                });
                   
                                   
                
            }else{
                var data = {};
                data.response = "Datos Inválidos";
                data.in = datos_llegan;
                data.user = rows;
                data.data = "Datos Inválidos";
                guardarHistoria(datos_llegan.nombreusuario,"error",modulo,data,"login",function(val){
                    console.log(val);
                    res.status(200).send("DATOSINVALIDOS");
                    res.end();
                });  
            }
        }
    });
});
app.all("/logout", function(req, res){
    var datos_llegan;
    var modulo = "CerrarSesion";
    if (req.method.toUpperCase()=="POST"){
		datos_llegan=req.body;
	}
	if(req.method.toUpperCase()=="GET"){
		datos_llegan=req.query;
    }
    var fechaultimologout = moment().unix();
    datos_llegan.fechaultimologout = fechaultimologout;
    connection.query(`Update usuarios Set ? Where itemid = ?`,[{activo:0,fechaultimologout:fechaultimologout},datos_llegan.itemid],function(errr,result){
        if (errr) {
            var data = {};
            data.response = "ERROR";
            data.in = datos_llegan;
            data.user = "ERROR";
            data.data = errr;
            guardarHistoria(datos_llegan.usuario,"error",modulo,data,"logout",function(val){
                console.log(val);
                res.status(200).send("ERROR");
                res.end();
            });  
        } else {
            var data = {};
            data.response = "OK";
            data.in = datos_llegan;
            data.user = datos_llegan.usuario;
            data.data = result;
            guardarHistoria(datos_llegan.usuario,"logout",modulo,data,"logout",function(val){
                console.log(val);
                res.status(200).send("OK");
                res.end();
            });  
        }
    });
});



app.all("/mostrarArchivo", function(req, res) {
    var path = require('path');
    var mime = require('mime');
    var fs = require('fs');
    var tipo = "", archivo = "", ruta = "";
    var data;
    if (req.method == "GET") {
        data = req.query;
        tipo = req.query.tipo;
        archivo = req.query.archivo;
    } else if (req.method == "POST") {
        data = req.body;
        tipo = req.body.tipo;
        archivo = req.body.archivo;
    }
    // console.log(req.query);
 
    if(tipo=="Perfil"){
        if(!fs.existsSync(ruta_archivos + archivo)){
            ruta = ruta_filesmostrar;
            archivo = "profile.png";
        }else{
            ruta = ruta_archivos;
        }
    }
    if(tipo=="tipoHabitacion"){
        if(!fs.existsSync(ruta_archivos + archivo)){
            ruta = ruta_filesmostrar;
            archivo = "habitaciondef.jpg";
        }else{
            ruta = ruta_archivos;
        }
    }
    
    var file = ruta + archivo;
    console.log(file,"file");
    if (!fs.existsSync(file)) {
        return res.send("<b>No hay archivo que mostrar</b>");
    }

    var filename = path.basename(file);
    console.log(filename,tipo);
    var mimetype = mime.lookup(file);
    console.log(mimetype);
    if (data.descarga) { //Descargar Archivo Precarga de Nómina
        res.setHeader('Content-disposition', 'attachment; filename=' + filename);
    }
    res.setHeader('Content-type', mimetype);

    var filestream = fs.createReadStream(file);
    filestream.pipe(res);
});
function EnviarCorreo(correos, titulo, texto, paths, html, callback) {
    // crear un objeto de transporte reutilizable usando SMTP transport (GMAIL para pruebas)
    var transporter = nodemailer.createTransport(correo_datos);
    // configura los datos del correo
    var mailOptions = {
        from: '<edgar@dvtrace.com>',
        to: correos,
        bcc: '<edgar@dvtrace.com>',
        subject: titulo,
        text: texto
    };
    var new_paths = [];
    if(paths){
        paths.forEach(path => {
            if(fs.existsSync(path.path)){
                new_paths.push(path);
            }
        });
    }
    if(paths){
        mailOptions.attachments = new_paths;
    }
    if (html) mailOptions.html = html;
    // Envía el correo con el objeto de transporte definido anteriormente
    transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
            console.log(error);
            callback(error, null);
        } else {
            console.log(info);
            callback(null, "OK");
        }
    });
}
function guardarHistoria(usuario,accion,modulo,objeto,evento, callback) {
    var values = {};
    values.usuariocrea = usuario;
    values.accion = accion;
    values.objeto = JSON.stringify(objeto);
    values.evento = evento;
    values.modulo = modulo;
    values.fechacrea = moment().unix();
    connection.query("INSERT INTO historial SET ?",[values],function(error,result){
        if(error){
            callback(error);
        }else{
            callback(result);
        }
    });
}